---
layout: markdown_page
title: "Engineering"
---

## Communication<a name="reach-engineering"></a>

At GitLab, we communicate in the open whenever possible. We want each discussion point to be viewable by as many people as possible. This allows anyone who is able to provide a valuable response faster, where something like an email or a direct message may require waiting on somebody to return from vacation, for example. The following communication channels are listed in the order in which they should be considered.

- [**Public Issue Tracker (for GitLab CE)**](https://gitlab.com/gitlab-org/gitlab-ce); please use confidential issues for topics that should only be visible to team members at GitLab.
- [**Chat channel**](https://gitlab.slack.com/?redir=%2Farchives%2Fdevelopment); please use the `#development`, `#vpe`, `#frontend`, `#production`, `#ci-cd`, `#support_gitlab-com`, and `#support_self-managed` chat channels for questions that don't seem appropriate to use the issue tracker or the internal email address for.
- VPE Office Hours: Each week [Eric Johnson](https://gitlab.com/edjdev) holds open office hours on Zoom for questions, feedback, and handbook changes. It's typically Thursdays at 9am PST for 1 hour, but see his calendar for current times.

## On this page
{:.no_toc}

- TOC
{:toc}

## Other Related Pages

- [Engineering Management Issue Board](https://gitlab.com/gitlab-com/www-gitlab-com/boards/980804)
- [Engineering Compensation Roadmaps](/handbook/engineering/compensation-roadmaps/)
- [Engineering Hiring](/handbook/engineering/hiring/)
- [Developer onboarding](/handbook/developer-onboarding)
- [Engineering Career Development](/handbook/engineering/career-development)
- [Engineering Management](/handbook/engineering/management)
- [Engineering Workflow](/handbook/engineering/workflow)
    - [Code Review](/handbook/engineering/workflow/code-review/)
- [Frequently Used Projects](/handbook/engineering/projects)
- [Issue Triage Policies](/handbook/engineering/issue-triage)
- [Critical Security Release Process](/handbook/engineering/critical-release-process)
- [Emergency Meeting Protocol](/handbook/engineering/emergency-meeting-protocol)
- [Performance of GitLab](/handbook/engineering/performance)
- [Monitoring of GitLab.com](/handbook/engineering/monitoring)
- [Production Readiness Guide](https://gitlab.com/gitlab-com/infrastructure/blob/master/.gitlab/issue_templates/production_readiness.md)
- [Contributing to Golang projects](/courses/dev-101)
- [Pajamas Design System](/handbook/engineering/ux/pajamas-design-system/)
- Engineering READMEs
  - [Marin Jankovski](/handbook/engineering/infrastructure/team/delivery/engineering-manager/)
  - [VPE Eric J's README](/handbook/engineering/erics-readme)
  - [Yorick Peterse](/handbook/engineering/engineer-readme/yorick-peterse)
  - [Clement Ho](https://gitlab.com/ClemMakesApps/readme)

## The Importance of Velocity

* The rate at which GitLab delivers new value to users in the form of features is a competitive advantage for the project and the company.
* As an open source project, people are welcome to fork us. However, in order to ensure that the community remain intact, and the bulk of energy is directed toward one version of GitLab it is important to move fast so that any fork is quickly out of date.
* Companies tend to slow down as they grow. It takes deliberate effort to prevent this, so it must always be top of mind.
* Once you slow down, it is incredibly painful to speed back up again.

## Velocity over predictability

We optimize for shipping a high volume of user/customer value with each release. We do want to ship multiple major features in every monthly release of GitLab. However, we do not strive for predictability over velocity. As such, we eschew heavyweight processes like detailed story point estimation by the whole team in favor of lightweight measurements of throughput like the number of merge requests that were included or rough estimates by single team members.

There is variance in how much time an issue will take versus what you estimated. This variance causes unpredictability. If you want close to 100% predictability you have to take two measures:

1. Invest more time in estimation to reduce that variance. The time spend estimating things could otherwise be used to create features.
1. Leave a reserve of time with unscheduled work so you can accommodate the variance. According to [Parkinson's law](https://en.wikipedia.org/wiki/Parkinson%27s_law) the work expands so as to fill the time available for its completion. This means that we're not adhering to our [iteration value](/handbook/values/#iteration) and that for the next cycle our estimates for comparable features will be larger.

Both measures reduce the overall velocity of shipping features.
The way to prevent this is to accept that we don't want perfect predictability.
Just like our [OKRs](/company/okrs/) are so ambitious that we expect to reach about 70% of the goal this is fine for shipping [planned features](/handbook/product/#ambitious-planning) too.

_Note:_ This does not mean we place zero value on predictability. We just optimize for velocity first.

## Engineering Management Issue Board

The VP of Engineering and their Direct reports track our highest priorities in the [Engineering Management Issue Board](https://gitlab.com/gitlab-com/www-gitlab-com/boards/980804), rather than to do lists, Google Doc action items, or other places. The reasons for this are:

* It's a way to use our own product more (dogfooding)
* It lends itself to our preferred async method of working
* It's transparency across the company what senior leaders in Engineering are working on
* It allows for delegation while reducing the need for status check-ins by relying on issue notifications

Here are the mechanics of making this work:

* Use the `Engineering Management` label to get it on the board, and the department label to get it in progress (e.g. `Development Department`)
* We can re-prioritize in 1:1’s or staff meetings periodically
* Track the highest priorities by each department in Engineering
* Directors can delegate items to anyone in their department
* Link to issues on this board in places where status needs to be tracked _e.g._ 1:1 docs, staff meeting notes, etc
* It's okay to link to other issues, boards, epics, etc in the body of an issue to avoid duplicating content
* It's important to take the extra time to create an issue in the moment it becomes necessary
* We require descriptive issue titles and due dates
* An issue per quarterly OKR is expected


## GitLab Repositories

GitLab consists of many subprojects. A curated list of GitLab Repositories can be found at the [GitLab Engineering Projects](/handbook/engineering/projects) page.

When adding a repository please follow these steps:
1. Ensure that the project is under the [gitlab-org](https://gitlab.com/gitlab-org) namespace for anything related to the application or under the [gitlab-com](https://gitlab.com/gitlab-com) namespace for anything strictly company related.
1. [Add the project to the list of GitLab Repositories](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/projects.md)
1. Add an MIT license to the repository. It is easiest to simply copy-paste the [MIT License](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/LICENSE) verbatim from the `gitlab-ce` repo.
1. Add a section titled "Developer Certificate of Origin and License" to `CONTRIBUTING.md` in the repository. It is easiest to simply copy-paste the [DCO + License section](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#developer-certificate-of-origin-license) verbatim from the `gitlab-ce` repo.
1. Add any further relevant details to the Contribution Guide. See [Contribution Example](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md).
1. Add a link to `CONTRIBUTING.md` from the project's `README.md`

When changing the settings in an existing repository, it's important to keep [communication](#communication) in mind. In addition to discussing the change in an issue and announcing it in relevant chat channels (e.g., `#development`), consider announcing the change during the [Company Call](/handbook/communication/#company-call). This is particularly important for changes to [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce) and [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee).

## Access Requests
GitLab consists of many different type of applications and resources.

When you require escalated permissions or privileges to a resource to conduct task(s), or support for creating resource(s) with specific endpoints, please submit an issue to the [Access Requests Issue Tracker](https://gitlab.com/gitlab-com/access-requests/issues) using the template provided.

Below is a short list of supported technologies:
* For complete list, please see the issues template(s)
* G-Suite
* Slack
* BambooHR
* 1Password
* Greenhouse
* Other, including Email Distros

## Engineering Departments & Teams

* [Dev Backend](/handbook/engineering/dev-backend/)
  * [Create](/handbook/engineering/dev-backend/create/)
  * [Distribution](/handbook/engineering/dev-backend/distribution/)
  * [Geo](/handbook/engineering/dev-backend/geo/)
  * [Gitaly](/handbook/engineering/dev-backend/gitaly/)
  * [Gitter](/handbook/engineering/dev-backend/gitter/)
  * [Manage](/handbook/engineering/dev-backend/manage/)
  * [Plan](/handbook/engineering/dev-backend/plan/)
* [Frontend](/handbook/engineering/frontend/)
  * [CI/CD](/handbook/engineering/frontend/ci-cd/)
  * [Configure](/handbook/engineering/frontend/configure/)
  * [Create](/handbook/engineering/frontend/create/)
  * [Manage](/handbook/engineering/frontend/manage/)
  * [Monitor](/handbook/engineering/frontend/monitor/)
  * [Plan](/handbook/engineering/frontend/plan/)
  * [Secure](/handbook/engineering/frontend/secure/)
* [Infrastructure](/handbook/engineering/infrastructure/)
  * [Reliability](/handbook/engineering/infrastructure/team/reliability/)
  * [Delivery](/handbook/engineering/infrastructure/team/delivery/)
* [Ops](/handbook/engineering/ops/)
  * [Configure](/handbook/engineering/ops/configure/)
  * [Monitor](/handbook/engineering/ops/monitor/)
  * [Serverless](/handbook/engineering/ops/serverless/)
  * [Release](/handbook/engineering/ops/release/)
  * [Secure](/handbook/engineering/ops/secure/)
  * [Verify](/handbook/engineering/ops/verify/)
* [Quality](/handbook/engineering/quality/)
  * [Dev QE](/handbook/engineering/quality/dev-qe-team/)
  * [Ops & CI/CD QE](/handbook/engineering/quality/ops-qe-team/)
  * [Secure & Enablement QE](/handbook/engineering/quality/secure-enablement-qe-team/)
  * [Engineering Productivity](/handbook/engineering/quality/engineering-productivity-team/)
* [Security](/handbook/engineering/security/)
* [Support](/handbook/support/)
* [UX](/handbook/engineering/ux)

## Headcount planning

Before the beginning of each fiscal year, and at various check points throughout the year, we plan the size and shape of the Engineering and Product Management functions together to maintain symmetry.

The process should take place in a single artifact (usually a spreadsheet), and follow these steps:

1. **Product Management:** Supplies headcount numbers for PMs and development groups proportional to our roadmap efforts
1. **Engineering:** Supplies feedback to Product, headcount for management roles in the development department, and full plans for the Security, UX, Quality, and Infrastructure departments
1. **CEO:** Supplies feedback to Engineering and Product, or gives final approval

Note: Support is part of the engineering function but is budgeted as 'cost of sales' instead of research and development. Headcount planning is done separately according to a different model.

## Starting new teams

Our product offering is growing rapidly. Occasionally we start new teams. Backend teams should map to our [product categories](/handbook/product/categories/). Backend teams also map 1:1 to [product managers](/handbook/product/).

A dedicated team needs certain skills and a minimum size to be successful. But that doesn't block us from taking on new work. This is how we iterate our team size and structure as a feature set grows:

1. **Existing Team:** The existing PM schedules issues for most appropriate existing engineering team
  * If there is a second PM for this new feature, they work through the first PM to preserve the 1:1 interface
1. **Shared Manager Team:** Dedicated engineer(s) are identifed on existing teams and given a specialty
  * The manager must do double-duty
    * Their title can reflect both specialties of their engineers _e.g._ Engineering Manager, Distribution & Package
    * Even if temporary, managing two teams is a valuable career opportunity for a manager looking to develop director-level skills
  * Each specialty can have its own process, for example: Capitalized team label, Planning meetings, Standups
1. **New Dedicated Team:**
  * Engineering Manager
  * Senior/Staff Engineer
  * Two approved fulltime vacancies
  * A dedicated PM

## Team Page Template

```markdown
## Vision

...

## Mission

...

## Team Members

The following people are permanent members of the [Blank] Team:

<%= direct_team(manager_role: 'Engineering Manager, [Blank]') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Blank/, direct_manager_role: 'Engineering Manager, [Blank]') %>

## Common Links

 * Issue Tracker
 * Slack Channel
 * ...

 ## How to work with us

 ...

```

## Fast Boot Events

New teams may benefit from holding a [Fast Boot](/handbook/engineering/fast-boot/) event to help the jump start the team.
During a Fast Boot, the entire team gets together in a physical location to bond and
work alongside each other.

## Collaboration

To maintain our rapid cadence of shipping a new release every month, we must keep
the barrier low to getting things done. Since our team is distributed around the
world and therefore working at different times, we need to work in parallel and
asynchronously as much as possible.

That also means that if you are implementing a new feature, you should feel
empowered to work on the entire stack if it is most efficient for you to do so.

Nevertheless, there are features whose implementation requires knowledge that is
outside the expertise of the developer or even the group/[stage](/company/team/structure/#stage-groups) group. For these situations,
we'll require the help of an expert in the feature's domain.

In order to figure out how to articulate this help, it is necessary to
evaluate first the amount of work the feature will require from the expert.

If the feature only requires the expert's help at an early stage, for example
designing and architecting the future solution, the approach will be slightly
different. In this case, we would require the help of at least two experts in
order to get a consensual agreement about the solution. Besides, they should be
informed about the development status before the final solution is finished.
This way, any discrepancy or architectural issue related to the current solution,
will be brought up early.

## Code Quality and Standards

We need to maintain code quality and standards. It's very important that you are familiar with the [Development Guides] in general, and the ones that relates to your group in particular:

- [UX Guides](https://docs.gitlab.com/ee/development/ux_guide/index.html)
- [Backend Guides](https://docs.gitlab.com/ee/development/README.html#backend-howtos)
- [Frontend Guides](https://docs.gitlab.com/ee/development/fe_guide/index.html)
- [Database Guides](https://docs.gitlab.com/ee/development/README.html#databases)

Please remember that the only way to make code flexible is to make it as simple as possible:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">A lot of programmers make the mistake of thinking the way you make code flexible is by predicting as many future uses as possible, but this paradoxically leads to *less* flexible code.<br><br>The only way to achieve flexibility is to make things as simple and easy to change as you can.</p>&mdash; Nearby Cats (@BaseCase) <a href="https://twitter.com/BaseCase/status/1085686616499183616?ref_src=twsrc%5Etfw">January 16, 2019</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


It is important to remember that quality is everyone's responsibility.  Everything you merge to master should be production ready.  Familiarize yourself with the [definition of done].

[Development Guides]: https://docs.gitlab.com/ee/development/README.html
[definition of done]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/merge_request_workflow.md#definition-of-done

## Error Budgets

We use [SRE](https://en.wikipedia.org/wiki/Site_Reliability_Engineering)-like error budgets in [OKRs](/company/okrs/2018-q4/) to incentivize risk management and help make GitLab.com ready for mission critical customer workloads.

Each backend and frontend development team is responsible for not exceeding an allocated budget of 15 points each quarter. The severity of issues caused will impact their budget accordingly:

* ~S1: 30 points
* ~S2: 15 points
* ~S3: 6 points
* ~S4: 3 point

The Infrastructure team will perform attribution as part of the root cause analysis process and record the results in the OKRs page.

## Engineering Proposed Initiatives

Engineering is the primary advocate for the performance, availability, and security of the GitLab project. Product Management prioritizes all initiatives, so everyone in the engineering function should participate in the Product Management [prioritization process](/handbook/product/#product-process) to ensure that our project stays ahead in these areas. The following list should provide some guidelines around the initiatives that each engineering team should advocate for during their release planning:

- Review fixes from our support team. These issues are tagged with the `support-fix` label.  You can filter on open MRs [here](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?label_name%5B%5D=support-fix).
- Working on high priority issues as a result of [issue triaging](/handbook/engineering/issue-triage/). This is our commitment to the community and we need to include some capacity to review MRs or work on defects raised by the community.
- Improvements to the performance and scalability of a feature.  Again, the Product team should be involved in the definition of these issues but Engineering may lead here by clearly defining the recommended improvements.
- Improvements to our toolchain in order to boost efficiency.

## Rails by default, VueJS where it counts

Part of our engineering culture is to keep shipping so users and customers see significant new value added to GitLab.com or their self-managed instance. To support rapid development, we focus on Rails page views by default. When an area of the application sees significant usage, we typically rewrite those screens as a [VueJS](https://vuejs.org/) single page app backed by our API, in order to maintain the best qualitative experience and quantitative performance.

## Demos

The idea that [working software is the primary measure of progress](http://agilemanifesto.org/principles.html) is one of the principles of agile software development. Demoing gets more eyes on the project to uncover bugs and reveal ambiguities in the product requirements. It's also a transparent and lightweight way to provide status to the rest of the organization. Developers should demo at least once a week with product managers present. Demo meetings should be kept to 30 minutes or less. The emphasis should be on the product requirements or acceptance criteria and less on the implementation details.

## Canary Testing

GitLab makes use of a 'Canary' stage. Production Canary is a series of servers running GitLab code in production environment. The Canary stage contains code functional elements like web, container registry and git servers while sharing data elements such as sidekiq, database, and file storage with production. This allows UX code and most application logic code to be consumed by smaller subset of users under real world scenarios before being made available to all users on GitLab.com.

The production Canary stage is automatically enabled for all users visiting GitLab Inc. operated groups:

* https://gitlab.com/gitlab-com
* https://gitlab.com/gitlab-org
* https://gitlab.com/charts

The Infrastructure department teams can globally disable use of production Canary when necessary.
Individuals can also opt-out of using production Canary environments.

To opt in/out, go to https://next.gitlab.com/ and move the toggle appropriately.

To verify that Canary is enabled, use the [performance bar](https://docs.gitlab.com/ee/administration/monitoring/performance/performance_bar.html) (typing `pb`) in GitLab and watch out for the Canary icon next to the web server name.

## Resources for Development
{: #resources}

When using any of the resources listed below, some rules apply:

* Consider the cost and whether anything can be done to reduce the cost.
* You can boot up as many machines as you need.
* It is your responsibility to clean up after yourself; if a machine is not used, remove it.
* If you observe any resource that is running for long periods of time, ask the person responsible whether the machine is still in use.
* Prepend your username to any resource you start. Eg. if your name is Jane Doe, name the resource `janedoe-machine-for-testing`.

### Google Cloud Platform (GCP)

Every team member has access to a common project on [Google Cloud Platform](https://console.cloud.google.com/). Please see the secure note with the name "Google Cloud Platform" in the shared vault in 1password for the credentials or further details on how to gain access.

Once in the console, you can spin up VM instances, Kubernetes clusters, etc. Please remove any resources that you are not using, since the company is [billed monthly](https://cloud.google.com/pricing/). If you are unable to create a resource due to quota limits, file an issue on the [Infrastructure issue tracker](https://gitlab.com/gitlab-com/infrastructure).

### Digital Ocean (DO)

Every team member has access to the [dev-resources project](https://gitlab.com/gitlab-com/dev-resources/) which allows everyone to create and delete machines on demand.

### Amazon Web Services (AWS)

In general, most team members do not have access to AWS accounts. In case you need an AWS resource, file an issue on the [Infrastructure issue tracker](https://gitlab.com/gitlab-com/infrastructure). Please supply the details on what type of access you need.

## DevOps Slack Channels

There are primarily two Slack channels which developers may be called upon to assist the production team
when something appears to be amiss with GitLab.com:

1. `#backend`: For backend-related issues (e.g. error 500s, high database load, etc.)
2. `#frontend`: For frontend-related issues (e.g. JavaScript errors, buttons not working, etc.)

Treat questions or requests from production team for immediate urgency with high priority.

## Reaction Rotation

See [the Reaction description](/handbook/engineering/reaction/).
