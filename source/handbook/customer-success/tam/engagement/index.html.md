---
layout: markdown_page
title: "Account Engagement"
---

# Technical Account Management Handbook
{:.no_toc}

## On this page
{:.no_toc}

- TOC
{:toc}

- Account Engagement *(Current)*
- [Technical Account Manager Summary](/handbook/customer-success/tam/)
- [Account Triage](/handbook/customer-success/tam/triage)
- [Account Onboarding](/handbook/customer-success/tam/onboarding)
- [Using Salesforce within Customer Success](https://about.gitlab.com/handbook/customer-success/using-salesforce-within-customer-success/)
- [Gemstones](https://about.gitlab.com/handbook/customer-success/tam/gemstones)

---

# Engagement Models
There are three models currently offered for Technical Account Manager engagement. These are broken into tiers that currently use Annual Recurring Revenue as a metric for determining a manageable volume for a single Technical Account Manager and the depth of involvement during the engagement.

## Managing the Customer Engagement
Technical Account Managers will typically manage customer engagements via a GitLab project in the [`account-management` group](https://gitlab.com/gitlab-com/account-management/). This project will be based off a [customer collaboration project template](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template) and then customized to match the customer's needs as outlined above. The project is pre-loaded with milestones, issues, labels and a README template to help kick off the project and outline a proof of concept, implementation and customer onboarding.

### To start a new customer engagement:
1.  Somewhere between step 3 and step 7 of the customer journey sequence, a Solutions Architect should create a project for the customer in GitLab and include an Implementation Engineer and Technical Account Manager who're best aligned with the customer account.
2. After the Technical Account Manager has been aligned with the account, they will be assigned to the “Technical Account Manager” field within Salesforce.
3. The Technical Account Manager confirms that a new customer project has been created based on the [customer collaboration project template project](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template). If it hasn't, they need to create it and work with the Strategic Account Leader/Account Manager and/or Solutions Architect to complete it. This _should_ have been done prior to Technical Account Management involvement.
3. Follow the steps in the PLEASE-READ-THESE-INSTRUCTIONS.md file.

### To start a customer upgrade engagement:
1. Based on the customer upgrade, alignment with [gemstone tier](/handbook/customer-success/tam/onboarding) is performed to ensure the appropriate service levels. 
2. After the technical account manager has been aligned with the account, they will be assigned to the “Technical Account Manager” field within Salesforce.
3. Provided that the customer is Ruby tier or higher, confirm that the customer project has been created previously during the customer journey sequence, and if not available create a project for the customer in Gitlab and include a Technical Account Manager who're best aligned with the customer account transition.
4. Verify that the project complies with the [customer collaboration project template project](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template).
5. Follow the steps in the PLEASE-READ-THESE-INSTRUCTIONS.md file as needed.


### Where does a Technical Account Manager fit in?
During the pre-sales process, a Solutions Architect owns the project with assistance from the Strategic Account Leader and should include the Implementation Engineer if there is one assigned. A Technical Account Manager is involved but only for visibility. Until the account becomes a paying customer the project remains in pre-sales. Once the customer has paid, the Strategic Account Leader will set up the Welcome to GitLab call along with the key GitLab employees (SAL, SA, IE and Technical Account Manager) and the customer. There is a preloaded issue for this in the project template. 

This call will introduce the customer to the Technical Account Manager and begin the handover process. The Technical Account Manager will then lead the rest of the call and own the project going forward. The project is then moved from a pre-sales project under the [`pre-sales account-management` group](https://gitlab.com/gitlab-com/account-management/pre-sales) to a post-sales project under [`account-management` group](https://gitlab.com/gitlab-com/account-management).

Typically this project template isn't used for small business or mid-market accounts. However, if an Account Executive, Account Manager or Solutions Architect feels this could be useful to assist their relationship with the customer, then they can utilise it and edit down as needed. 
