---
layout: job_family_page
title: "Evangelist program manager"
---

As the Evangelist Program Manager, you will be responsible for enabling members from the GitLab community to evangelize GitLab.

## Responsibilities

* Help people write blog posts about GitLab. Curate topics, introduce people to experts, cross-post to our blog, give incentives. Result is the number of blog posts and views per month.
* Help people give talks about GitLab. Pair people with requests from event organizers, sponsor events, ensure their travel is paid for, introduce them to experts in the company. Result is the number of talks and audience members per month.
* Help people organize meetups about GitLab. Have a starter package, surface interesting content, pair them with experts, pay for location and food costs. Result is the number of meetups and meetup visitors per month.

## Requirements

* You have 5-7 years of experience running developer relations or community advocacy programs, preferably open source in nature.
* Analytical and data driven in your approach to building and nurturing communities.
* You have experience facilitating sensitive and complex community situations with humility, empathy, judgment, tact, and humor.
* Excellent spoken and written English.
* Familiarity with developer tools, Git, Continuous Integration, Containers, and Kubernetes.
* Experience in communicating with writers on a range of technical topics is a plus.
* You share our [values](/handbook/values/), and work in accordance with those values.

## Specialties
Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

### Technical Evangelism Program Manager

_Reports to: Director of Technical Evangelism_

**Mission:** Build a powerhouse technical evangelism program at GitLab that encompasses teammates and the wider community. Create a program to increase GitLab’s awareness in the wider community.

As part of this role you will have the opportunity to work closely with deeply technical leaders and marketing experts. You will be coached to develop your own technical thought leadership platform and also have the opportunity to grow your career as a technical marketer.

**Responsibilities:**
* Develop speaker’s bureau database that includes speakers and thought leadership platforms
* Manage the conferences' Call For Proposals (CFP) process, working hand-in-hand with internal and external thought leaders on their talk submissions
* Review slide decks and develop a speaker training program
* Develop slide deck formats for speaker’s that include slides of interest to GitLab campaigns 
* Support technical evangelism content development in line with thought leadership platforms
* Liaison with PR and content to execute on evangelism campaigns, as well as extend the effort of recorded talks at various events
* Work hand-in-hand with the community relations team to extend the evangelism program’s reach.  
* Define metrics of measurement with the Tech Evangelism Director
* Instrument for metrics of measurement and report on them regularly
* Build thought leadership in GitLab product areas over time



**Requirements:**
* Excellent written and verbal communication skills
* Background in or deep curiosity about the cloud computing and devops ecosystem
* Track record of success in a software company
* At least 3-5 years work experience in a fast paced working environment 
* Exceptional organizational skills
* Technical skills are a plus
* Relationships in the software DevSecOps space are a plus
* Time zone: +/- 3 hours PST






