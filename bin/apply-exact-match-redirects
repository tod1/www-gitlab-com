#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/redirect'

dictionary_id = ENV['FASTLY_DICT_ID']
headers = { 'Fastly-Key' => ENV['FASTLY_API_KEY'], 'Content-Type' => 'application/json' }

resp = Gitlab::Homepage::FastlyClient.get("/dictionary/#{dictionary_id}/items", headers: headers)
current_redirects = resp.parsed_response
items = []
Gitlab::Homepage::Redirect.from_definitions_file do |redirect|
  next if redirect.comp_op != '='

  target = redirect.target

  redirect.sources.each do |source|
    # Delete still existing redirects from the to-be-deleted list
    current_redirects.delete_if { |r| r['item_key'] == source }

    items << { op: 'upsert', item_key: source, item_value: target }
  end
end

Gitlab::Homepage::FastlyClient.patch("/dictionary/#{dictionary_id}/items", body: { items: items }.to_json, headers: headers)

# Delete items still remaining, that means they were deleted from redirects.yml
current_redirects.each do |redirect|
  key = redirect['item_key'].gsub('/', '%2F')
  Gitlab::Homepage::FastlyClient.delete("/dictionary/#{dictionary_id}/item/#{key}", headers: headers)
end
