---
layout: markdown_page
title: Handbook Changelog
---

### 2019-04-02
- [!20904](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20904) Revert "Merge branch 'remove-a.h' into 'master'"
- [!20902](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20902) Update redirect process in handbook
- [!20682](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20682) Add to tips for Slack
- [!19804](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19804) Cleanup Paid Tiers product section including tier determination process

### 2019-04-01
- [!20890](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20890) Added "What to expect" section
- [!20886](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20886) update boardroom TV instructions
- [!20859](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20859) Document the /h1import command correctly
- [!20857](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20857) Move gl-website content after other content.
- [!20849](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20849) Add clarity on whether 30% tax ruling can be expensed.
- [!20846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20846) Add anchor
- [!20840](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20840) Updated the offer process and added business ops to laptop order process
- [!20839](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20839) Removed time marker from maintainership guideline
- [!20794](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20794) Sales ops operating metrics rep productivity
- [!20791](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20791) Removed the offer email step in Contract area
- [!20544](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20544) 401(k) limit
- [!20487](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20487) Update source/handbook/ea/index.html.md
- [!20437](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20437) Correct the name of the #loc_valley chat room
- [!20429](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20429) Add section to handbook explaining the process of requesting maintainer access to www-gitlab-com
- [!19438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19438) corrected dates per feedback from a team member
- [!19388](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19388) Added "find a time" guidance in Tools and Tips
- [!19386](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19386) Modified Calendly guidance
- [!18417](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18417) Update "wider community" term in quiz

### 2019-03-30
- [!20841](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20841) Removed offer email
- [!20834](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20834) add meeting doc template per Sid
- [!20833](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20833) Update zoom recording section
- [!20824](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20824) Moved feedback workflow from Services to Support Workflows and added product feedback.

### 2019-03-29
- [!20829](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20829) Fix quality department roadmap and hiring vacancies
- [!20809](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20809) Avoid vague reasons that are actually opaque when making a change
- [!20796](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20796) Both public and private content
- [!20795](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20795) Update  hostname to localhost
- [!20780](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20780) Add PM responsibility allocation chart
- [!20777](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20777) Add Product Leadership Meeting to Product Handbook
- [!20763](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20763) Add instructions for archiving company call agenda
- [!20683](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20683) Add Portugal hiring info
- [!20561](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20561) Fixing a broken link to the Marketing Issues tracker.

### 2019-03-31
- [!20815](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20815) Add Month End Review

### 2019-03-28
- [!20787](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20787) update campaing type statuses
- [!20785](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20785) Update website handbook on gl-website group.
- [!20783](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20783) update kaiser deduction rates
- [!20774](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20774) Add pipeline minutes reset request to internal support page
- [!20768](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20768) Added content to handbook Greenhouse section regarding social referrals
- [!20767](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20767) Added a link and note to the lifecycle revamp issue.
- [!20761](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20761) Add note to clarify tuition reimbursement limits
- [!20751](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20751) updating engineering recruiter alignment
- [!20748](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20748) Add the Sendoso workflows to the handbook
- [!20742](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20742) Remove instruction re: not attending company call
- [!20727](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20727) A work in progress but updated the contract process to reflect no more offer...
- [!20696](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20696) Updates to Periscope Directory Page
- [!20692](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20692) Add product weekly meeting
- [!20669](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20669) New section for resetting CI minutes plus minor edits
- [!20571](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20571) Location factor charts
- [!20474](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20474) 2019 UK Benefits
- [!18500](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18500) Add Marketing Rapid Response Team Process to Marketing Handbook
- [!18216](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18216) Adjust Product KPIs

### 2019-03-27
- [!20724](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20724) Fix link to monitor stage
- [!20712](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20712) Added requested 10 day turnaround to Sec. Q process
- [!20711](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20711) Add guide about using discussions in issues
- [!20709](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20709) Encourage small comments in video call documents
- [!20705](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20705) add HB links to CEO shadow page
- [!20697](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20697) Add inclusiveness as part of the reason why we use the Q&A document for zoom call
- [!20686](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20686) replace rent index with location factor
- [!20684](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20684) Modify incident definition
- [!20677](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20677) Adding a ‘Company’ hiring chart
- [!20672](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20672) Sales comp page revert in finance handbook
- [!20668](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20668) Document daily stand up process for Data Team
- [!20651](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20651) Adding roles & responsibilities for FP&A team.
- [!20649](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20649) Update BI ref on feature implementation page
- [!20632](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20632) Resolve "Create Periscope Directory"
- [!20599](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20599) Add Distribution team Support Request to handbook
- [!20508](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20508) Support: Refresh feedback and complaints workflow
- [!20505](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20505) Support: Minor fixes/changes to confirmation email workflow
- [!20452](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20452) Be more proactive with issue progress/activity
- [!20445](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20445) Adding details for meetups speakers and improving TOC on Evangelist Program Handbook page

### 2019-03-26
- [!20660](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20660) Resolve "Update data page with Snowflake Permission Paradigm"
- [!20656](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20656) Update index.html.md
- [!20650](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20650) rmv deprecated tool
- [!20643](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20643) Update Infrastructure design docs links
- [!20638](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20638) Fix test plan links
- [!20634](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20634) Update CI to CI/CD tag
- [!20633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20633) Remove duplicate links in Infrastructure design index.
- [!20631](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20631) Update index.html.md
- [!20614](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20614) fix table tag
- [!20606](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20606) Adjusting segment notes so that ^ For NYC is on Northeast and Great lakes and...
- [!20593](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20593) Update time to UTC and add marketing activity examples
- [!20568](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20568) Update source/handbook/marketing/marketing-sales-development/online-marketing/index.html.md
- [!20567](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20567) Include more useful links
- [!20566](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20566) Monthly calls update.
- [!20565](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20565) Note need to enable Waiting Room for Personal Meeting ID
- [!20523](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20523) Consolidate repack blueprints
- [!20390](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20390) Add a new section for Contribute for prize.
- [!20165](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20165) Add an SLO for Community Contribution First Response
- [!19915](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19915) Sponsored Billable Travel

### 2019-03-25
- [!20610](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20610) Update nomgov
- [!20602](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20602) add vp app dev
- [!20595](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20595) Update with information on Linux Laptops
- [!20587](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20587) Update email address to dmca@
- [!20577](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20577) Handbook update with notes for using Gitlab team accounts with accountless...
- [!20575](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20575) Resolve "Add Periscope as Data Team Visualization Tool"
- [!20377](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20377) Add a line about batteries and small repairs to ensure we stay frugal
- [!20375](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20375) Quality Engineering Roadmap
- [!20367](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20367) Clarify the release post manager scheduling

### 2019-03-23
- [!20554](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20554) Udpate mm cali map for consistency
- [!20552](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20552) OPS handbook Maintenance
- [!20551](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20551) Delete test landing page used to troubleshoot webex issue
- [!20549](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20549) Adding Image to Handbook for MM territory in CA
- [!20545](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20545) Expense reimbursement
- [!20328](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20328) Jj 102
- [!19891](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19891) Document the process for new stages, groups, categories

### 2019-03-22
- [!20541](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20541) Update monitor group name on Monitor page
- [!20539](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20539) Handbook: Add Slack check emoji use
- [!20535](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20535) Use UTC for release post
- [!20530](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20530) remove UTC
- [!20522](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20522) Transparency means saying why, not just what
- [!20516](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20516) Add updated Gross IACV defintion and fix true-up hyphens
- [!20512](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20512) Reinforce values
- [!20479](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20479) Add virtual background to tools and tips
- [!20469](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20469) Fix calendly link
- [!20461](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20461) Add focus on improvement to iteration value
- [!20456](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20456) Update handbook with Anniversary Swag information
- [!20414](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20414) Fix link to pages domain verification info in docs
- [!20378](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20378) Add clement ho manager readme

### 2019-03-21
- [!20513](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20513) Add snowflake warehouse and dbt config details
- [!20502](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20502) Update with auto-renewal process.
- [!20500](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20500) Add another plan change section to internal support page
- [!20493](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20493) cleanup of desaign directory
- [!20486](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20486) Update grammar under "Collaboration"
- [!20484](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20484) Devops_Maturity_Model-Not-Found
- [!20483](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20483) Resolve "Document why sharing a camera video calls are worse than desk ones"
- [!20480](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20480) revised-calculator-link
- [!20478](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20478) Fixed Typo
- [!20472](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20472) Mention slack themes in handbook (tools and tips)
- [!20463](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20463) Thrive -> strive
- [!20449](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20449) Clarify OTE split with adjustments
- [!20424](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20424) Move the adding section from Merch Ops to Workflows handbook

### 2019-03-20
- [!20455](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20455) adds instructions for seeing who is on-call for support
- [!20450](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20450) Add counts to maintainer assignments
- [!20438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20438) Soften language of not re-opening issues
- [!20435](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20435) Add new security filter in blog
- [!20427](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20427) add correct video link to shadow page
- [!20423](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20423) Updating to reflect BE tech interview
- [!20421](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20421) Fix Plan playlist link
- [!20416](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20416) Jj organize
- [!20411](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20411) Avoid re-opening issues
- [!20409](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20409) Update source/handbook/ceo/index.html.md
- [!20408](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20408) Resolve "update python style guide"
- [!20384](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20384) Clarify temp contractor
- [!20247](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20247) Clarifying recommended equipment for tech writers
- [!19487](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19487) Information around who can change account ownership, to who and the email...

### 2019-03-19
- [!20392](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20392) Add links to incident and change management. Remove table for stable.
- [!20385](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20385) Small primitives
- [!20379](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20379) Add missing slash
- [!20376](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20376) Update source/handbook/tax/index.html.md
- [!20369](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20369) Add note that Retention can change because MRR can change.
- [!20368](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20368) Add notes on assignee vs approver for data team MRs
- [!20350](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20350) adding the python style guide for data team
- [!20346](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20346) Adding that MSP fees are not covered in BC, Canada
- [!20344](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20344) Adding the initial, simplified, CI/CD architecture diagram.
- [!20343](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20343) Updating interview training issue template urls
- [!20337](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20337) Fix typo (propietary -> proprietary)
- [!20305](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20305) Updating wording for female team member interview
- [!19929](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19929) Geo board and milestone process update

### 2019-03-18
- [!20364](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20364) Corrected links to point to Corporate Marketing project
- [!20363](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20363) Adding Matt Allen to Meltano
- [!20358](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20358) Clarify the expected amount of time in San Francisco for CEO shadow program
- [!20354](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20354) Resolve "continuous delivery for newsjacking"
- [!20352](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20352) Fix typo "CI/CE" -> "CI/CD" found during interview
- [!20345](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20345) add pricing links
- [!20341](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20341) Resolve "CEO shadow handbook additions"
- [!20335](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20335) Add integrated campaigns to Handbook
- [!20331](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20331) Updated the note on currency translation account (CTA)
- [!20316](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20316) Update QA debugging guidelines
- [!20307](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20307) Update recruiting Alignment
- [!20276](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20276) PubSec owned accounts clarification
- [!20158](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20158) Add the External Shopify orders section to the Handbook
- [!20084](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20084) DMCA Handoff from Support to Abuse
- [!19893](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19893) Updated a few small typos in the product handbook.
- [!19791](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19791) Remove trailing _ typo in eng handbook
- [!19618](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19618) Mona- You're quick review please.. trying to help SDRs get in front of leads that the SALs want to...

### 2019-03-16
- [!20324](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20324) Fix missing Oxford commas , punctuation, and capitalization.
- [!20321](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20321) Update the blog handbook on promoting integrations

### 2019-03-15
- [!20303](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20303) Specify where to find conversion rates for expenses in foreign currencies
- [!20298](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20298) Add interview guidelines
- [!20297](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20297) Fix identiation issue on Pajamas handbook page
- [!20295](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20295) Update Safeguard locations and process.
- [!20286](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20286) Update pricing page with assessment of geo based pricing suggestion
- [!20279](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20279) Remove skeleton
- [!20278](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20278) Update source/handbook/engineering/ux/index.html.md
- [!20277](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20277) Consolidate and update application security review process
- [!20069](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20069) Exec review of director+ promotions 3 months in advance
- [!19191](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19191) first run on CMOC checklist
- [!17027](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17027) Update source/handbook/sales/index.html.md
- [!16270](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16270) Adding note about when to reach out to the security team for input
- [!15247](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15247) Fix markdown link issue.

### 2019-03-14
- [!20275](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20275) Updates #support slack channel(s) in engineering communication docs
- [!20271](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20271) Small merge requests
- [!20265](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20265) jburrows001 Added links to RACI chart page
- [!20261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20261) add cloud native decks
- [!20252](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20252) Add DLV003 to the training listing
- [!20249](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20249) Update TP's pronouns
- [!20237](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20237) Update comms for unexpected or emergency situations
- [!20222](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20222) Add DLV002 recording link for Delivery team training session
- [!20218](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20218) Update interview training location
- [!20210](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20210) Update SM IM Process
- [!20207](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20207) jburrows001 Add RACI chart page and feedback links
- [!20192](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20192) Adding section to detail how we track our work
- [!20176](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20176) Clarify the release post manager for the Kickoff
- [!20155](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20155) Add calendar to Community Relations handbook page
- [!20148](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20148) Add recommendation to exclude default off features
- [!20132](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20132) Steps to debug QA pipeline failures
- [!20116](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20116) Adding calendar, office hours, and recording info to Evangelist Program Handbook page
- [!20073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20073) Update deprecated slides link to closest match from https://about.gitlab.com/training/
- [!20072](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20072) Fix typos in WIR Podcast Workflows
- [!20040](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20040) Update source/handbook/communication/index.html.md to note that slack is for informal communication
- [!19999](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19999) Desired Split COLA
- [!19994](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19994) Document Compensation Questions
- [!19901](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19901) fixing some misspellings, formatting, and dead links
- [!19781](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19781) Add pajamas to handbook
- [!19702](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19702) Add Canceling orders section to the handbook
- [!19458](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19458) Fixing typo - Update index.html.md.erb
- [!19258](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19258) update to processing vacation requirements

### 2019-03-13
- [!20204](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20204) Update offer process to exclude offer email
- [!20202](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20202) Handbook: Minor updates to import workflow
- [!20200](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20200) Updated job notifications to match the current offer process and slack announcement
- [!20184](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20184) Resolve "Updates to the data team page"
- [!20181](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20181) Reorg release post scheduling
- [!20179](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20179) Fix release post scheduling table
- [!20171](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20171) Make sure there is a pure open source repo since people in...
- [!20146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20146) Fix broken link to infrastructure blueprints
- [!20099](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20099) Support: update diagnose errors workflow
- [!19884](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19884) Adding link to team and stage labels
- [!19846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19846) Update references from GitLab Summit to Contribute
- [!18657](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18657) Add a Slack access section in the handbook

### 2019-03-12
- [!20161](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20161) Update source/handbook/marketing/marketing-sales-development/field-marketing/index.html.md
- [!20160](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20160) jburrows001 Added remaining guidance docs to handbook
- [!20145](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20145) Consolidate Finance Handbook to appropriate Finance Functions
- [!20144](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20144) Link to blog post "Beware of private conversations"
- [!20143](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20143) Resolve "Remove broken "data analysis" link under Product"
- [!20142](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20142) Resolve "Updates to the Data Page"
- [!20141](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20141) docs: process if a vuln becomes irrelevant after triage
- [!20139](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20139) Avoid private messages on Slack
- [!20136](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20136) Update index.html.md
- [!20131](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20131) added tax section to Finance
- [!20121](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20121) Quality handbook - revise test planning and quarantine process
- [!20117](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20117) Add best practice of "whoever recorded it, uploads it" when adding meetings to YouTube
- [!19916](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19916) Corrections of typos and grammatical errors in handbook pages
- [!19911](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19911) Fixes and minor maintenance of team page
- [!19511](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19511) update how to report sick time
- [!18195](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18195) Updates to workflow from username to namespace

### 2019-03-11
- [!20118](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20118) jburrows001 Started adding guidance documentation to handbook 2019-03-11
- [!20109](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20109) updated the handbook linking to new issue.
- [!20107](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20107) Second tentative to fix broken link on social media guidelines page
- [!20106](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20106) FP&A update
- [!20105](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20105) Fix broken link on social media guidelines page
- [!20101](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20101) Explain when the top priority issue in the Create issue board can be skipped
- [!20100](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20100) Adding charts link to hiring overview page
- [!20093](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20093) Jj 102
- [!20090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20090) Fix release post draft link
- [!20088](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20088) Jj 102
- [!20087](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20087) Dynamically generating hiring chart pages in new location
- [!20085](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20085) Fix wrong link for relocation
- [!20083](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20083) Fix links in the Quality handbook
- [!20076](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20076) fixed typo
- [!20075](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20075) fixed typos
- [!20071](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20071) Add Product Designer under Delaney, Dev Team Lead
- [!20070](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20070) Jj 102
- [!19995](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19995) Fixed Broken link to Gitlab Values page from...
- [!14663](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14663) Add Equipment Info for Dual Monitor Needs

### 2019-03-09
- [!20068](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20068) added short(er) version of new secure demo
- [!20067](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20067) upgraded Secure demo
- [!20061](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20061) Update source/handbook/board-meetings/index.html.md
- [!20060](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20060) Quality eng team pages
- [!20059](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20059) Add measurements and owners to Journeys + add Contribute to Buyers Journey
- [!20058](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20058) modify list view explanation
- [!20056](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20056) Update handbook confidentiality guidelines
- [!20054](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20054) Quality handbook update - polish department page with department members

### 2019-03-10
- [!20066](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20066) Add obligations for internal confidential information
- [!20065](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20065) Update the GitLab address in community/sweepstakes/ and CA Merchandise Handbook
- [!19596](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19596) Adding process of how to handle duplicate web directs.

### 2019-03-08
- [!20048](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20048) Update 'ops' dept name and link to exclude 'backend'
- [!20047](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20047) Update Development department with proper labels
- [!20046](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20046) Updating GC schedule
- [!20045](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20045) Move 'monitoring' to 'monitor' for team page
- [!20034](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20034) Organize recorded AMAs to encourage more empathy
- [!20028](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20028) Update index.html.md corrected sonarqube and added twistlock and aqua
- [!20027](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20027) Resolve "Update SA Handbook with working agreements template and renewed engagement details"
- [!20009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/20009) Financial information not public
- [!19998](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19998) Earnings Committee
- [!19997](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19997) Extend stable_counterparts to take another manager
- [!19992](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19992) Intro eng mgmt board
- [!19989](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19989) Add more details on broken master actionable steps
- [!19983](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19983) Fix broken link on 'git pages update' page
- [!19974](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19974) Replace the use of 'crew' with 'working groups'
- [!19952](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19952) Updated the legal page to include Requests for Insurance Certificates
- [!19868](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19868) Update UX Desginer onboarding page to include buddy template and process info
- [!18636](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18636) Engineering/infrastructure/blueprints/testing environments

### 2019-03-07
- [!19996](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19996) Resolve "Broken link to kickoff doc in product handbook page"
- [!19987](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19987) Update stock levels
- [!19965](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19965) Update internal trial request note
- [!19960](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19960) Update Frontend Themed Call section
- [!19902](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19902) update some recruiter/coordinator alignment
- [!19861](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19861) Updated that we send email & webinar invites regardless or not if an opp is in flight
- [!19726](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19726) Support Workflows: updates to ip-blocks guide
- [!19711](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19711) Update Group Conversation Schedule with new groups
- [!19707](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19707) Update onboarding-processes to new Swag Store process
- [!19447](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19447) Update source/handbook/engineering/infrastructure/production/index.html.md
- [!19205](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19205) Support Handbook: Use of Admin Notes Update
- [!19146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19146) adds steps for admin imports

### 2019-03-05
- [!19886](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19886) More examples of public and not public
- [!19885](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19885) Update source/handbook/marketing/product-marketing/competitive/index.html.md
- [!19883](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19883) Update to add nom gov charter
- [!19882](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19882) Update source/handbook/support/index.html.md
- [!19876](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19876) Link to stock option grant levels for clarity
- [!19875](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19875) add a way to navigate to the sales resource page
- [!19873](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19873) Update PMM team
- [!19862](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19862) Add frontmatter to Support IM page
- [!19858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19858) Update index.html.md with Support Incident Management link
- [!19857](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19857) First iteration of Incident Management process for Support
- [!19855](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19855) Team yml validation of start_date
- [!19843](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19843) Added Krisp to Tools
- [!19837](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19837) Added GitLab Executive Interview Scheduling Process
- [!19768](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19768) Updated Authorization Matrix to clarify vendor contracts approval requirements
- [!19731](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19731) Updates CI/CD team pages to reflect new names and accurately load members
- [!19703](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19703) Add the preprod environment
- [!19656](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19656) Update Availability Section
- [!19623](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19623) Security Team contact information
- [!19580](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19580) Production team on call update to include new manager rotation and DBREs
- [!19252](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19252) modify ownership rules

### 2019-03-06
- [!19860](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19860) Update the sourcing page
- [!19821](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19821) added training
- [!19792](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19792) Add Prioritization Consideration for Community Building
- [!19645](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19645) Guidance for converting ideas into MVCs quickly
- [!19566](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19566) Design: Required changes for automated scheduled deploys
- [!19522](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19522) Adds structure and a changes section to category maturity
- [!19358](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19358) Adding references for heuristic usage

### 2019-03-04
- [!19840](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19840) Fix typo of incluing
- [!19832](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19832) updated Dutch tax treatment of stock options
- [!19829](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19829) Update index.html.md to point to correct transgender non-discrimination link
- [!19828](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19828) Fix formatting for triage handbook
- [!19813](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19813) Fixing the german hiring process
- [!19812](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19812) Update removed duplicate entry
- [!19798](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19798) Fixed typo and adjusted spaces
- [!19663](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19663) add prometheus section to monitoring

### 2019-03-02
- [!19787](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19787) update label error & initials

### 2019-03-01
- [!19780](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19780) Issues should be non-confidential by default
- [!19776](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19776) recruiting metrics report monthly
- [!19774](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19774) Update index.html.md with just commit calendar
- [!19770](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19770) Why today
- [!19764](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19764) typo
- [!19762](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19762) Add unique values of our crispness between pm and eng
- [!19753](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19753) Update internal hiring process
- [!19750](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19750) Update approval for outside work
- [!19745](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19745) Removing Tinggly to avoid confusion, until we have chosen a new provider.
- [!19744](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19744) Quality - Triage Ops - Convert list items to headings for packages
- [!19727](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19727) GS 102
- [!19598](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19598) move personas from design system to handbook and change related links
- [!19590](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19590) Add ally resources to the handbook
- [!19348](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19348) Geo - Moving next gen information from doc to handbook
- [!19310](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19310) Added guidance on application review for interviewers
- [!19245](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19245) Update Lg & MM Tables
- [!18650](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18650) Updates to compensation plan

### 2019-02-28
- [!19730](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19730) use emoji reactions
- [!19724](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19724) Fix no toc for positive intent
- [!19714](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19714) Add thank you and best wishes
- [!19705](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19705) jburrows001 Fixed broken link in acceptable use
- [!19701](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19701) Clarify that decision tree is a guide
- [!19699](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19699) Clarifying that PTO Ninja now updates Bamboo HR.
- [!19693](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19693) Add form for new macbook orders
- [!19691](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19691) DCO update
- [!19685](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19685) Add CI/CD architecture page
- [!19680](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19680) spacing
- [!19658](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19658) Update repairs
- [!19616](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19616) Restructure quality dept okrs
- [!19588](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19588) Tweak trainee maintainer process
- [!19576](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19576) Add instructions for executing scripted changes
- [!19531](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19531) Make relevant engineering group aware of new test failures

### 2019-02-27
- [!19669](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19669) rmv looker
- [!19665](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19665) linked infra meetings from infram main
- [!19664](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19664) fix typo
- [!19662](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19662) Update index.html.md
- [!19661](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19661) Jj 102
- [!19655](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19655) Fix formatting of DQP page
- [!19653](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19653) typos
- [!19652](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19652) deleted page
- [!19651](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19651) spelling errors and job updates
- [!19649](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19649) Added Service Account Request to Access Management Process
- [!19647](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19647) candidate in interview stages that are deleted
- [!19640](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19640) Template helper for chart that visualizes headcount and vacancies by department
- [!19639](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19639) Add GCK to engineering productivity stewardship
- [!19636](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19636) Fixing the SDR Practice to what we actually do
- [!19633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19633) update recruiter alignment
- [!19630](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19630) Adding directions to mark an event as a Featured Event
- [!19613](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19613) Remove mentions of our old BI tool
- [!19584](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19584) Update KPIs and Operating Metrics with Links and Placeholders
- [!19523](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19523) Iteration on Data Team issue management and prioritization
- [!19512](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19512) Fix misspelling of ‘dialogue’ in Update index.html.md
- [!19463](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19463) Jj 101

### 2019-02-26
- [!19619](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19619) updating documentation for features.yml details changes
- [!19607](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19607) Resolve "Update Customer Reference page in Handbook"
- [!19605](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19605) how to change group slack DMs to private channels
- [!19604](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19604) exported to imported
- [!19603](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19603) Update Frontend theme call for March
- [!19599](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19599) Include next.gitlab.com in the canary section
- [!19569](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19569) Give all images on about.gitlab.com alt values
- [!19530](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19530) Add more information regarding triage packages to the handbook.
- [!19401](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19401) Publish and explain our metrics in the handbook

### 2019-02-25
- [!19575](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19575) Measure to Framework for Manage
- [!19571](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19571) Update release day tasks
- [!19559](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19559) Point out that Create issue board should be filtered by milestone
- [!19551](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19551) Fix typo "an merge -> a merge"
- [!19547](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19547) Revised P5- MQL- Trial note
- [!19543](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19543) Added internal use only
- [!19490](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19490) Resolve "Remove everything about non-Delivery teams doing releases"
- [!19481](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19481) sendoso updates
- [!19380](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19380) Update @dennis' title again

### 2019-02-22
- [!19529](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19529) Update source/handbook/engineering/ux/index.html.md
- [!19519](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19519) Add google docs notifications tip
- [!19517](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19517) Summit to contribute
- [!19506](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19506) Adds "Assume Positive Intent" behavior to collaboration
- [!19502](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19502) Add PYB template and update handbook
- [!19494](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19494) Update supplies ordering.
- [!19489](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19489) add sec-ops
- [!19485](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19485) Add Shift-Command-5
- [!19483](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19483) Added a section for UI
- [!19480](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19480) Jburrows001 updated sec controls page
- [!19479](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19479) Fixing typo on index.html.md
- [!19475](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19475) Re-order operating metrics page
- [!19433](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19433) Add docs vs blog post instructions
- [!19429](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19429) Clarify UK pension contributions are gross
- [!19397](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19397) Update the evangelist merchandise workflow section of the handbook
- [!19392](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19392) Update evangelist merchandise operations section of the handbook
- [!19322](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19322) Update Interview Scheduling process for executives
- [!19149](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19149) Adding headcount DRI
- [!19118](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19118) Add experience factor to salary update
- [!18999](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18999) Add info on providing large files
- [!18270](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18270) Design Document for GitLab Service Inventory Catalogue. Reference:...

### 2019-02-21
- [!19477](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19477) fixing links
- [!19474](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19474) fixing typo
- [!19471](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19471) Improved estimation issue example for Manage
- [!19465](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19465) Update index.html.md
- [!19462](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19462) Add examples to Customer counts
- [!19457](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19457) Fix bad HTML on operating metrics page
- [!19443](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19443) Change URLs to 'sponsorship-request' template
- [!19440](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19440) Explain hierarchy design
- [!19439](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19439) Update index.html.md revised whitepaper for secure
- [!19436](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19436) Added "advocate for a day" workflow to handbook
- [!19427](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19427) XDR requested updated
- [!19413](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19413) Add email review protocol for 1000+ target list
- [!19405](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19405) Add troubleshooting section to support workflows
- [!19327](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19327) Clarify remote vs distributed
- [!19309](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19309) Update index.html.md to correctly align services support roles
- [!19223](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19223) Introducing panel format for kickoff meeting
- [!19138](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19138) Discuss with Security Team before changing priority for sec issues
- [!19000](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19000) Adding evaluation criteria for community events
- [!18914](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18914) Resolve "Increase specificity of how we count subscription in Operating Metrics"

### 2019-02-20
- [!19398](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19398) ZFS Blueprint
- [!19395](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19395) update to date
- [!19391](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19391) Update onboarding for deployer
- [!19382](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19382) Simplify acquisition process
- [!19378](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19378) Add note about making sure your pipelines pass if you commit direct to master
- [!19374](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19374) Fix typo
- [!19080](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19080) Correctly indent sub-bullet points

### 2019-02-19
- [!19364](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19364) bdr updates & test
- [!19363](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19363) New bullet under transparency value
- [!19361](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19361) updated the timeline for the reviews
- [!19360](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19360) Adding Zoom webinars link to EA page
- [!19352](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19352) add "competitive" as a reason to not be public by default
- [!19338](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19338) Update chat channel info
- [!19333](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19333) Update source/handbook/support/index.html.md
- [!19332](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19332) Update index.html.md
- [!19330](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19330) Update index.html.md
- [!19320](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19320) Update source/handbook/ceo/pricing/index.html.md
- [!19316](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19316) Add serial form
- [!19303](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19303) Adding pre-fix bounty review process.
- [!19257](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19257) remove TriNet from sick time
- [!19212](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19212) Add OKR section
- [!19063](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19063) Add in TAM top 10 process MVC
- [!18846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18846) Clarify the scope of 'outside projects' that require prior approval
- [!18720](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18720) Guidance for link unfurling in Slack.
- [!17796](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17796) Fix typo under "Equal Employment Laws" section of Code of Conduct

### 2019-02-18
- [!19317](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19317) Lbrown link doc & presentation to group conversations

### 2019-02-17
- [!19314](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19314) fixed typo in basics section
- [!19270](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19270) A little Oxford comma humor

### 2019-02-15
- [!19297](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19297) update last name
- [!19295](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19295) rmv fmr tmmbr + new sdr pair
- [!19285](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19285) Mentions instead of assign
- [!19281](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19281) Jj salespage2
- [!19280](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19280) Add Zendesk light agent info
- [!19276](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19276) Encourage people to use merge requests even if merging themselves
- [!19273](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19273) fixing format error
- [!19267](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19267) Updating Sales Resource Page
- [!19264](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19264) Add label links to sensing mechanisms section
- [!19261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19261) update campaign type w/status progs
- [!19254](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19254) Add ci/cd resources
- [!19225](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19225) Changing the product handbook
- [!19166](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19166) Explain why focusing on a single instance is better

### 2019-03-24
- [!19293](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19293) Updated Frontend Plan and Create pages to correct manager roles

### 2019-02-14
- [!19243](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19243) Fix broken links to backend engineering management
- [!19237](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19237) Rename instance monitoring to self-monitoring
- [!19230](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19230) updating FY2020 function & cost center changes
- [!19220](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19220) Update broken link
- [!19216](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19216) Adding link to the 'Rebuild in GitLab' label view to dogfooding section of PM handbook
- [!19188](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19188) Update Internal Department Transfers to not be contradicting.
- [!19153](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19153) Modifying goodbye procedure for feasibility
- [!19150](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19150) Closing thought 1-3 feature highlight
- [!19122](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19122) Added a how to read this data section for Throughput
- [!19049](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19049) Jj sales resource page
- [!18973](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18973) Remove "above and beyond" from discretionary bonus criteria language

### 2019-02-13
- [!19195](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19195) Regrouping thoughts in the Group Conversations section
- [!19184](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19184) Update EA support
- [!19183](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19183) Add clarification on non-traditional coworking spaces
- [!19179](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19179) Examples: ETL update and Looker update
- [!19178](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19178) Change header size and colors from handbook/values
- [!19177](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19177) Clarify filename format
- [!19169](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19169) Updating territories and assignments
- [!19160](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19160) Adjusting Sales segment on Lead to match SFDC rules
- [!19158](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19158) Update source/handbook/finance/prepaid-expense-policy/index.html.md
- [!19155](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19155) values fit
- [!19151](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19151) adding additional kpi to kpi page and updating investor update process
- [!18926](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18926) Add traveling to relocation
- [!18878](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18878) How to evaluate and assign community events
- [!18820](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18820) few typo edits
- [!18716](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18716) Fixed formatting issue in Mac Tips
- [!18609](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18609) fix a typo
- [!18549](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18549) Minor edits on interviewing page
- [!18226](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18226) Fix broken Looker links on Feature Instrumentation page
- [!17793](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17793) Bug: Fix outstanding broken links for all-remote page
- [!17722](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17722) Added USB-C hub for New MBPs

### 2019-02-12
- [!19143](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19143) Add 30 days Cash Collections escalation email to AE
- [!19141](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19141) Update index.html.md Handbook Equipment Example update w/ new monitor suggestion
- [!19136](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19136) How to verify that Canary is enabled on GitLab.com
- [!19130](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19130) Add workflow for CA suspended tickets
- [!19128](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19128) Update How-to-be-a-FRT-Hawk.html.md add view sort info
- [!19111](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19111) Update UX support rep
- [!19014](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19014) Handbook customer success tam 70
- [!18982](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18982) Quality dept guidelines
- [!18790](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18790) Blueprint for taking CI/CD oncall in SRE
- [!18277](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18277) Blueprint+Design: Database bloat analysis + pg_repack

### 2019-02-11
- [!19108](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19108) Jburrows001 update security awareness training
- [!19096](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19096) Update link to dormant namespace requests.
- [!19095](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19095) Fix broken and missing design docs links
- [!19094](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19094) Updated primary contact
- [!19092](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19092) Fix broken link to design document
- [!19090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19090) Update CXC Canada duration
- [!19087](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19087) Clarify dogfooding process
- [!19085](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19085) rmv emoji tags
- [!19064](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19064) DPA standard terms
- [!18983](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18983) Add blog calendar to handbook
- [!18941](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18941) Add instance monitoring category epic to other monitor functionality
- [!18891](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18891) Update source/handbook/ea/index.html.md
- [!18851](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18851) Update index.html.md
- [!18803](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18803) Emphasize iteration and issues when outlining category vision
- [!18648](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18648) Add specific example for importing repos from GitHub.
- [!18546](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18546) Remove upgrade barometer section from the release blog posts
- [!18474](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18474) Added notes on estimation

### 2019-02-09
- [!19082](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19082) add back a necesary label
- [!19081](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19081) modify mktgops section w updated info
- [!19039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19039) add new campaign channel types
- [!19013](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19013) Update source/handbook/finance/prepaid-expense-policy/index.html.md
- [!18961](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18961) internal dmca request workflow

### 2019-02-08
- [!19059](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19059) Update broken link to infra team slack channels
- [!19056](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19056) Updates to Kickoff Process and Adds Closing Reminder
- [!19054](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19054) Support workflow update for W9 and DPA
- [!19047](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19047) create email templates in greenhouse
- [!19022](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19022) new changes to how we eval corp events
- [!19007](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19007) quick typo and format updates
- [!18963](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18963) Add more detail to Zuora Subscription management
- [!18919](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18919) Update index.html.md
- [!18128](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18128) Update evangelist program page section on content contributions

### 2019-02-07
- [!18997](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18997) Please do not use the GitLab logo as your personal avatar on social
- [!18996](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18996) Updated Ux Research Processes
- [!18989](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18989) Clarifying the mobile policy
- [!18988](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18988) Add CD product competitors table
- [!18984](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18984) Remove secure onboarding doc
- [!18978](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18978) Revert "Update index.html.md"
- [!18977](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18977) Submitting weekly forecasts
- [!18976](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18976) add ryan rmv michael
- [!18975](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18975) Add Headers and Anchors to Behavior Lists in Values
- [!18960](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18960) Update HackerOne process
- [!18952](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18952) Remove search functionality from Plan
- [!18897](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18897) Update engineering workflow

### 2019-02-06
- [!18967](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18967) update spelling
- [!18957](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18957) Update source/handbook/ceo/shadow/index.html.md
- [!18956](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18956) Add 2/6/19 training
- [!18954](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18954) Update setting_ticket_priority.html.md
- [!18953](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18953) Fix block quotes
- [!18951](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18951) Update recruiter alignment
- [!18949](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18949) Updates to CI and CD details
- [!18947](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18947) Resolve "Make clearer that dbt bullet points are coming soon on data team page"
- [!18946](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18946) Clarify gitlab VPS info
- [!18938](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18938) Update internal-support/index.html.md
- [!18935](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18935) draft of new corp events goals
- [!18934](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18934) Add yearly clarifier to 401k match
- [!18933](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18933) Jj itgroups2
- [!18930](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18930) Twitter is a publishing channel
- [!18922](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18922) Update source/handbook/incentives/index.html.md
- [!18921](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18921) Update index.html.md
- [!18917](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18917) Updates to territory AEs.
- [!18913](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18913) Update CEO Phishing Instructions
- [!18910](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18910) Explain grant options acceptance
- [!18901](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18901) Security release development design
- [!18899](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18899) Handbook update for Geo team issue weights
- [!18889](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18889) Add process for milestone cleanup
- [!18888](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18888) updating intro to engr metrics and adding links
- [!18873](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18873) Add updating Kickoff doc to timeline
- [!18771](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18771) Update SAO Criteria
- [!18746](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18746) Update sub-departments, groups, and categories

### 2019-02-05
- [!18937](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18937) adds note to communicate ticket merge to customer
- [!18928](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18928) No longer require a google doc for discretionary bonuses
- [!18915](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18915) Add link to new videos in Slack
- [!18908](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18908) Adding a new setup guide to tools
- [!18904](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18904) Update index.html.md
- [!18902](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18902) Document tax exemption process
- [!18890](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18890) Clarify project not product
- [!18883](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18883) Update source/handbook/marketing/product-marketing/index.html.md
- [!18882](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18882) Jburrows001 fix sec controls format
- [!18875](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18875) Add MRR change documentation
- [!18866](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18866) Add 200% limit for ramping SDR quota (last MR was just for BDR quota)
- [!18841](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18841) Removed Andrew and added Brandy to the EMEA territory table
- [!18837](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18837) Add trademark link
- [!18827](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18827) Add customer discovery and sharing to customer meetings for Product handbook
- [!18787](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18787) link to shared gdoc for incident and close RCA on end of discussion
- [!18781](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18781) add qualifies on what is inclusive language
- [!18705](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18705) Fixed Manage dev-backend stable_counterparts issue caused by my title change
- [!18666](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18666) Prioritize Internal Requests at 10x Customer Request
- [!18568](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18568) Tech Writing handbook updates including FY20 Vision and main epic links
- [!16971](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16971) Fix broken links to an article

### 2019-02-04
- [!18879](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18879) Update source/handbook/engineering/security/index.html.md,...
- [!18870](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18870) Update source/handbook/marketing/corporate-marketing/index.html.md
- [!18869](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18869) Change order
- [!18864](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18864) Fix FE Team pages for Secure, Monitor and Configure
- [!18860](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18860) Remove Move Calc
- [!18859](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18859) how to apply to internal job board plus help article
- [!18858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18858) update pto ninja link
- [!18847](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18847) Fix a few typos in the storage node blueprint
- [!18843](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18843) adjusting language around word tribe to IT group
- [!18839](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18839) Small improvements inside 360 feedback handbook page
- [!18819](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18819) new hires on interview teams
- [!18815](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18815) fixed broken link on cs salesforce page
- [!18774](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18774) Global Optimization of critical issues process addition
- [!18761](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18761) Update Content Hack Day hb
- [!18664](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18664) Update Kibana.html.md
- [!18577](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18577) creating internal support quick reference
- [!18494](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18494) Add a design document for merging CE and EE codebases
- [!18083](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18083) Small improvements inside compensation page
- [!15937](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15937) Corrected two typos.

### 2019-02-03
- [!18852](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18852) Gerir/infra/design/okr
- [!18848](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18848) Update SAO criteria and definition
- [!18811](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18811) Fix "definition of done" link to go directly to description.

### 2019-02-01
- [!18833](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18833) PTO Ninja
- [!18832](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18832) Edit Support Handbook's documentation-related text and add links to docs policies
- [!18829](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18829) Add language around renewal gaps for subscriptions
- [!18828](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18828) how to apply as internal applicant
- [!18825](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18825) bgc process same time as references
- [!18824](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18824) added additional language that is recommended under the NY guidelines.
- [!18821](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18821) Add promotion & transfer process for internal applicants
- [!18813](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18813) Update sheetload documentation
- [!18810](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18810) Link to more useful issue boards from Create team page
- [!18806](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18806) Add SFDC metrics for EDU and OSS programs
- [!18802](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18802) Update source/handbook/marketing/marketing-sales-development/field-marketing/index.html.md
- [!18794](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18794) Update #infrastructure Slack channel is now #production
- [!18778](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18778) Add fast boot handbook page
- [!18758](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18758) Reorganize Product Handbook
- [!18731](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18731) Don't force people to attend meetings or pay attention to them.
- [!18715](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18715) Update Create 'What to work on' process
- [!18714](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18714) Mind the oxford comma
- [!18685](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18685) Clarify breadth over depth
- [!16612](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16612) Uniform Auto DevOps variants

### 2019-02-02
- [!18822](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18822) refining blueprints
- [!18701](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18701) add details related to BDR priority view process

### 2019-01-31
- [!18786](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18786) adding Area Sales Manager role to stock
- [!18783](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18783) Remove Reference to TriNet
- [!18777](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18777) Add in criteria for the issue template description, and provide details on milestone and due date.
- [!18773](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18773) Add CXC Canada doc link
- [!18772](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18772) clarify recruiting tasks for vacancies and add internal job board instructions
- [!18770](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18770) Upload New File for handbook
- [!18766](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18766) Quick update to 360 updates
- [!18750](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18750) #movingtogitlab
- [!18744](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18744) jburrows001 Fixed format of the security compliance controls page
- [!18743](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18743) Enablement live stream
- [!18729](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18729) Fixed two small typos
- [!18728](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18728) Add announcing new job openings to current team on Slack
- [!18718](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18718) Clarify two way door decisions

### 2019-01-30
- [!18737](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18737) Link from Discretionary Bonus Page to Process
- [!18736](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18736) Update index.html.md
- [!18733](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18733) jburrows001 Add controls page and linked it in the security handbook
- [!18727](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18727) Added two new demo videos
- [!18723](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18723) Add CI/CD primer back to page
- [!18722](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18722) updated 360 language
- [!18719](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18719) Jj tribes2
- [!18712](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18712) add link to connect slack and greenhouse
- [!18708](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18708) Update EDU workflow to make sure quotes are received
- [!18699](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18699) Standardize on Likely Buyer terminology
- [!18692](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18692) Updating emergency chat instructions to match the newest version of Zoom
- [!18689](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18689) adds emergency chat protocol
- [!18668](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18668) Update Distribution and Delivery team tables
- [!18634](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18634) Update source/handbook/customer-success/tam/index.html.md
- [!17618](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17618) Add security workflow to self-managed handbook

### 2019-01-29
- [!18683](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18683) Adjustments to Comp Page, currency conversion
- [!18680](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18680) how to link greenhouse users to their profiles
- [!18679](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18679) Update source/handbook/marketing/product-marketing/analyst-relations/index.html.md
- [!18673](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18673) Updated Buyer to Primary Buyer and fixed link
- [!18672](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18672) Update source/handbook/marketing/marketing-sales-development/field-marketing/index.html.md
- [!18670](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18670) Exaplain how to request Greenhouse access
- [!18656](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18656) Update source/handbook/tools-and-tips/index.html.md
- [!18644](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18644) Adding method to post a goodbye message upon termination
- [!18642](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18642) remove ellinger from territory chart
- [!18635](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18635) what to do if a candidate reapplies
- [!18633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18633) Add weight guidelines to Geo team handbook
- [!18581](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18581) Update confidential youtube videos setting in handbook
- [!18564](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18564) Link to categories explainer
- [!18542](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18542) Fix the order of interview steps
- [!18528](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18528) Remove post-MVC epics and mention epic relations
- [!18383](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18383) Adding note about simplicity, and link to Robert C. Martin's SOLID principles
- [!18320](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18320) Make page simpler and shorter.
- [!18283](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18283) additional language to promotions- managers not informing team member until...
- [!18153](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18153) Updated referral language
- [!18064](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18064) Update onboarding template names

### 2019-01-28
- [!18621](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18621) greenhouse access requests
- [!18619](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18619) Update source/handbook/marketing/marketing-sales-development/field-marketing/index.html.md
- [!18612](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18612) Fix YouTube link for 360 culture amp
- [!18604](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18604) Change Distribution EM
- [!18603](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18603) add pops analyst to job appproval flow for comp
- [!18593](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18593) Explain canary environment and update for opt-out
- [!18585](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18585) Resolve "Adding Customer Reference Board to CRP handbook page"
- [!18584](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18584) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!18578](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18578) corrected  few typos and added the new video link to be uploaded as youtube
- [!18545](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18545) Update Recruiter Alignment
- [!18544](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18544) Add executive level hiring page
- [!18532](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18532) Updated "Involving Experts Workflow"
- [!18335](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18335) Capture our "Product Structure over Group Capacity" notion in Handbook
- [!18271](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18271) Dutch law changed since Jan 1st - we now have five days of paternity leave instead of two
- [!18209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18209) Changed Art Nasser name change
- [!18063](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18063) Remove outdated reference to scheduling slack channel

### 2019-01-26
- [!18580](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18580) rmv terminus from tech stack
- [!18572](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18572) Fix minor typo
- [!18495](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18495) Add missing space after comma (,) on index.html.md
- [!17933](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17933) Add 200% ceiling for ramping SDRs

### 2019-01-25
- [!18567](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18567) add key messages to just commit framework
- [!18565](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18565) Added MOM Motto, "the mom mot"
- [!18555](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18555) grammar edits to contracts page, add ping to update
- [!18551](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18551) 360 feedback
- [!18548](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18548) interviewers can't view closed jobs
- [!18543](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18543) Add workflow for users over license (EDU and OSS programs)
- [!18533](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18533) add updated intn bc info
- [!18531](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18531) add repairs + loaner sections
- [!18520](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18520) Fix Categories Maturity milestone from Summit to May 2019
- [!18511](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18511) Updated Quality dept links and career path
- [!18507](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18507) Update boards to new product marketing project
- [!18337](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18337) Add increased emphasis to dogfooding
- [!18258](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18258) Add documentation on how zuora subscriptions are linked

### 2019-01-24
- [!18529](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18529) updated design index for OKRs
- [!18526](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18526) OKR design: first iteration
- [!18506](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18506) additional language around managers communicating a promo
- [!18488](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18488) first iteration on Infra meetings
- [!18482](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18482) Link secruity blueprint page and Delivery training page to their index pages
- [!18481](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18481) Actually link to the calculator's home page
- [!18479](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18479) workload workflow: blueprints and designs
- [!18478](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18478) Add a page for Delivery team trainings
- [!18476](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18476) Update entities and locations
- [!18470](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18470) Update index.html.md with integrated campaign and updates to event processes for MPMs
- [!18462](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18462) updates to reliability workload workflow
- [!18452](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18452) - 360 Section
- [!18426](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18426) Define a logo request workflow in CA handbook
- [!18401](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18401) updated timeline for 360 survey
- [!18334](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18334) Emphasize when to close an issue and why
- [!18261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18261) Clarify process on broken e2e test that will be introduced to master
- [!18245](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18245) Security Release Blueprint: Current situation and path forward
- [!16028](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16028) First draft of Release design and engineering workflows blueprint

### 2019-01-23
- [!18446](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18446) updated DMP page to document keyword volume usage
- [!18444](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18444) Add clarification around Fiscal dates for GitLab and Looker
- [!18439](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18439) Adding event decision tree to marketing handbook pages
- [!18438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18438) director notes: workload management, initial draft
- [!18435](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18435) Update Community Relations workflows and guidelines handbook pages to remove .html extension
- [!18429](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18429) Replaced request to contact a specific person
- [!18428](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18428) offers slack channel
- [!18423](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18423) Update involving experts links and copy
- [!18398](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18398) Updated CEO scam section for SSOT
- [!18346](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18346) Add Kibana slide deck
- [!18294](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18294) Contract type factor to contract factor to simplify things.
- [!18123](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18123) Groups for Manage
- [!17694](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17694) Add workflow page for the distribution team.
- [!17668](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17668) Vue salary calculator

### 2019-01-22
- [!18403](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18403) Jj refine trbs
- [!18395](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18395) Update index.html.md
- [!18394](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18394) Fix link to "How to add events to the events page"
- [!18393](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18393) Updated security handbook for CEO fraud instructions
- [!18392](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18392) Updated old ceo scam link
- [!18389](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18389) clarification on initials and note about future alignments as we grow
- [!18387](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18387) Add missing word to the internet subscription expense
- [!18384](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18384) Resolve "Add Looker workflow overview"
- [!18378](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18378) handbook updates documenting form formatting
- [!18358](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18358) Support: Add note on merging tickets
- [!18354](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18354) Add to PR section of hb
- [!18351](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18351) Fix get-help link in distribution handbook
- [!18348](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18348) Clarifies Gold incentive availability and links directly to issuable template.
- [!18313](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18313) Get Fancy on Data Team Page
- [!18310](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18310) adding language with manager skills requirements
- [!18297](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18297) Add Looker video to the handbook page
- [!18265](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18265) Added churn definition to operating metrics

### 2019-01-21
- [!18344](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18344) Added missing frontmatter to Yorick's README
- [!18339](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18339) Deleted deprecated page about finding tickets in ZD
- [!18338](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18338) clarify git to gitlab
- [!18333](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18333) Update Education and Open Source program handbook pages
- [!18331](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18331) Clean up format
- [!18330](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18330) Fixed link, added templates, and cleaned up text
- [!17940](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17940) Added a personal employee README

### 2019-01-20
- [!18323](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18323) Revamp legal page and add Attorney-Client privilege content
- [!18322](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18322) Update Conga process
- [!18217](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18217) International payroll process

### 2019-01-19
- [!18319](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18319) update form number
- [!18311](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18311) add content for testing
- [!18309](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18309) Add new directory

### 2019-01-18
- [!18293](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18293) Add information regarding the Beamy.
- [!18292](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18292) Greenh 118
- [!18291](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18291) add SA Manager stock options level
- [!18281](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18281) Add link to CFP Submission template
- [!18263](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18263) Preference for product categories which map to vendor categories from analysts
- [!18256](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18256) Add guidance for title length on release post
- [!18248](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18248) Additional 401(k) Match Details
- [!18147](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18147) Expanded awareness training description
- [!18021](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18021) Changing the way to do compensation for current TM.
- [!17772](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17772) Typo correction, "assests" should be "assets".
- [!17389](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17389) Fix spelling/typos in links at bottom of index.html.md
- [!17377](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17377) Update index.html.md
- [!17184](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17184) clarifies hiring and role of support team
- [!17007](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17007) Update index.html.md

### 2019-01-17
- [!18229](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18229) Update package specialty
- [!18222](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18222) update backup snapshot details to be current re: GCP
- [!18213](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18213) For the handbook, some tips on using the phone with Zoom
- [!18206](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18206) Fix categories typo
- [!18197](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18197) Added link to BitBar and GitLab Plugin
- [!18191](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18191) Add Target audience and experience to category epic template
- [!18163](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18163) Resolve "Update CS Handbook for POC"
- [!18122](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18122) Fix link to setup NextTravel account
- [!18050](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18050) Include persona labeling information towards PM and UX handbooks
- [!18010](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18010) Prioritization of Broken master and Tests
- [!17955](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17955) Add documentation for distribution usage of dependencies.io

### 2019-01-16
- [!18200](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18200) Move Gitter to Create
- [!18199](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18199) Revert "Merge branch 'brendan-add-amazing' into 'master'"
- [!18196](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18196) Remove "start on Monday" recommendation in interview process
- [!18192](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18192) Add amazing as a maturity level
- [!18187](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18187) Add frontend call theme for February
- [!18186](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18186) Add fulfilment to @dennis title
- [!18170](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18170) Start any day -
- [!18168](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18168) Add instructions for categories
- [!18117](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18117) Adopt label renaming from ~"feature proposal" to ~feature
- [!18087](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18087) Changed team name: Sync to Fulfillment
- [!18076](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18076) Remove deprecated workflow
- [!18002](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18002) Add additional Sketch admin
- [!17824](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17824) Update distribution infrastructure maintenance: SSH public keys
- [!17742](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17742) Fold country factor into location factor
- [!17719](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17719) updating team's focus to how we work and current / oncall notes about triage
- [!17651](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17651) Updates 2019 direction (and fixes some typos)

### 2019-01-15
- [!18167](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18167) Update competitive assessment sensing mechanism to include reference to missing features
- [!18162](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18162) Update index.html.md
- [!18160](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18160) Patch 10236
- [!18137](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18137) Remove SMAU tracking
- [!18118](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18118) Add maintainer review to Distribution Triage handbook entry
- [!18102](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18102) add 401(k) match
- [!18074](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18074) Update CA knowledge base
- [!18037](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18037) Any change request that involves running a script must have a dry-run...
- [!18028](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18028) Updated Customer At Risk (Emergency Room) documentation
- [!17982](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17982) Jj add tribes
- [!17977](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17977) Add lost instances definition to operating metrics
- [!17928](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17928) Add tech onboarding for secure team
- [!17901](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17901) Fix broken gitlab link on onboarding issue tasks page
- [!17296](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17296) Add note about grooming

### 2019-01-14
- [!18126](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18126) Further updates to Territory Assignment tables.
- [!18120](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18120) Update index.html.md with some updates
- [!18115](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18115) Add link to Serverless from ops-backend/index
- [!18110](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18110) Change milestone start/end date
- [!18107](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18107) Remove SDR and BDR Lead, this no longer exists
- [!18104](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18104) expense report due date
- [!18046](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18046) Fix some broken links to MR coach
- [!17858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17858) update emea territory table
- [!17829](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17829) Add dogfooding to Serverless team mission

### 2019-01-13
- [!18092](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18092) Fix typo: s/BitBucket/Bitbucket/g
- [!18086](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18086) Fix links to guides on issues and MRs
- [!18085](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18085) Fixed an outdated feature flags documentation link in the Engineering workflow
- [!18033](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18033) Refine enablement

### 2019-01-12
- [!18081](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18081) Add links to OKR and Epic Roadmap for Data Team
- [!15259](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15259) Use "replies" over "reverts"

### 2019-01-11
- [!18075](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18075) Update Wave/MQ section with MP suggestion
- [!18066](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18066) Fix link name for Terraform Automation design doc
- [!18060](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18060) Updates for new Data Team group
- [!18052](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18052) handbook: align corporate marketing ordered lists to styleguide
- [!18047](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18047) bonus process changed to CCO
- [!18039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18039) Broken master issues need to be worked on
- [!18031](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18031) Update index.html.md
- [!18026](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18026) Technical Account Management Handbook Updates
- [!18023](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18023) Update wording on wave/mq section
- [!18022](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18022) Add new section on AR page responding to Waves and MQs
- [!18020](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18020) Absorb non-development info from the Looker project README
- [!18015](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18015) Google search console malware process
- [!18013](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18013) Language regarding terminating a people manager and informing the team before...
- [!18009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18009) Fixing broken link on spending-company-money page
- [!18008](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18008) point severity doc link to new location
- [!17995](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17995) Add new FRT Hawk File
- [!17941](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17941) add milestone framework to marketing process
- [!17842](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17842) Add design for deployer
- [!17680](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17680) Refactor email templates ca handbook

### 2019-01-10
- [!18004](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18004) Correct links to livestream section of the handbook
- [!18000](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/18000) Update company policy on conference speaker travel
- [!17994](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17994) Add double-check to ensure Vacancy posted correctly
- [!17985](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17985) Add Chart Operator training to Distribution team training page.
- [!17984](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17984) Jarv/instance groups
- [!17976](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17976) Update to #recruiting channel
- [!17969](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17969) Fix broken link in Engineering Career Development
- [!17966](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17966) Fix link to Create team retrospectives
- [!17822](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17822) Add Casey Allen Shobe to team page

### 2019-01-09
- [!17954](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17954) Updated for comp table and "partially" definition
- [!17944](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17944) Update index.html.md to clarify AMER Tues Crush session -> Casual
- [!17943](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17943) Clean up cm handbook
- [!17942](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17942) Added sec-ops-team and sec-compliance-team tags to the Slack Channels section
- [!17938](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17938) update url for xdr project
- [!17936](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17936) Update DMCA doc
- [!17931](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17931) add board for xdr project
- [!17925](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17925) Updated tech stack for UX team
- [!17924](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17924) Update CHD dates
- [!17922](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17922) infra structure 3rd iter
- [!17920](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17920) add commuter benefits
- [!17917](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17917) More clearly define MRR definition
- [!17903](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17903) fix redirect
- [!17773](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17773) Add Sensing Mechanisms to Product Handbook
- [!17630](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17630) Add link to Community Relations handbook
- [!17183](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17183) Handbook: Support .com: project imports

### 2019-01-08
- [!17906](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17906) SRE team is now Reliability Engineering
- [!17905](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17905) add links to individual projects
- [!17893](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17893) Update index.html.md
- [!17888](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17888) Fix typos on Marketing Handbook page.
- [!17887](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17887) Update email alias list to include analyst relations
- [!17886](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17886) Update André Luís's job title
- [!17881](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17881) Fix one rogue link
- [!17877](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17877) update recruiter/coordinator alignment
- [!17875](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17875) Add post kickoff direction items
- [!17870](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17870) Make 1Password TOTP guide to be up-to-date
- [!17866](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17866) Adding quote coverage definition.
- [!17859](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17859) Update index.html.md
- [!17818](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17818) Dormant Username Policy - Minor Updates
- [!17771](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17771) Add campaign reporting to MPM section
- [!17701](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17701) Adding approval of work-related travel

### 2019-01-07
- [!17846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17846) Resolve "Update data team looker repos links"
- [!17841](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17841) Updated link to the Responsible Disclosure Policy on the security page
- [!17810](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17810) updates to website in handbook with new CTA guidelines
- [!17809](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17809) doc: Correct link on Engineering Workflow doc
- [!17797](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17797) Fixed code review link
- [!17781](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17781) Extract Serverless to separate team page for ops-backend
- [!17623](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17623) Update responsibilities table to include IT Ops Charter items
- [!16637](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16637) Eliminate duplicated words

### 2019-01-05
- [!17817](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17817) Shorten title for better url
- [!17469](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17469) Marketing Project Management

### 2019-01-06
- [!17812](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17812) Update compliance goals in the handbook
- [!17798](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17798) Add anti-mining entry to the Internal Acceptable Use Policy

### 2019-01-04
- [!17808](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17808) fix territory tabls
- [!17792](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17792) Fix broken link typo
- [!17788](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17788) Update source/handbook/communication/index.html.md
- [!17778](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17778) Add label definition
- [!17748](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17748) Add Tech Stack Support Heirarchy
- [!17653](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17653) Add Secure Data NDA note to Data section
- [!17478](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17478) Rename Competitor to DevOps Tool
- [!17431](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17431) Removed references to major holidays due to comments left in...
- [!17427](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17427) Broaden guidelines for juniors to include more than just engineers
- [!14611](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14611) Update index.html.md - tweak standup wording

### 2019-01-03
- [!17758](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17758) Moved release doc from Quality dept page to release docs
- [!17753](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17753) Update index.html.md - New W-9 2019
- [!17749](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17749) Add source for exchange rate for 2018 Annual review.
- [!17744](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17744) Change 2FA Workflow
- [!17738](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17738) adding some language about utilizing the travel grant and obtaining manager approval first.
- [!17735](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17735) Fix frontend monitor team members page
- [!17728](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17728) Resolve "Update extractor information in the handbook (data team)"
- [!17726](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17726) Resolve "Update data team pointing"
- [!17683](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17683) Added process for license generation
- [!17671](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17671) Add product OKR tracking process

### 2019-01-02
- [!17730](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17730) doc: Fix typo in people ops CoC
- [!17727](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17727) doc: Fix markdown formatting for people-ops CoC
- [!17725](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17725) doc: Fix typo in anti-harassment page
- [!17721](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17721) Added links to SMAU dash
- [!17720](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17720) remove salary source
- [!17717](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17717) doc: Remove outdated Slack Cert mention from tools
- [!17715](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17715) Update index.html.md, added possessive 's to legal language.
- [!17686](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17686) Handbook: Stronger language on Support always replying to customer issues with a link, and why
- [!17563](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17563) Rent index to location factor

### 2019-01-01
- [!17702](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17702) Adding a note about interim responsibilities

### 2018-12-27
- [!17650](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17650) spelling correction

### 2018-12-25
- [!17642](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17642) Add retweeting workflow to CA handbook
- [!17361](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17361) Amend triage process for closed issues

### 2018-12-28
- [!17635](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17635) Handbook updates for docs as SSOT / docs-first: New Documentation section, updated Technical Writing, Support

### 2018-12-22
- [!17625](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17625) Add viable
- [!17601](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17601) Update DB architecture diagram
- [!17025](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17025) Release post - GitLab 11.6
- [!16645](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16645) Add fake phishing or text process.

### 2018-12-21
- [!17613](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17613) Update MPM handbook
- [!17612](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17612) adds overview of how to record a WIR podcast
- [!17607](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17607) Update MPM Handbook
- [!17604](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17604) Remove quick links from categories page
- [!17603](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17603) Enforce some relative links
- [!17600](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17600) Add Cloud Native Engineer regex to Distribution team member list
- [!17595](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17595) Code contributor program handbook update
- [!17591](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17591) Changing sales call to bi-weekly.
- [!17587](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17587) adding a caveat to comp transparency
- [!17585](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17585) updates to handbook for GTM system
- [!17582](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17582) adding pubsec to the territory map section
- [!17577](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17577) Assigning issues
- [!17576](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17576) Updated Access Management Process to include info and use of Baseline Entitlements
- [!17521](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17521) Fixed typo on Invoice Owner
- [!17174](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17174) Updating documentation for `security@` and ZenDesk
- [!17161](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17161) Update security handbook for external contribution process
- [!17059](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17059) Add text for ~"security request"
- [!16139](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16139) Update priority board link for Manage team

### 2018-12-20
- [!17580](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17580) update outdated schedule
- [!17558](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17558) Update index.html.md
- [!17557](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17557) Add frontend theme call theme for January
- [!17554](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17554) verify shorter
- [!17524](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17524) Updated reddit workflow
- [!17518](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17518) Update source/handbook/marketing/blog/index.html.md
- [!17513](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17513) Update index.html.md - fix typo (missing word 'to')
- [!17499](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17499) Guidance on activities to ensure direction/vision/roadmap are updated
- [!17498](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17498) Fix internal link
- [!17367](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17367) IT Operations Charter for Discussion
- [!17365](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17365) Clarify Retention Definitions

### 2018-12-19
- [!17522](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17522) changing quote-help to deal-desk chatter group.
- [!17519](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17519) Update community response channels
- [!17517](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17517) Update Community Response channels
- [!17512](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17512) Remove the due-22nd label
- [!17510](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17510) Current rate but adjust once a year.
- [!17505](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17505) update territory ownership table
- [!17504](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17504) Adding New Hire
- [!17502](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17502) Updating methods of changing the forecast category and renewal forecast...
- [!17501](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17501) Adding Clari to tech stack list on Business Ops home page
- [!17500](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17500) Adding Clari to Tech Stack
- [!17494](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17494) Reddit handbook update
- [!17493](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17493) Updates to Calc
- [!17468](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17468) Update index.html.md with details on events & template
- [!17462](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17462) Updated typo
- [!17438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17438) Add "we won't do this" as a reason to close an issue
- [!17398](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17398) Add link to multi-tier sales objection handling
- [!17397](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17397) Add mixed-tier objection handling solutions to sales handbook.
- [!17375](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17375) Add stage groups to product categories

### 2018-12-18
- [!17484](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17484) add link to glossary from sclau section
- [!17480](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17480) Fix small typo in follow through section of ux designer handbook page
- [!17473](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17473) Update knowledge-base with Serverless
- [!17471](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17471) gerir/infra/blueprint/storage
- [!17460](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17460) Change Rent Index to Location Factor
- [!17452](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17452) Add Data Quality Process to handbook
- [!17434](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17434) Improve workflow documentation for publishing to the blog
- [!17432](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17432) Updates to the Forecasting Process
- [!17378](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17378) Adds Viable category maturity
- [!17193](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17193) Add Data Quality section
- [!17089](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17089) Update Looker to point to Access management process
- [!16831](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16831) Resolve "Update sheet load description to actually talk about file naming conventions"
- [!16726](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16726) december contributor blog post
- [!16595](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16595) Sfdc internal workings branch

### 2018-12-17
- [!17455](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17455) On this page tam changes
- [!17454](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17454) Fix eng Secure link and promote Security to General section of hb index
- [!17442](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17442) remove sales@ mention
- [!17433](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17433) [Communication]: Add a section about asking "is this known"
- [!17425](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17425) Add Manage team repository to team product page
- [!17422](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17422) Update notebook specs
- [!17293](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17293) adding wbso process
- [!17227](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17227) Add review step

### 2018-12-15
- [!17420](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17420) change online growth to digital marketing programs
- [!17382](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17382) Update source/handbook/marketing/marketing-sales-development/marketing-programs/index.html.md

### 2018-12-14
- [!17413](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17413) Propose new workflow for follow through of issues, remove sections that speak...
- [!17410](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17410) Update evangelist program page
- [!17393](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17393) Update source/handbook/values/index.html.md
- [!17385](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17385) Adding DataFox Details
- [!17287](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17287) Change file name in the handbook for homepage banner
- [!17278](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17278) Add other functionality to Secure
- [!17220](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17220) Hackathon page update
- [!17210](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17210) Add DevOps stages icons to release posts
- [!17198](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17198) Rename team to stage for release post items
- [!16795](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16795) fix broken links for frontend handbook
- [!16640](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16640) Adding defining problems before solutions to efficiency values
- [!16639](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16639) Add results value of uncertainty
- [!15393](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15393) Add design document for Infrastructure Git Workflow

### 2018-12-13
- [!17381](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17381) Update source/handbook/marketing/marketing-sales-development/index.html.md
- [!17364](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17364) add smb ownership to table
- [!17357](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17357) Add new campaign type details
- [!17356](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17356) update timelines
- [!17344](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17344) Update Bitergia info.
- [!17333](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17333) Replace Internal Review with Stakeholder Collaboration
- [!17329](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17329) added on call SLA note
- [!17323](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17323) Writers are encouraged to import their blog posts to personal Medium accts
- [!17309](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17309) Provide guidance on Product presentations to CAB
- [!17298](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17298) Add maximum price for non-Mac laptops
- [!17286](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17286) Fix 2 internal pagelinks.
- [!17275](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17275) Update users' URL on gitlab.com
- [!17264](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17264) Add interning process recommendations
- [!17235](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17235) Add breaking change flow
- [!17201](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17201) Add the second template for  Supporting Community initiative

### 2018-12-12
- [!17328](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17328) GDPR Updates
- [!17318](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17318) recruiting custom fields
- [!17315](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17315) link picture back to original framework image and remove broken links
- [!17313](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17313) greenhouse permissions clarify who gets what
- [!17310](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17310) Additional language to internal transfers
- [!17304](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17304) Beamy how to additions.
- [!17300](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17300) Add format to the top of categories.yaml
- [!17261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17261) Clarify social request process
- [!17255](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17255) add experience factor worksheet to the promotion process
- [!17215](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17215) Update Product Handbook to include internal review of planning and roadmaps
- [!17209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17209) Add personal stories page about remote work
- [!17082](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17082) updated internal links that were using JS redirects
- [!16921](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16921) Handbook: Support: update issue prioritization

### 2018-12-11
- [!17258](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17258) CMM handbook update
- [!17248](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17248) Remove accidental left-over word
- [!17244](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17244) change online growth to digital marketing programs
- [!17242](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17242) greenhouse org changes site admin
- [!17237](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17237) Add credit card details to CA handbook
- [!17234](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17234) Added new process for MDF credit
- [!17228](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17228) Update handbook instructions for handling suspected phishing attempts
- [!17224](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17224) Update title regex for Manage team FEM
- [!17222](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17222) Update index.html.md
- [!17206](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17206) update field event statuses
- [!17194](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17194) Serverless FAQ
- [!17145](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17145) Added a section about beta testing
- [!17111](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17111) Add knowledge sharing Deep Dive guidelines to handbook
- [!17090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17090) GDPR Article 15 Workflow Updates
- [!17085](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17085) Turn category vision suggestion into a markdown template.
- [!16913](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16913) canceling future interviews for rejected candidates
- [!16873](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16873) adding C1-C3 examples to change requests

### 2018-12-10
- [!17213](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17213) Support handbook minor fixes
- [!17204](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17204) correct link to offers page
- [!17199](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17199) Fixed typo
- [!17187](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17187) Update Growth FEM to @dennis
- [!17094](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17094) Fix www.about problems
- [!16933](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16933) Add more visibility on the growth team
- [!16932](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16932) Updating how Sales Segment on Lead is populated
- [!16836](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16836) workflow support: add admin note page
- [!16537](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16537) Adding the additional process of how to mass add contacts and add them as…

### 2018-12-07
- [!17168](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17168) add nextravel to tech-stack
- [!17167](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17167) Change the speakers to Zendesk
- [!17162](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17162) Update merchandise.html.md - Fixed "list of countries we do not do business in"
- [!17159](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17159) modify apac/latam mm
- [!17156](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17156) Add description of team member laptop buyback to offboarding page
- [!17149](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17149) modify territory mpas
- [!17148](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17148) Update index.html.md
- [!17147](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17147) Update index.html.md
- [!17146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17146) Update index.html.md
- [!17142](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17142) adding instructions
- [!17127](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17127) Fix the broken link to the ceo pricing page
- [!17113](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17113) Adds a network architecture diagram
- [!17068](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17068) Updating Offboarding Process
- [!16993](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16993) Add note to distribution triage steps about follow up issues

### 2018-12-09
- [!17166](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17166) Removed Sync areas from Manage "other functionality"

### 2018-12-06
- [!17124](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17124) Specify that Delivery does .com releases
- [!17114](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17114) Update Coaching section
- [!17098](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17098) Whole mumbers
- [!17097](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17097) Details on EE license.
- [!17092](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17092) Added the video recording requirement for the Stage Vision page.
- [!17087](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17087) Fix broken link & typo
- [!17053](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17053) Link terraform automation design doc from index
- [!17046](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17046) Added Sync team
- [!17042](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17042) Update GitLab Status Twitter account workflow
- [!16877](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16877) Added prioritization values to Manage page
- [!16853](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16853) Create a new page as a collection of useful links
- [!16805](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16805) Quick reference for common workflows

### 2018-12-05
- [!17088](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17088) Update mpm handbook with calendar for email marketing
- [!17079](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17079) Handbook: support: 2FA add note
- [!17076](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17076) move "other security topics" to bottom + add section statement supporting compliance
- [!17073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17073) update how to add fed financial check to bgc
- [!17066](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17066) Split Gifs section
- [!17064](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17064) Add key to explain product role acronyms
- [!17063](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17063) Update youtube/index.html.md to fix typos
- [!17019](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17019) Add Product OKRs for 2019 Q1
- [!16910](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16910) Adjust office spending caps to better align with market values

### 2018-12-30
- [!17078](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17078) Geo Product Vision Page

### 2018-12-04
- [!17035](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17035) update salary field as optional with examples of when to use it
- [!17033](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17033) add link to doc, instructions for new access-request, and description of provisioner
- [!17024](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17024) add new source
- [!17018](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17018) add speaking sess to scoring
- [!17013](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17013) Rename Dev Prod to Eng Prod per industry standards
- [!16985](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16985) Adding team members and counterparts
- [!16960](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16960) Reverting sync addition to category page
- [!16949](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16949) Add on-call participation as reasoning for mobile reimbursement
- [!16916](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16916) Resolve "Add SOW scoping details to CS handbook"
- [!16876](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16876) Handle one-license-per-institution EDU rule on a case-by-case basis
- [!16874](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16874) Automatically compress PNG images
- [!16649](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16649) Removed sections referring to deprecated Risk Assessment Process
- [!15821](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15821) change formatting on blog analysis
- [!15719](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15719) Add marketing coordination

### 2018-12-03
- [!17010](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17010) Add internal customers section
- [!17000](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17000) Fix wrong label mention for blog posts
- [!16999](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16999) adding spain and ireland
- [!16997](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16997) adding cxc payroll change process
- [!16995](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16995) Probationary Periods
- [!16988](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16988) Benefits: SafeGuard and CXC
- [!16987](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16987) Updated extension for devops-tools workflow page
- [!16935](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16935) Improve CA workflow and update CA handbook
- [!16904](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16904) Adds sentry deck to support handbook
- [!16897](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16897) Update source/handbook/finance/department-structure/index.html.md,…
- [!16883](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16883) Newsletter Updates to Handbook

### 2018-12-01
- [!16958](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16958) Markdown Layout Modification
- [!16924](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16924) Update 404 URL to correct link

### 2018-11-30
- [!16957](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16957) update greenhouse - background checks, contracts, offers
- [!16953](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16953) Update source/handbook/marketing/product-marketing/messaging/index.html.md
- [!16950](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16950) Reworded reassurance sentence for paging security
- [!16946](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16946) sync team to categories page
- [!16941](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16941) Update index.html.md to describe our first Support week-in-review iteration
- [!16939](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16939) Add suggestions for labeling product categories epics
- [!16928](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16928) Update source/handbook/ceo/pricing/index.html.md
- [!16915](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16915) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!16914](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16914) Create spending company money examples page
- [!16852](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16852) Remove Support Turbos & Breach Hawks
- [!16845](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16845) Add reference for important dates.
- [!16803](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16803) Add instructions about analyst engagement for product team
- [!16761](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16761) Add Growth to product categories
- [!16760](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16760) SMAU introduction.

### 2018-11-29
- [!16906](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16906) updated for group conversation livestream
- [!16905](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16905) Changed devops-tools.md > devops-tools.html in index
- [!16903](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16903) update initial source table
- [!16900](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16900) Adding East SDR and fixing 2 spelling errors.html.md
- [!16899](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16899) Update source/handbook/business-ops/index.html.md
- [!16895](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16895) "Quality" is a Department
- [!16887](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16887) Add blog templates to handbook
- [!16881](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16881) changed SDR Southern Europe to TBH to Anthony Seguillon
- [!16868](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16868) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!16858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16858) Moving Security under the Engineering section of the TOC
- [!16835](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16835) Add `gitlab-elasticsearch-indexer` to Plan's functionality
- [!16811](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16811) Add Technical onboarding (workflows) doc for Secure
- [!16792](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16792) Move OSS project to Community Relations subgroup
- [!16782](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16782) Workflow support: add account/project change workflow
- [!16765](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16765) Adjusts instructions for updating release post manager

### 2018-11-28
- [!16856](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16856) Bgc 18
- [!16844](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16844) update link to 2019
- [!16842](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16842) Greenh offers
- [!16838](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16838) Added to section on how to request a field event.
- [!16833](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16833) Create is the point of contact for shell and workhorse
- [!16832](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16832) Include MVC issue and improvement epics in category vision
- [!16826](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16826) remove reference to sales@
- [!16822](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16822) Add "Team Meetings" calendar instructions
- [!16821](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16821) Break 'announcing terminations' into more paragraphs
- [!16818](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16818) 401(k) funding process
- [!16816](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16816) Added the DevOps Tools page to the community response channels and documented workflow
- [!16812](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16812) Improve screenshot compression guide
- [!16810](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16810) Added Index and fixed formatting issues
- [!16808](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16808) Move Territory info
- [!16807](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16807) Updated current object storage state in DR Design Doc
- [!16742](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16742) Add "What to work on" section for Create backend team
- [!16715](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16715) Remove incentive
- [!16654](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16654) Adding SSO exclusion from prohibition and added examples of removable media reference in the AUP
- [!16286](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16286) Outlines different interactions with security
- [!16026](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16026) Specify interface from Prioritization (PM) -> Execution (EM)

### 2018-11-27
- [!16798](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16798) update new orleans summit invitation letter
- [!16788](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16788) Update index.html.md. Add LittleSnitch Firewall as recommended software
- [!16737](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16737) Reformat EDU/OSS part of the CA handbook
- [!16700](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16700) Added a section for the Production Change Lock (PCL)
- [!16192](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16192) PII workflow
- [!15647](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15647) Terraform automation design doc

### 2018-11-26
- [!16756](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16756) update company call to new platform
- [!16750](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16750) Update index.html.md to add amendment section
- [!16718](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16718) Minor spelling corrections in 360 feedback page
- [!16708](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16708) Update Engineering Alignment
- [!16706](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16706) Update Support process for getting an on-call Engineer's attention
- [!16697](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16697) Fix some onboarding links after content was moved
- [!16694](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16694) create recruiting process framework section, update hiring links

### 2018-11-23
- [!16741](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16741) UX Research handbook updates
- [!16717](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16717) Handbook: support pages reorg attempt
- [!16671](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16671) Repeat the header for each stage in the maturity page
- [!16601](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16601) Document best practices for cross-domain-area collaboration
- [!16502](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16502) Add Global Maps & Territory Assignment
- [!16125](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16125) handbook: support workflow dormant process make uptodate

### 2018-11-22
- [!16722](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16722) Remove lingering references to CI/CD ops-backend team page
- [!16667](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16667) Handbook update for hackathon
- [!16318](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16318) added the OPS team will create a separate account in Salesforce instead of can do it ourselves.
- [!16194](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16194) add additional inquiry types

### 2018-11-21
- [!16705](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16705) Update reseller marketing kit page to include security
- [!16699](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16699) Fixing the broken links to Engineering Manager job family
- [!16691](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16691) Support handbook deep dive section
- [!16672](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16672) Update improving our processes section
- [!16593](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16593) Avoid offset meetings
- [!16588](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16588) Updated priorities for UX designers
- [!16388](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16388) Adding re-instating blocked accounts workflow  fixes https://gitlab.com/gitlab-com/support/support-team-meta/issues/1319

### 2018-11-20
- [!16683](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16683) Added missing step to contract instructions.
- [!16681](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16681) Update source/handbook/marketing/product-marketing/tiers/index.html.md
- [!16680](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16680) Update source/handbook/finance/sales-comp-plan/index.html.md
- [!16678](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16678) included SRE onboirding edits
- [!16676](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16676) Update community response channels overview
- [!16670](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16670) Fix link to Maintainer/Reviewer
- [!16663](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16663) Update index.html.md
- [!16662](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16662) updated laptop policy
- [!16658](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16658) fixes to Infra main page for Other Pages section
- [!16643](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16643) Minor spelling updates to global compensation page
- [!16634](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16634) Improve onboarding page.
- [!16633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16633) Bringing the oncall page up-to-date.
- [!16613](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16613) Add note about announcing CE/EE settings changes
- [!16582](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16582) Added page on feature instrumentation

### 2018-12-08
- [!16666](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16666) Add best practice for transitioning 1-1s to new managers

### 2018-11-19
- [!16655](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16655) Clarified last step
- [!16653](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16653) mention instead of assign
- [!16648](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16648) three interviews per day, other scheduling edits
- [!16644](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16644) Update source/handbook/finance/department-structure/index.html.md
- [!16622](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16622) fix broken images
- [!16579](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16579) Added access control procedures to access management section - access removal timeframe for review by PeopleOps
- [!16510](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16510) Define maturity stages
- [!16196](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16196) Tam Handbook - Current Upgrading Customers
- [!15714](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15714) Update DMCA workflow

### 2018-11-16
- [!16608](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16608) add EKS setup file
- [!16602](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16602) how to set up email permissions and send on behalf of someone else
- [!16598](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16598) add new campaign channel
- [!16591](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16591) Updating CXC
- [!16583](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16583) Update handbook links
- [!16572](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16572) Added Acceptable Use Policy Blurb to Code of Conduct page
- [!16571](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16571) Local update team page
- [!16570](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16570) Update index.html.md revised link to security deck to point to new Nov 2018 deck
- [!16547](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16547) Jfinottochange2
- [!16516](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16516) Add security to our workflow
- [!16506](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16506) Fixing a few more places where FGU is still referenced
- [!16471](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16471) Resolve "Add pointing details to the data team page"
- [!16440](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16440) Update index.html.md.erb added to solution definition.
- [!16435](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16435) Add Combine consistency and agility to Leadership Page
- [!16421](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16421) Incident review process
- [!16217](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16217) first iteration of Infra OKR blueprint

### 2018-11-15
- [!16576](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16576) clarify sharing social links and referrals
- [!16575](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16575) Jj pmmhandbook1
- [!16574](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16574) Adding AUPolicy Link to Other Resources for Gitlabbers section
- [!16573](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16573) ak-pmm-responsibilities
- [!16567](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16567) Move pm content
- [!16565](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16565) Update index.html.md
- [!16563](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16563) ak-add-pr-link
- [!16562](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16562) open enrollment
- [!16554](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16554) add cigna information
- [!16545](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16545) adding thumbs up
- [!16541](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16541) Updated Error Budget description
- [!16536](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16536) Update 1:1 index.html.md subordinate -> individual contributor.
- [!16535](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16535) fix spelling
- [!16533](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16533) Point tech interview link to deep link instead of redirect
- [!16515](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16515) fix dormant username links in handbook
- [!16514](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16514) wording edits to DMCA workflow
- [!16513](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16513) Remove duplicate content between Job Families and Hiring pages
- [!16501](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16501) Add images and updates to MPM handbook
- [!16494](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16494) Update Product Handbook page to be explicit about dogfooding tracking activities.
- [!16480](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16480) Ak split pmm hbook
- [!16457](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16457) Restructure planning and roadmaps
- [!16430](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16430) Resolve "Internal channel convention"
- [!16360](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16360) Changing "Master" to "Maintainer" in Permissions section
- [!16304](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16304) Maintainer mentorship
- [!16281](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16281) Add how to become a maintainer for frontend

### 2018-11-14
- [!16508](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16508) Integrate Applying page into Jobs FAQ
- [!16505](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16505) Low context communications - Handbook update
- [!16504](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16504) Update PO process
- [!16503](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16503) Create post-interview instructions page
- [!16499](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16499) Add experience factor definitions for new backend engineer role
- [!16498](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16498) add images
- [!16496](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16496) update vacancy process to include recruiting tasks
- [!16495](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16495) Minor nit fixes while reviewing /handbook/product/categories
- [!16492](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16492) Made changes based on discussion with Jamie
- [!16490](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16490) Update Manage FE page
- [!16489](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16489) Added TOC per handbook style guide
- [!16488](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16488) Revert "Update index.html.md"
- [!16486](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16486) Updated to include more thorough guide for using ContractWorks
- [!16474](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16474) Update suggested email intro template
- [!16463](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16463) Cleanup database team page.
- [!16438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16438) Update frontend themed call results and add next theme
- [!16436](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16436) Update retrospective
- [!16408](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16408) add process for administering secret snowflake
- [!16273](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16273) Update Metrics Meeting to include decision and action tracking in the presentation
- [!16243](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16243) move page instructions

### 2018-11-13
- [!16446](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16446) Add reference for Kenny Johnston.
- [!16445](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16445) Fix TOC on internal AUP page
- [!16443](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16443) Adjust Ops Product Leadership
- [!16439](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16439) Creation of new AUPolicy Page
- [!16434](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16434) Add a section about growing in Transparency to my README
- [!16431](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16431) Fix capitalisation of "Apple Watch" and "Apple Pencil"
- [!16428](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16428) updating candidate information in greenhouse
- [!16419](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16419) Update MVP announcement channel to #release-post
- [!15001](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15001) Design Doc for Disaster Recovery

### 2018-11-12
- [!16405](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16405) Continued work on /handbook/hiring/interviewing
- [!16391](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16391) Update MVP process
- [!16382](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16382) Values consistency and efficiency

### 2018-11-10
- [!16373](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16373) Edit security handbook 2FA guidelines
- [!16371](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16371) Adding high-level overview for new delivery team
- [!16370](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16370) Update index.html.md

### 2018-11-09
- [!16369](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16369) Add team pages for Release and Verify, remove CICD
- [!16366](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16366) add public sector contact form to scoring
- [!16362](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16362) Add slack alias to Monitor team page
- [!16359](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16359) Update security best practice 2FA
- [!16357](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16357) Add EDW security requirements to Data & Analytics
- [!16356](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16356) Jj xdr coaching page
- [!16349](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16349) Provide background for Engineering communication
- [!16340](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16340) Merge branch 'update-support-channels' into 'master'
- [!16339](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16339) Jj typos3
- [!16334](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16334) fix: Update typo

### 2018-11-08
- [!16337](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16337) Hiring handbook section rework - pt. 2
- [!16328](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16328) Removing PO approval for direct customers since all direct customer POs are null…
- [!16326](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16326) Fix #group-conversations Slack channel name
- [!16323](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16323) Re-organize /handbook/hiring around target audiences & function
- [!16320](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16320) link to Chorus training instructions
- [!16315](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16315) Fix broken link
- [!16312](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16312) removed extra -
- [!16301](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16301) Add implementation details about throughput
- [!16300](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16300) Adding new page for Chorus
- [!16299](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16299) Adding Chorus to the Tech Stack
- [!16298](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16298) clarify List Imports
- [!16296](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16296) Fix Licenses page link
- [!16284](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16284) Clarifies the role of Support Team at GitLab
- [!16283](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16283) Details how to respond to objections to the technical assignment
- [!16256](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16256) Draft a policy for engaging with comments and users on YouTube
- [!16252](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16252) Reorganize the sales team's front page in the handbook
- [!16251](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16251) Update on the company expenses
- [!16170](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16170) Developer productivity  to be gitlab-docs maintainer
- [!16057](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16057) Add merch complaint instructions

### 2018-11-07
- [!16280](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16280) Update support team slack channels
- [!16268](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16268) Add Plan guidance on picking an issue to work on
- [!16265](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16265) removed links to individual sequences, redundant, add Gold trial nurture issue
- [!16263](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16263) update bgc policy to include sterling
- [!16262](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16262) tos to people ops
- [!16257](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16257) Add style for current post
- [!16247](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16247) Added note about infrastructure level approval for DB and server-level access
- [!16238](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16238) Adding a space - just one
- [!16193](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16193) Add throughput labels
- [!16152](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16152) clear up messaging on CE/EE vs pricing tiers
- [!16062](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16062) Update ee language
- [!15619](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15619) Updated UX Research processes

### 2018-11-17
- [!16253](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16253) Support Handbook Combing Updates

### 2018-11-06
- [!16250](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16250) add notification config to greenhouse and update offer package process
- [!16245](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16245) add Google Search Console and Google AdWords to tech stack
- [!16239](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16239) Linked to Sales Process for contracts
- [!16236](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16236) Updated slide deck link
- [!16227](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16227) Fix Distribution team members
- [!16225](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16225) Fix a broken link in the 'Developer productivity team' page
- [!16223](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16223) Change ap@gitlab.com to payroll@gitlab.com for contractors
- [!16222](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16222) Update index.html.md with gating asset procedure
- [!16218](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16218) Update source/handbook/finance/operating-metrics/index.html.md
- [!16212](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16212) Changed page title in link
- [!16158](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16158) Integrate CEO-101 FAQs into Handbook
- [!15882](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15882) Clarifying support policy for short-duration OOO
- [!15628](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15628) Add enablement instructions to the handbook
- [!15554](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15554) Minor Support Handbook Updates

### 2018-11-05
- [!16209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16209) Removed Group Conversations deck template link
- [!16205](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16205) Rename file with proper extension and move to proper folder
- [!16188](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16188) Fix link to the patch release template
- [!16157](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16157) Add team meeting issue review process to Support Handbook
- [!16122](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16122) Move social hb
- [!15735](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15735) adding abuse incident section
- [!15702](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15702) Added process for contracts
- [!14589](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14589) Add tech writers to product categories

### 2018-11-03
- [!16183](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16183) cleanup of infra struct png
- [!16182](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16182) infra teams + org structure diagram
- [!16176](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16176) Fix PMM pages
- [!16172](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16172) Correct spelling error - Update source/handbook/leadership/index.html.md

### 2018-11-02
- [!16167](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16167) Update VP-Group Definitions - source/handbook/leadership/index.html.md
- [!16150](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16150) Update the number of pages in the handbook to 2k
- [!16143](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16143) update to ultimate, add links
- [!16133](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16133) Update index.html.md revised competitive app sec page
- [!16132](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16132) Update index.html.md revised PMM handbook page
- [!16130](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16130) Transpose the maturity data
- [!16129](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16129) Update index.html.md revise competitive marketing page links to app sec
- [!16128](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16128) Change exploring to implementing throughput.
- [!16127](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16127) Small spelling corrections on the EA index page
- [!16126](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16126) over 2000 pages in the handbook!
- [!16124](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16124) fix progression order
- [!16096](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16096) Junior developers -> junior engineers
- [!16092](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16092) Resolve "Update 'how to become a maintainer' to add new guidelines"
- [!16081](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16081) Add category epics to product handbook
- [!16015](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16015) 2nd iteration on service levels and error budgets framework blueprint
- [!14734](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14734) Add SDR deck

### 2018-11-01
- [!16121](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16121) update list import information
- [!16114](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16114) Update guidelines for handbook hierarchical structure
- [!16113](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16113) Quick markdown formatting fix
- [!16112](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16112) greenhouse additional resources
- [!16105](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16105) Updated Manage board links for FE and BE
- [!16100](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16100) Add maturity to categories
- [!16094](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16094) Update index.html.md removed paragraph explaining difference of roles and…
- [!16093](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16093) Update index.html.md moved roles vs persona definition here.
- [!16090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16090) add auto-tag info
- [!16089](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16089) Update fix typo
- [!16085](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16085) update broken link
- [!16082](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16082) Add DMCA workflow reference to security handbook
- [!16078](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16078) Update index.html.md removed dates and fixed formatting
- [!16069](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16069) Resolve "Update SA Triage process"
- [!15474](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15474) add tables of requests
- [!15471](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15471) conference policy
- [!14726](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14726) Adds a better explanation about inclusive interviewing.

### 2018-11-04
- [!16116](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16116) Adds issue #82 report to the research archive

### 2018-10-31
- [!16075](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16075) Update source/handbook/marketing/product-marketing/index.html.md
- [!16074](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16074) Summit leadership training integration into primary summits page
- [!16072](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16072) right link for STD form
- [!16068](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16068) Update fix font on report links
- [!16064](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16064) update payroll procedures
- [!16061](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16061) Update EE language
- [!16058](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16058) Reformat Info on the Page
- [!16055](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16055) Add link to dependency license pages to distribution handbook
- [!16053](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16053) Add next frontend themed call
- [!16051](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16051) Consolidate Opportunity information to Business OPS section
- [!16041](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16041) Rearrange handbook handbook table of contents
- [!16038](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16038) Change "remote-only" folder name to "all-remote" and fix related links
- [!16035](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16035) Update analyst handbook page to include Thought Leadership section
- [!16009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16009) Test automation career path
- [!16001](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16001) Define order of what page to link
- [!15627](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15627) Update direct deal PO process

### 2018-10-30
- [!16040](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16040) More minor handbook updates
- [!16039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16039) Ensuring people know that don't ask, doesn't mean don't tell for Time off
- [!16036](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16036) comp calc may be different after annual review
- [!16033](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16033) Add Data Integrity Policy
- [!16018](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16018) Update source/handbook/marketing/marketing-sales-development/field-marketing/index.html.md
- [!16013](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16013) Add info about expensing VPN
- [!16010](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16010) blueprint: service levels and error budgets
- [!16007](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16007) Still interviewing but not accepting new applicants = Draining
- [!16006](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16006) Updating the recruiting alignment to include Kike
- [!16002](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16002) Add social native activities
- [!15995](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15995) Update _categories.md.erb
- [!15817](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15817) Update general guidelines index.html.md into subheadings

### 2018-10-29
- [!16004](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/16004) links need new target
- [!15990](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15990) Stating that medical related time off should also be communicated and moved to STD after 25 days
- [!15988](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15988) AmEx policy update.
- [!15986](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15986) Quickly removing a comma
- [!15984](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15984) Document manager as being the person who manages design tooling licenses
- [!15983](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15983) update greenhouse page and vacancy opening paragraph
- [!15976](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15976) Editorial style update
- [!15975](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15975) Add note on submitting minimal Expensify reports to avoid excessive fees
- [!15966](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15966) Link to new issue in Alliances project with template pre-selected
- [!15924](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15924) update interviewing page
- [!15865](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15865) Resolve "Add information for customer communication to TAM Handbook"
- [!15858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15858) minor handbook support add link to feedback template
- [!15830](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15830) Add section on scheduling guideline and code review/submission
- [!15825](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15825) Update needs org workflow
- [!15763](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15763) support dormant username workflow update
- [!15323](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15323) Add design for GitLab CICD.

### 2018-10-28
- [!15958](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15958) Add alt links

### 2018-10-27
- [!15952](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15952) updating dotcom escalation point
- [!15703](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15703) Resolve "Create page for Legal process for Salesforce Negotiations"

### 2018-10-26
- [!15940](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15940) Update source/handbook/business-ops/index.html.md to add link to help issue template in _issues.help
- [!15931](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15931) Kl update crp
- [!15925](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15925) remove headcount spreadsheet until finalized
- [!15920](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15920) reorder tech stack to alphabetical order
- [!15915](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15915) iteration on delivery team addition
- [!15911](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15911) Add link to slack channel for CS team
- [!15910](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15910) Share as much as is useful.
- [!15897](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15897) add new campaign types& defs
- [!15896](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15896) Use the correct title for Delivery team EM
- [!15894](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15894) Resolve "Move pages to community section"
- [!15891](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15891) Resolve "Move feature pages to products"
- [!15888](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15888) correction to scoring definitions
- [!15886](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15886) Resolve "Move team pages under /company/"
- [!15873](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15873) add payroll changes doc
- [!15861](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15861) Need to fill out fields in SFDC: Expected Product and Expected Number of User in Stage 1 or Later
- [!15857](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15857) Add SPD/Fee Disclosure to the Hanbook
- [!15826](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15826) KL_Update source/handbook/marketing/product-marketing/index.html.md
- [!15788](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15788) Working on unscheduled issues for Plan team members
- [!15772](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15772) minor edits to the support onboarding page
- [!15669](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15669) Update Developer role to Backend Engineer
- [!15593](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15593) Adding Visual Compliance to the tech stack and also notes around opportunities…
- [!15302](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15302) Updated Manage page - team and unscheduled items
- [!15042](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15042) add note to carry over not-merged items for kick-off

### 2018-10-25
- [!15883](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15883) Molly add issue board
- [!15881](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15881) Minor handbook updates to formatting
- [!15880](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15880) From led to proposed
- [!15878](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15878) add training link
- [!15872](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15872) Security runbook updates
- [!15864](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15864) add information on manager input form
- [!15854](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15854) Resolve "Move find a speaker page"
- [!15846](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15846) Clarify image crediting
- [!15843](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15843) compress common links; split mission and vision
- [!15839](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15839) Resolve "Install page updates"
- [!15835](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15835) Add links to people
- [!15833](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15833) Move product categories back to markdown
- [!15832](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15832) Resolve "Move Culture section under Company"
- [!15829](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15829) Added Boston CISO & CIO event details
- [!15824](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15824) update vacancy process in greenhouse
- [!15818](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15818) Resolve "Move topic pages to solutions"
- [!15812](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15812) updated security handbook page with access process information
- [!15799](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15799) Move product pricing page to CEO folder
- [!15791](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15791) Added more info to Data Page in Handbook
- [!15730](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15730) updated access request process note on environments page
- [!15590](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15590) Fix typos and links
- [!15534](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15534) remove outdated info

### 2018-10-24
- [!15823](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15823) modify opp close date.
- [!15815](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15815) update close date to align w/ sales process
- [!15814](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15814) Added link to Contract approval and procure to pay process
- [!15811](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15811) Add Security Awareness Training provider
- [!15808](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15808) Offer Signatures to include Recruiting Director
- [!15807](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15807) Added word "contract" up higher.
- [!15805](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15805) Update source/handbook/marketing/product-marketing/enablement/xdr-coaching/index.html.md
- [!15804](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15804) Move instruction for work in progress feature
- [!15803](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15803) Add instruction for proper use of the work in progress feature
- [!15797](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15797) Update the root cause analysis page filename and links to it
- [!15790](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15790) Fix some links in blog handbook
- [!15789](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15789) infra/blueprints cleanup
- [!15784](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15784) Convert "post-mortem" to "root cause analysis" throughout the site
- [!15782](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15782) Add Slack UI tip
- [!15781](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15781) Remove areas
- [!15780](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15780) Resolve "Update /sdlc links"
- [!15779](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15779) fix broken link
- [!15778](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15778) Add map for mileage
- [!15776](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15776) Resolve "Move licensing FAQ"
- [!15767](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15767) Reformatting 'handbook guidelines' under sub-headings
- [!15755](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15755) Make asking for peer reviews a bit more defined by having a minimum stated
- [!15667](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15667) Update Exchange Rate policy for clarity
- [!15650](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15650) align content marketing team to dev and ops integrated teams
- [!15645](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15645) added section about stable counterparts
- [!15610](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15610) Gerir/infra/blueprint/delta
- [!15409](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15409) Markdown guide: how to embed GitLab Snippets in md files
- [!15278](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15278) Add people ops calendar

### 2018-10-23
- [!15775](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15775) Update post event process
- [!15764](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15764) Update add Gartner ARO page link
- [!15758](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15758) Add scheduling info to hb
- [!15757](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15757) Fixes a few typos in distribution eng. manager
- [!15756](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15756) Update links from categories
- [!15754](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15754) Update investigation procedure
- [!15748](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15748) Correct the links for EM for Distribution
- [!15736](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15736) Resolve "Organize support pages"
- [!15721](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15721) Terminology Update
- [!15666](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15666) added postType to definitions and frontmatter
- [!15658](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15658) Remove security examples
- [!15591](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15591) Update product handbook to utilize design system to work autonomously
- [!15348](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15348) Create a team page for Delivery team

### 2018-10-22
- [!15734](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15734) Resolve "Move contact page"
- [!15733](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15733) add more greenhouse info, remove lever page
- [!15726](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15726) clarify account ownership
- [!15725](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15725) added access request directions to tech stack
- [!15724](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15724) update IT roles links
- [!15723](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15723) Update campaign cost tracking in Marketing Ops page
- [!15722](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15722) Add an about page for Marin Jankovski, EM for Distribution and Delivery
- [!15712](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15712) Rename functional-group-updates project to group-conversations
- [!15707](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15707) Resolve "Move company pages under /company/"
- [!15705](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15705) Added finance budget approval
- [!15704](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15704) Resolve "Add security to footer nav"
- [!15685](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15685) Add async standup description to Monitor team page
- [!15677](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15677) Update index.html.md with new MPM Support process for events.
- [!15633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15633) Medium/linkedin handbook update
- [!15631](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15631) Event social process update
- [!15622](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15622) Make the 'Accepting merge requests' workflow consistent
- [!15515](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15515) Resolve "Reconcile stages.yml and capabilities.yml"
- [!15107](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15107) Add cross post section to hb
- [!15078](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15078) Release post - GitLab 11.4

### 2018-10-20
- [!15697](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15697) Resolve "Move cloud image page to handbook"
- [!15696](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15696) Add website naming conventions
- [!15692](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15692) Add links to main slide files under auto plays. Fix intro paragraph position
- [!15472](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15472) Add info about 'Requires e2e tests' and how it is to be used

### 2018-10-19
- [!15690](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15690) correct no toc tags
- [!15686](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15686) add defs to glossary
- [!15681](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15681) fix campaign definitiions
- [!15680](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15680) Update list of Security Department resources.
- [!15676](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15676) add privacy for public exposure
- [!15675](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15675) Update index.html.md
- [!15672](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15672) Add off-line demo instructions
- [!15659](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15659) Add Distribution training session DIS007
- [!15655](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15655) add video
- [!15653](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15653) update details
- [!15640](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15640) remove bad quality desk
- [!15637](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15637) Cigna STD
- [!15507](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15507) Improved UX workflow graphic to better reflect documented dates
- [!15343](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15343) Update link to internal issue tracker for Gold requests
- [!15281](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15281) Quality team structure

### 2018-10-18
- [!15644](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15644) Move cloud native ecosystem page
- [!15643](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15643) Work second.
- [!15642](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15642) add identifying quality post section
- [!15639](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15639) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!15638](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15638) swag updates
- [!15629](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15629) update pre/post-event campaign status deetails
- [!15625](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15625) Resolve "Cloud native enablement page"
- [!15621](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15621) Use community relations vs. community marketing
- [!15615](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15615) Added definition for monthly active group
- [!15609](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15609) update issue board details
- [!15605](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15605) Changing references of WoW to Milestones, adding theme doc and notes about standup and retro
- [!15599](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15599) Resolve "Add demo-ready environment reference for SA's"
- [!15596](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15596) KL_ Update source/handbook/marketing/product-marketing/index.html.md
- [!15329](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15329) - infra planning blueprint
- [!15324](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15324) Resolve "Document blog analysis finding in HB"
- [!15298](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15298) Resolve "Deprecate /sdlc"
- [!14995](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14995) Add team members to the Distribution handbook.
- [!14790](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14790) update tables in Production Labels  for Change Review

### 2018-10-21
- [!15635](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15635) Scheduling security issues process
- [!15276](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15276) Edited link details for sending invoices and executed contracts to ap.

### 2018-10-17
- [!15602](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15602) update link to issue board
- [!15594](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15594) Adding Visual Compliance to our tech stack
- [!15579](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15579) update the link
- [!15578](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15578) add details related to issue board in mktg ops section
- [!15573](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15573) Our Listed Initial Sources are the only supported Initial sources via a validation rule
- [!15570](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15570) Added payroll email address under Communication section
- [!15568](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15568) updated email address for contractor invoices
- [!15563](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15563) Corrected the date formats on this page
- [!15559](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15559) Fixed broken link for support onboarding issue
- [!15556](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15556) add workflow video
- [!15548](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15548) clarifying the need to notify managers of time off.
- [!15537](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15537) Consider tradeoffs of video calls
- [!15535](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15535) Added details on instructions for considering a partnership
- [!15504](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15504) Cblake xdr roles
- [!15469](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15469) Resolve "Move and 301 redirect /comparison/* to /devops-tools/*"
- [!15413](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15413) Resolve "Flesh out data page of handbook"

### 2018-10-16
- [!15552](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15552) Clarify process for updating Gitlab.com status
- [!15547](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15547) debit card
- [!15533](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15533) Updates to sales closing process
- [!15530](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15530) Remove TriNet
- [!15525](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15525) correct mistype in routing rule
- [!15520](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15520) Added planning information to our home page.
- [!15514](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15514) Business OPS handbook updated
- [!15509](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15509) Updated gemstones description
- [!15460](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15460) Reorganize Community Relations handbook
- [!15452](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15452) Update ContractWorks instructions
- [!15385](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15385) Update Acquisition Process Overview: source/handbook/alliances/acquisition-offer/index.html.md
- [!15246](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15246) Changing Benchmarks Based on Role Requirement Changes
- [!14493](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14493) Added MAU definition to operating metrics

### 2018-10-15
- [!15506](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15506) Update URL for Group Conversations (formerly FGUs) and fix broken links
- [!15503](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15503) Named Account Ownership update
- [!15498](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15498) Returning swag typo fix
- [!15493](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15493) Resolve "Add messaging lead to upcoming release posts"
- [!15490](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15490) Add 2018 Timeline for Comp Review
- [!15488](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15488) Cblake patch1015 add content to manager section of roles
- [!15483](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15483) adjust response time
- [!15480](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15480) add eva, fix finance recruiters
- [!15466](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15466) Move and elaborate on product vision
- [!15453](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15453) add holistic UX to ux handbook
- [!15451](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15451) Update PO requirements for closing deals
- [!15388](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15388) FGUs should be renamed to Group Conversations
- [!15361](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15361) add 3 no rule with Dan
- [!15352](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15352) Add Chef Automation design doc

### 2018-10-14
- [!15461](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15461) Expand process for epics

### 2018-10-13
- [!15457](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15457) 20181013 Landing page and Handbook updates
- [!15449](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15449) rmv dup source
- [!15447](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15447) internal control addition -  COA admins
- [!15236](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15236) Resolve "Deprecate sdlc.yml"

### 2018-10-12
- [!15445](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15445) Update directions for asking handbook questions
- [!15440](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15440) Add initial source table to handbook
- [!15437](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15437) Update export
- [!15424](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15424) add code review tag
- [!15422](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15422) HSA Transfer
- [!15418](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15418) create recruiting alingment page
- [!15416](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15416) Group Conversations to replace FGUs
- [!15414](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15414) add best practices
- [!15412](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15412) lever to greenhouse for job families and vacancies page
- [!15410](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15410) Added "Engaging the Security On-Call" section immediately after the "Security…
- [!15407](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15407) Add Admin Notes process
- [!15406](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15406) Updated index.html.md with the Security team on-call section
- [!15405](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15405) Rename backend-retrospectives -> async-retrospectives
- [!15397](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15397) Mainly fixes link in topic about release posts
- [!15395](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15395) updated CW log-in request instructions
- [!15394](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15394) Cblake enterprise IT roles page
- [!15366](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15366) update how ux work is assigned
- [!15364](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15364) KL Update source/handbook/marketing/product-marketing/enablement/index.html.md
- [!15346](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15346) Add community response channels workflow sections
- [!15342](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15342) Distribution Open Work Day page
- [!15339](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15339) update Company Card Policy

### 2018-10-11
- [!15375](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15375) format update
- [!15371](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15371) adding example of participating in live broadcast to transparency value
- [!15367](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15367) Update reports relevant to XDR enablement
- [!15357](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15357) Detail for document process
- [!15356](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15356) Add details re zoom license
- [!15355](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15355) style consistency: deuppercase headings
- [!15351](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15351) Specify "technical debt" as not a "boring" solution
- [!15345](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15345) Auto generate changelog
- [!15338](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15338) live demo instructions in toc and change i2p to p2m
- [!15332](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15332) Update language to move away from "cost of living," which is misleading
- [!15331](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15331) Update Gartner information on other reports
- [!15326](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15326) first iteration to GitLab Access Requests Guidelines
- [!15320](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15320) Wrong support channel listed
- [!15274](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15274) adding instructions to optimize images
- [!15261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15261) Updates from 2018-10-09 at risk triage
- [!14779](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14779) GitLab.com stability update

### 2018-10-10
- [!15328](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15328) swag updates
- [!15325](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15325) Create a Data and Analytics space in the handbook
- [!15318](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15318) Add community response channels overview
- [!15317](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15317) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!15316](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15316) generalized instructions
- [!15314](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15314) Resolve "preparing confidential blog posts"
- [!15313](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15313) Add latest overview demo
- [!15310](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15310) Added updated links for instructions to ContractWorks.
- [!15309](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15309) Improve Create team page  somewhat
- [!15307](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15307) Updated to line up with new Contract Approval Workflow
- [!15299](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15299) formatting for headers
- [!15293](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15293) xdr enablement Enterprise IT Roles 2018-10-10
- [!15290](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15290) Update dates and descriptions for Forrester reports
- [!15228](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15228) updated Infra FGU responsible person (me)
- [!15202](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15202) Enhance Distribution infrastructure maintenance documentation
- [!15182](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15182) Added step to "Creating a New Page" section
- [!15000](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15000) edits from review apps  to code review and change i2p to p2m.
- [!14931](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14931) Update to Lead Routing based on new rules as of 2018-10-01; removing Strategic segmentation.
- [!14191](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14191) Generate a changelog for the handbook

### 2018-10-09
- [!15289](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15289) fixing link
- [!15288](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15288) rmv outdated tech
- [!15284](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15284) Update remove old forrester content
- [!15279](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15279) Avoid use special characters in branch name that people copy/paste
- [!15268](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15268) Update export info in handbook.
- [!15267](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15267) rmv tech stack details not used any longer
- [!15263](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15263) Chart of Accounts Policy
- [!15255](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15255) add medicare notice to benefits page
- [!15249](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15249) Add Frontend team pages
- [!15248](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15248) Prioritization section for Manage team
- [!15242](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15242) Clarify how to add yourself to team page
- [!15241](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15241) added oct 2018 newsletter
- [!15240](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15240) updated gemstones
- [!15237](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15237) Add information on what to do if someone is working on a ticket that you'd also like to work on
- [!15201](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15201) Fix typo in index.html.md
- [!15178](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15178) Resolve "Create Vendor Contract filing process page"
- [!15171](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15171) Evangelist program page
- [!15058](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15058) Jarv/add canary design

### 2018-10-08
- [!15233](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15233) add send limit to outreach info
- [!15227](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15227) How to credit blog posts
- [!15222](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15222) Clarify criteria
- [!15220](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15220) Why business cards for all.
- [!15215](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15215) Adding information about how to become an interviewer.
- [!15214](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15214) Add LinkedIn
- [!15209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15209) Add License Management report
- [!15191](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15191) update company and fgu call instructions
- [!15188](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15188) updated TAM index page
- [!15139](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15139) Initial CI/CD Infra Blueprint
- [!14841](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14841) Add page outlining disaster recovery concepts.

### 1970-01-01
- [!15203](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15203) Fix typo in index.html.md
- [!14331](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14331) Correct "setup" and "set up" grammar sitewide
- [!13958](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13958) Fix typo in confidential information section

### 2018-10-07
- [!15195](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15195) Fix multiple pages w/ spelling formetting and links

### 2018-10-05
- [!15187](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15187) Add ToC
- [!15181](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15181) Jj xdr coaching
- [!15170](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15170) Update handbook - move XDR content to separate dedicated page.
- [!15168](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15168) Added instructions on how to request access to YouTube
- [!15166](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15166) Update support handbook main page
- [!15165](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15165) Update move content from main AR handbook page to this page.
- [!15162](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15162) 20181005 mpm section updates
- [!15157](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15157) add bizible definitions
- [!15155](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15155) Add link to Marketing Programs
- [!15146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15146) Add Bizible definitions and usage
- [!15145](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15145) update fgu to change names and add engage
- [!15143](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15143) Update fixing some typos, adding dates to reports, adjusting layout
- [!15124](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15124) Updated handbook typo
- [!15116](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15116) #15 Update wording
- [!15073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15073) Update issue escalations workflow
- [!15064](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15064) [Quality] Add a link to the known QA failures
- [!15030](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15030) add blurb around maintaining vision epic/issues up to date.
- [!15015](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15015) Added link to vendor contract approval process
- [!14804](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14804) Geo Planning Board link update

### 2018-10-04
- [!15141](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15141) Resolve "New AR handbook subpage for XDR training"
- [!15132](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15132) Clarify inactive issues - blog hb
- [!15131](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15131) Remove Forrester VSM info, because link to VSM page is here.
- [!15128](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15128) Fix a typo: chang -> change
- [!15126](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15126) Add on-call summary description to SRE handbook page
- [!15123](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15123) Velocity typo and clarification
- [!15119](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15119) add best practices
- [!15111](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15111) Add events calendar
- [!15103](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15103) Modify scoring & record creation
- [!15091](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15091) Updated status info

### 2018-10-03
- [!15100](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15100) fix tool page template and make website edit instructions talk about using it
- [!15092](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15092) Adding a "the" as a candidate example.
- [!15081](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15081) Add professional services rqst details
- [!15080](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15080) Add example of standalone specialist job families
- [!15074](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15074) Added link to Manage playlist
- [!15072](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15072) Add crosslinks between sections talking about ambition
- [!15067](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15067) Updates to CHD
- [!15066](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15066) Added "mandatory" comment on details for event to be added
- [!15065](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15065) Resolve "add workflow to blog handbook"
- [!15063](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15063) add link to email policy in rules of engagement
- [!15061](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15061) Link directly to the template
- [!15053](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15053) Fix ERB templating in ops-backend/configure/index.html.md.erb
- [!15050](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15050) Add email signature to tools-and-tips page
- [!15049](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15049) add MPM details to duties chart
- [!15039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15039) Updating  the 360 page to remove Lattice
- [!15021](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15021) Update hack day instructions
- [!14976](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14976) added detail on trial process
- [!14935](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14935) Added security facet in test planning.

### 2018-10-02
- [!15036](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15036) add link to issue board
- [!15023](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15023) Updating page to include instructions for specialist families
- [!15017](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15017) Add blackout notice to handbook
- [!15009](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15009) Adjustments to alliances handbook
- [!15005](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15005) Update ops-backend/configure/index to match new template
- [!15004](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15004) fixed tiny style typo on EJ README
- [!15002](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15002) ak-move-press-boiler-plate-section
- [!14997](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14997) fix spelling mistakes
- [!14996](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14996) Update Plan backend handbook page
- [!14993](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14993) Fixing empty alliances handbook page and adding inbound request section
- [!14991](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14991) Updated prioritized board link for Manage
- [!14984](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14984) fix dalia's role and update monitor team name
- [!14982](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14982) update blog handbook
- [!14946](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14946) Add quality team’s project management process
- [!14753](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14753) Capabilities are a grab bag.
- [!14652](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14652) updated wording for term departures

### 2018-10-01
- [!14971](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14971) Clarify instructions for adding features
- [!14962](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14962) Fix a few typos in source/handbook/engineering/quality/test-engineering/index.html.md
- [!14960](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14960) Add team members to Monitor team page
- [!14956](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14956) Update handbook for Content Hack Day Q3
- [!14953](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14953) Build individual team pages using data from team.yml
- [!14947](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14947) Tiny grammar fix turned reword for meetup schedule
- [!14943](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14943) Add link to dir readme and update CI/CD names
- [!14940](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14940) Template for team pages
- [!14938](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14938) RACI in README
- [!14933](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14933) Adding the MSA or EULA effective date if the client/prospect will not sign our order form.
- [!14907](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14907) updated Terminus section and swag.
- [!14789](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14789) Update blog handbook
- [!14303](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14303) Fix link + better description for adding blog posts social sharing image

### 2018-09-30
- [!14937](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14937) Management span.
- [!14936](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14936) Emphasizing velocity
- [!14926](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14926) 20180929 mpm section updates

### 2018-09-29
- [!14912](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14912) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14911](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14911) fix URLs without titles
- [!14707](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14707) Clarify that GitLab Gold is available for private accounts
- [!14617](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14617) Added test engineering, plans and process to the Quality handbook

### 2018-09-28
- [!14871](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14871) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14867](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14867) Update swag requests
- [!14862](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14862) Remove arrows from Forrester graphics
- [!14858](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14858) Update source/handbook/tools-and-tips/index.html.md
- [!14857](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14857) Update source/handbook/marketing/product-marketing/competitive/application-security/index.html.md
- [!14852](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14852) Add CFO to approval process
- [!14844](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14844) Resolve "Correcting a grammar mistake in the Gitlab handbook."
- [!14813](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14813) Adding an alliances handbook
- [!14808](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14808) Updates to engagement rules and adding MQLs
- [!14580](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14580) clarifying spending money
- [!14265](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14265) adds gdpr article 15 workflow
- [!14027](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14027) Create "How to Respond to Tickets" Page

### 2018-09-27
- [!14859](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14859) Added detail around "value" to the person.
- [!14851](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14851) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14850](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14850) Updated Email Communication Policy section
- [!14837](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14837) Clarifying that if no level is indicated, it is intermediate
- [!14833](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14833) Update Forrester list to remove CI tools. It's above.
- [!14830](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14830) Update remove ARO from future reports
- [!14824](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14824) Update source/handbook/marketing/marketing-sales-development/sdr/index.html.md
- [!14822](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14822) Wiring Design to Infra main page.
- [!14817](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14817) Add notes about ambitious planning to product handbook and direction page
- [!14816](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14816) Fix "Geo development" link
- [!14807](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14807) Update to include lessons learned in handbook
- [!14805](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14805) update to pro rated language in director comp
- [!14796](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14796) infra/5088: storage nodes design
- [!14784](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14784) initial infra design and template
- [!14296](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14296) Fix broken link to upgrade metrics on distribution page

### 2018-09-26
- [!14788](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14788) Move content marketing to corporate marketing
- [!14777](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14777) Reword Create team description and link to 2019 product vision.
- [!14775](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14775) Adds two extra points to the how to become a maintainer guide
- [!14774](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14774) Remove step to unlabel from Distribution triage documentation
- [!14773](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14773) Update post event f/up to match process
- [!14770](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14770) Links updates and change to APAC call time
- [!14768](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14768) Dalia's readme
- [!14733](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14733) example to clarify stock options for promotion
- [!14608](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14608) Add 'team calendar' section to monitoring team page

### 2018-09-25
- [!14757](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14757) Fix CI/CD typos
- [!14754](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14754) Adjusting the language for transfers to add clarity
- [!14743](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14743) Update source/handbook/git-page-update/index.html.md
- [!14740](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14740) GDPR Workflow Fixes
- [!14730](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14730) Update handbook with training docs from week 1
- [!14723](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14723) Update ops backend configure team page
- [!14714](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14714) Fix links which describe how we decide what is paid and what tier
- [!14713](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14713) Add GCP Security Guidelines Policy link to internal doc in handbook
- [!14712](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14712) Add security-aware dev report to archive
- [!14673](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14673) Rename Community Marketing to Community Relations in documentation
- [!14667](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14667) Fix minor typo on Communication page

### 2018-09-24
- [!14704](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14704) Add dev research report to archive
- [!14702](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14702) Example for candidate on how to edit the handbook
- [!14694](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14694) Move Secret Snowflake page to the People Operations section
- [!14692](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14692) additional wording to internal transfers.
- [!14688](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14688) Update editing paragraph to get rid of mailto
- [!14687](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14687) Update training paragraph.
- [!14686](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14686) Clarify role of backups
- [!14681](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14681) add results from survey page
- [!14676](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14676) Metric Meeting Format
- [!14675](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14675) Update segmentation in Business Ops Section
- [!14672](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14672) Adds GitLab instance survey report under "GitLab.com"
- [!14642](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14642) Add Bora as the OSS Project expert
- [!14614](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14614) Adding HRBP to review discretionary bonus
- [!14558](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14558) Update index.html.md
- [!14402](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14402) Update EDU process

### 2018-09-22
- [!14660](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14660) add demo link
- [!14656](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14656) Minimize pressure to work long hours
- [!13955](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13955) GitLab 11.3 Release Post

### 2018-09-23
- [!14653](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14653) Update source/handbook/finance/operating-metrics/index.html.md

### 2018-09-21
- [!14649](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14649) Added logo specifications to Design System
- [!14644](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14644) Update the email request process
- [!14633](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14633) Add EMEA Support / Sales / CS cross functional meeting
- [!14627](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14627) Fix broken link for Jetbrains license management in "Spending Company Money" section
- [!14625](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14625) documenting secret managemnent
- [!14621](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14621) Add Security Products header and anchor
- [!14618](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14618) Infrastructure Blueprint
- [!14607](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14607) Rollover to Betterment

### 2018-09-20
- [!14601](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14601) non-reimbursement expenses
- [!14600](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14600) Update index.html.md.
- [!14595](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14595) Add support and docs cross functional meeting
- [!14590](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14590) Rename engineering_areas to capabilities
- [!14586](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14586) Clarify experience factor template team member input section
- [!14565](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14565) adds cross functional meetings
- [!14529](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14529) Update:  Adding Abuse team to workflow
- [!14433](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14433) Move Monitor team sections around, remove technology
- [!14403](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14403) Changes to the Geo Team frontpage

### 2018-09-19
- [!14564](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14564) contractor invoice
- [!14562](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14562) add new slack channel
- [!14560](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14560) Add deprecaed doc to the sales enablement training page on PMM part of website
- [!14551](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14551) Add community contributions to product priority
- [!14546](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14546) Update fixing errors by deleting section
- [!14533](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14533) Fix playlist link
- [!14527](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14527) Update Marketing Handbook links
- [!14523](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14523) Fix several issues with communication page.
- [!14517](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14517) Removed link to article that doesn't exist anymore
- [!14502](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14502) Flow one
- [!14498](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14498) handbook: support: add chair, internal requests
- [!14479](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14479) Define the difference between "team leads" and "managers" at GitLab
- [!14449](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14449) Added Vendor Contract process

### 2018-09-18
- [!14508](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14508) Update analyst handbook page with training
- [!14507](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14507) add links to pipe-to-spend links to Update source/handbook/business-ops/index.html.md
- [!14500](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14500) entire devops lifecycle
- [!14495](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14495) Warn about Slack private groups
- [!14486](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14486) fix broken greenhouse link
- [!14484](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14484) remove referral, update greenhouse with zoom, removed sections in lever already in gh
- [!14482](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14482) Remove Robert from the Quality team page
- [!14478](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14478) Update update with education section of analyst reports
- [!14476](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14476) add domestic partner hsa information
- [!14469](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14469) Removing AZ
- [!14467](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14467) Define EULA Acronym
- [!14466](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14466) Add TOC.
- [!14465](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14465) Adding Outreach.io Call disposition info to handbook
- [!14453](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14453) Entire DevOps Lifecycle and 200 percent messaging
- [!14452](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14452) Secure Team - Add YouTube playlist
- [!14450](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14450) Jj removing maturitymodel
- [!14448](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14448) AR Workflow for refunds
- [!14442](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14442) Move redirect setup policy to the handbook-usage page
- [!14333](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14333) Fix link to image, and make Markdown source conform more to standards.
- [!14312](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14312) Explain why we still use "Security Products"
- [!14204](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14204) Fixing a typo in the handbook
- [!14190](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14190) Update the number of contributors to GitLab CE
- [!14128](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14128) Add bullet point in Communication linking to Chat channel page
- [!14037](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14037) Update "hiring SOs/family members" policy to address concerns of privacy

### 2018-09-17
- [!14446](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14446) Add social media profile assets
- [!14441](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14441) Replacing growth marketing with pipe-to-spend
- [!14440](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14440) Fix broken links to the /hiring/roles/ folder
- [!14439](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14439) Fix broken link to success_image
- [!14438](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14438) added sales enablement
- [!14435](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14435) Add new experience factor worksheet with soft skills, team member inputs
- [!14423](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14423) more updates to reseller marketing kit
- [!14420](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14420) KL Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14412](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14412) add inmail issue template Update source/handbook/marketing/marketing-sales-development/online-marketing/index.html.md
- [!14411](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14411) Cigna ID Cards
- [!14409](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14409) Revert "Merge branch 'complete-devops-lifecycle' into 'master'"
- [!14404](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14404) PS Opps must have a Parent Opp
- [!14400](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14400) Reviewers and Maintainers process
- [!14394](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14394) Add instructions for tool logos on home and compare pages
- [!14391](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14391) swag updates
- [!14387](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14387) move capabilities from pricing page
- [!14382](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14382) Update handbook pages via new community contribution workflow
- [!14354](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14354) Updating Barbie's schedule Hours preferences under EA
- [!14344](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14344) Move Secure page to direction
- [!14340](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14340) Add theme for Ocotober frontend call
- [!14316](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14316) Flatten categories and capabilities to make it easier to communicate and reason about.
- [!14197](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14197) Add advocate-for-a-day Slack channel to CM handbook

### 2018-09-16
- [!14390](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14390) moving some swag stuff to just be in corporate marketing
- [!14388](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14388) Update source/handbook/marketing/product-marketing/reseller-kit/index.html.md
- [!14384](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14384) Resolve "Comparison PDF Generation is Broken"
- [!14334](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14334) Update mentions of Complete Devops

### 2018-09-15
- [!14380](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14380) Add notice about adding links to categories and capabilities
- [!14377](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14377) Add DevOps to "Is it similar to GitHub?" message
- [!14352](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14352) Added note about 'partially' flag to 'tools' field.

### 2018-09-14
- [!14367](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14367) 401k Information
- [!14361](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14361) Standardize on complete DevOps lifecycle
- [!14330](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14330) Link into Greenhouse information, remove more references to Lever
- [!14323](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14323) Add pipe-to-spend to Marketing Metrics section in source/handbook/business-ops/index.html.md
- [!14317](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14317) Update content and links to reseller marketing kit
- [!14214](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14214) Make it simpler
- [!14207](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14207) Replacing availability with Team Meetings Calendar.
- [!14161](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14161) Update meltano team roles

### 2018-09-13
- [!14301](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14301) Update Exec preferences
- [!14298](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14298) Minor Manage Team page update
- [!14290](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14290) added manual way process for Non ACH and Non US Residents
- [!14287](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14287) fix image
- [!14283](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14283) moved swag from fm
- [!14282](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14282) moved swag to corp marketing
- [!14279](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14279) Revert note about registry not working with canary env
- [!14273](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14273) Adding notes that Parent account is the Ultimate Parent Account in SFDC
- [!14266](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14266) Add devops interview report to archive
- [!14262](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14262) Update MVC section with thin content details
- [!14242](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14242) Resolve "Update categories to use stub vs name"
- [!14212](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14212) Support workflow: needs-org: add resync
- [!14209](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14209) remove link written over
- [!14152](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14152) Update Supporting Community Individuals CA handbook
- [!14138](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14138) support workflow: update/rename 2FA
- [!14023](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14023) Creating reseller marketing kit page
- [!13996](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13996) handbook: add link to support email workflow

### 2018-09-12
- [!14261](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14261) Changing tone
- [!14258](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14258) Add link to Tommy's README
- [!14250](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14250) Remove hiring order list
- [!14244](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14244) comparison summary instructions
- [!14241](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14241) editing XDR coaching
- [!14236](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14236) Fix services .com support bootcamp link
- [!14234](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14234) fix headers on greenhouse page
- [!14229](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14229) adding XDR coaching to the enablement page
- [!14223](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14223) Update source/handbook/engineering/career-development/index.html.md
- [!14216](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14216) Add partial reimbursement language
- [!14203](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14203) Update field marketing page
- [!14179](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14179) Specifying confidentiality rule for security issues
- [!14137](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14137) adds information request workflow
- [!14127](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14127) Add new structure for Monitoring Team handbook page

### 2018-09-11
- [!14218](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14218) featured blog post instructions
- [!14211](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14211) Eric's Links
- [!14208](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14208) Servant leader
- [!14194](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14194) update referral info for GH
- [!14193](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14193) Updates to Benefits with Lumity
- [!14184](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14184) Never too high, or too low
- [!14181](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14181) clarify website scope and ownership
- [!14180](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14180) Clarifying non-traditional background
- [!14151](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14151) Update source/handbook/engineering/infrastructure/production/index.html.md
- [!14079](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14079) Added some additional language to the initiative part.  Managers and employees…

### 2018-09-10
- [!14175](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14175) clarify prime commuting times
- [!14168](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14168) Greenhouse-commits
- [!14146](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14146) Added retro info to Manage page
- [!14145](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14145) Update zoom info
- [!14143](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14143) Added Functional Approval defintion
- [!14130](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14130) Add sre report to ux archive
- [!14090](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14090) Update security paradigm link
- [!14062](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14062) Update source/handbook/engineering/career-development/index.html.md

### 2018-09-07
- [!14135](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14135) Add Lumity Payroll Processes
- [!14118](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14118) Documenting Iconography as part of our Marketing Design System
- [!14105](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14105) Added interviews with churned users report
- [!14098](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14098) Fix a typo on the Quality page
- [!14086](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14086) adding some language around career development roles and responsibilities for…
- [!14008](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14008) Offboarding Procedures: updating for clarity

### 2018-09-06
- [!14100](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14100) Update index.html.md
- [!14082](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14082) Updates to monitoring page
- [!14080](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14080) Resolve "move case study publishing process to customer reference handbook"
- [!14078](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14078) Rename Product Core Values to Product Principles so it's not confused with company values
- [!14073](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14073) update the campaign member status
- [!14071](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14071) Fixes various links for the public dashboards
- [!14070](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14070) Fix broken arch image and atom is opionated about whitespace/punctuation.
- [!14069](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14069) Add note on manually scheduling coffee breaks. Improve instructions to add new team members via the Web IDE.
- [!14057](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14057) Note how to request an account on Staging. There has been multiple Slack…
- [!14055](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14055) Rename Security Features page to Security Paradigm
- [!14046](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14046) Update source/handbook/marketing/product-marketing/customer-reference-program/index.html.md
- [!14044](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14044) Add social takeover notes
- [!14042](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14042) Update process to be more accurate
- [!13975](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13975) Add security features page

### 2018-09-05
- [!14043](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14043) Fix small typo in source/handbook/engineering/career-development/index.html.md
- [!14040](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14040) crosslink campaign expense policy to finance handbook
- [!14039](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14039) deprecate spreadsheet
- [!14031](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14031) Update source/handbook/values/index.html.md
- [!14022](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14022) reupdate company call
- [!14018](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14018) Add gitlab-ce#48847 to UX Design archive
- [!14012](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14012) Add section on career coaching for engineering, senior dev template
- [!14006](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14006) Career Dev components
- [!14004](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14004) Eric readme
- [!14003](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14003) Add github ci cd faq
- [!14000](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/14000) corrected run-on sentence on spending company money
- [!13999](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13999) Adds research report to research archive
- [!13998](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13998) Add new depts and allocation methodology
- [!13990](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13990) Typo
- [!13970](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13970) Fix links to dashboard graphs

### 2018-09-04
- [!13972](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13972) Rename eShares to Carta
- [!13971](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13971) Pension Edits

### 2018-09-03
- [!13960](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13960) Clarify Sales OPS link to BizOPS