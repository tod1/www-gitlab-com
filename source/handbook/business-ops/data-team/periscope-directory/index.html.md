---
layout: markdown_page
title: "Periscope Directory"
description: "GitLab Periscope Directory"
---

## On this page
{:.no_toc}

- TOC
{:toc .toc-list-icons}

{::options parse_block_html="true" /}

----

We have two Periscope [spaces](https://doc.periscopedata.com/article/spaces#article-title):
* GitLab
* GitLab Sensitive

They connect to the data warehouse with different users- `periscope` and `periscope_sensitive` respectively. 

Most work is present in the GitLab space, though some _extremely sensitive analyses_ will be limited to GitLab sensitive. Examples of this may include analyses involving contractor and employee compensation and unanonymized interviewing data. 

Spaces are organized with tags. Tags should map to function (Product, Marketing, Sales, etc) and subfunction (Create, Secure, Field Marketing, EMEA). Tags should loosely match [issue labels](handbook/business-ops/data-team/#issue-labeling) (no prioritization). 
Tags are free. Make it as easy as possible for people to find the information they're looking for. At this time, tags cannot be deleted or renamed. 

* [Example Dashboard](https://app.periscopedata.com/app/gitlab/403199/Example-Dashboard)

## GitLab Space

* [Corporate Metrics](https://app.periscopedata.com/app/gitlab/409920/Corporate-Metrics)
* [Distribution of Customers to ARR](https://app.periscopedata.com/app/gitlab/416459/Distribution-of-Customers-to-ARR)
   * [Customers by ARR Rank](https://app.periscopedata.com/app/gitlab/416590/Customers-by-ARR-Rank)
* [GitLab.com Customer Retention](https://app.periscopedata.com/app/gitlab/412223/GitLab.com-Customer-Retention)
* [GitLab.com Sandbox](https://app.periscopedata.com/app/gitlab/406359/Gitlab.com-Sandbox)
* [New Business Sales Cycle - Trailing 12 Months](https://app.periscopedata.com/app/gitlab/408541/New-Business-Sales-Cycle---Trailing-12-Months)
* [Retention](https://app.periscopedata.com/app/gitlab/403244/Retention)
* [Sales Forecast](https://app.periscopedata.com/app/gitlab/408411/Sales-Forecast)
* [Secure Metrics](https://app.periscopedata.com/app/gitlab/410654/Secure-Metrics)
* [Snowplow Summary Metrics](https://app.periscopedata.com/app/gitlab/417669/Snowplow-Summary-Dashboard)
* [Version Upgrade Rate](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate)


## GitLab Sensitive Space

This space is not used at this time. 

## Data Team Workflow

1. Issue in [Analytics Project](https://gitlab.com/gitlab-data/analytics/) 
2. Write SQL queries in Periscope; Dashboard should be named `WIP: Dashboard Name`
3. Add the hyperlinked dashboard to this directory (with just the `Dashboard Name`, no WIP)
4. Deliver dashboard in original issue (See [Data Analysis Process](/handbook/business-ops/data-team/#-data-analysis-process) for more details)

## Periscope Dashboard Checklist 
* [ ] "What am I looking at?" Box 
   * [ ] Original Issue Link
   * [ ] Metrics Definitions Link
   * [ ] Last edited by @slackhandle
   * [ ] Ask questions in #analytics
* [ ] Filters, if relevant
* [ ] Drill Down Linked, if relevant
* [ ] Overview/KPI/Top Level Metrics
* [ ] Section Label before more granular metrics
* [ ] Tags added
* [ ] Permissions reviewed
* [ ] Viz Titles changed to Autofit
* [ ] Axes labeled, if relevant
* [ ] Numbers (Currencies, Percents, Decimal Places, etc) cleaned, if relevant
* [ ] Chart description updated for each chart, linking to Metrics definitions where possible
* [ ] Legend is clear
