---
layout: markdown_page
title: "Meeting IEC 62304:2006 Requirements with GitLab"
---
## Building effective workflows for and meeting IEC 62304:2006 requirements with GitLab

IEC 62304:2006 defines requirements for the lifecycle of medical device software. It prescribe processes, activities, and tasks to improve the safety and effectiveness of a medical device by taking a comprehensive, risk-based approach to software development.

GitLab is a single application for the entire software development lifecycle, providing tools from project planning to security testing. By using GitLab, you can use tools across the development lifecycle to contribute to compliance with requirements across IEC 62304:2006.

### Creating and documenting plans and processes

Software development plans and processes can be created, maintained, and referenced throughout GitLab. With [Wiki](https://docs.gitlab.com/ee/user/project/wiki/), you can include a comprehensive documentation system into every project. You can also [integrate](https://docs.gitlab.com/ee/user/project/integrations/index.html) every project with a long list of external services, including Jira. If you have one set of requirements across several projects, you can use [project import/export](https://docs.gitlab.com/ee/user/project/settings/import_export.html) to create a template project with all the information required in it and then seamlessly import it into new projects as needed. Because of the collaborative nature of GitLab, itâs easy to keep your documentation both current and visible across relevant teams. And throughout the software development lifecycle, you can seamlessly reference and incorporate your plans and processes.

### Managing system, development, and customer requirements

With [description templates](https://docs.gitlab.com/ee/user/project/description_templates.html), you can create templates with the requirements and other considerations the development team need to incorporate into the software or development process. Along with [task lists](https://docs.gitlab.com/ee/user/markdown.html#task-lists), you can track the incorporation of requirements into the process and provide an easy-to-use way to track those tasks for developers and reviewers.

To enforce requirements and coding standards throughout the software development lifecycle, you can use [merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html) to require all merge requests are reviewed and approved by the appropriate person(s). With [push rules](https://docs.gitlab.com/ee/push_rules/push_rules.html), you can create and enforce unique rules which, if not followed, will decline a push. And using [protected branches](https://docs.gitlab.com/ee/user/project/protected_branches.html), you can set granular permissions for who can interact with branches and how, further enforcing specific requirements. In the future, the [Requirements Management](https://gitlab.com/groups/gitlab-org/-/epics/670) product category will allow for a more complete and structured way to granularly enforce and audit compliance with complex requirements.

Using [Service Desk](https://about.gitlab.com/product/service-desk/), you can interact with and collect feedback from customers and other external stakeholders. This allows you to seamlessly monitor, document, and evaluate feedback as part of your software maintenance process, straight from GitLab.

### Maintaining traceability

[Labels](https://docs.gitlab.com/ee/user/project/labels.html) and [related issues](https://docs.gitlab.com/ee/user/project/issues/related_issues.html) can be used to maintain traceability across the entire software development lifecycle. Using labels, issues and merge requests can be associated back to specific requirements, requests, plans, or customer feedback. By [subscribing](https://docs.gitlab.com/ee/user/project/labels.html) to a label, you can be notified when specific labels are used, helping increase visibility and reducing the risk of important items being missed. And on your GitLab instance, you can [search](https://docs.gitlab.com/ee/user/search/) through issues and merge requests by label and other criteria.

Issues can be [exported](https://docs.gitlab.com/ee/user/project/issues/csv_export.html) into a CSV format. You can select which issues to include in the export and all labels associated with those labels are included in the export file. With exports, issues with a particular label can be traced back to specific requirements and other issues. Because of the flexibility of CSV files, the export can be converted to other formats and otherwise manipulated to meet your specific requirements and use case.

With the [Audit Event](https://docs.gitlab.com/ee/administration/audit_events.html) tool, you can get visibility into group- and project-level events, including when users are added/removed from a group or project. The [goal](https://gitlab.com/groups/gitlab-org/-/epics/474) is for all group and project events to be auditable with the Audit Event tool, such as [project tags](https://gitlab.com/gitlab-org/gitlab-ee/issues/5246) and [merge request approval settings](https://gitlab.com/gitlab-org/gitlab-ee/issues/7531).

### Identifying and managing risks

Every new code commit can be automatically scanned for security vulnerabilities with [Static Application Security Testing](https://docs.gitlab.com/ee/user/project/merge_requests/sast.html) (SAST). SAST can help you identify unsafe code and several classes of software vulnerabilities in 12 different languages, including C and C++. Every scan generates a [SAST report artifact](https://docs.gitlab.com/ee/ci/yaml/README.html#artifactsreportssast-ultimate) which is added directly to the merge request, so every security finding maintains full traceability. With SASTâs ease-of-use, you can maintain security scanning coverage of your entire codebase.

When risks are identified and remediation efforts begin, your team can use [issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html) to track, prioritize, visualize, and document your efforts. Risk findings can be expressed in terms of an issue and viewed in the context of an issue board. Because of the powerful traceability features discussed above, you can maintain full traceability across the risk management process.

### What are the requirements for IEC 62304:2006 and how can GitLab help meet them

Every organization has different requirements and workflows, so which GitLab features are used and how depends on your organization's unique needs. The sections above aim to provide an example of how some features may be used to meet several key IEC 62304:2006 requirements. To learn more about GitLab features which may be able to contribute to IEC 62304:2006 requirements, the table below links to GitLab product categories that may be useful in meeting a process's activities and tasks.


<table>
  <tr>
    <td><center>**IEC 62304:2006 Activities**</center></td>
    <td><center>**How GitLab may be able to help**</center></td>
  </tr>
  <tr>
    <td colspan=2><center>**Software Development Process**</center></td>
  </tr>
  <tr>
    <td>Software Development Plan</td>
    <td rowspan=8><ul>
        <li>[Agile Portfolio Management](https://about.gitlab.com/product/agile-portfolio-management/)</li>
        <li>[Code Quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html)</li>
        <li>[CI/CD](https://about.gitlab.com/product/continuous-integration/)</li>
        <li>[Feature Flags](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html)</li>
        <li>[Kanban Boards](https://about.gitlab.com/product/kanban-boards/)</li>
        <li>[Project Management](https://about.gitlab.com/product/project-management/)</li>
        <li>[Release Orchestration](https://about.gitlab.com/direction/release/release_orchestration/)</li>
        <li>[Wiki](https://docs.gitlab.com/ee/user/project/wiki/)</li>
        <li>[Future: Requirements Management](https://gitlab.com/groups/gitlab-org/-/epics/670)</li>
      </ul></td>
  </tr>
  <tr>
    <td>Software Requirements Analysis</td>
  </tr>
  <tr>
    <td>Software Architecture Design</td>
  </tr>
  <tr>
    <td>Software Detailed Design</td>
  </tr>
  <tr>
    <td>Software Unit Implementation and Verification</td>
  </tr>
  <tr>
    <td>Software Integration and Integration Testing</td>
  </tr>
  <tr>
    <td>Software System Testing</td>
  </tr>
  <tr>
    <td>Software Release</td>
  </tr>
  <tr>
    <td colspan=2><center>**Software Maintenance Process**</center></td>
  </tr>
    <tr>
    <td>Establish Software Maintenance Plan</td>
    <td rowspan=3><ul>
        <li>[Agile Portfolio Management](https://about.gitlab.com/product/agile-portfolio-management/)</li>
        <li>[Kanban Boards](https://about.gitlab.com/product/kanban-boards/)</li>
        <li>[Project Management](https://about.gitlab.com/product/project-management/)</li>
        <li>[Release Orchestration](https://about.gitlab.com/direction/release/release_orchestration/)</li>
        <li>[Service Desk](https://about.gitlab.com/product/service-desk/)</li>
        <li>[Wiki](https://docs.gitlab.com/ee/user/project/wiki/)</li>
        <li>[Future: Requirements Management](https://gitlab.com/groups/gitlab-org/-/epics/670)</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>Problem and Modification Analysis</td>
  </tr>
  <tr>
    <td>Modification Implementation</td>
  </tr>
  <tr>
    <td colspan=2><center>**Software Risk Management Process**</center></td>
  </tr>
  <tr>
    <td>Analysis of Software Contributing to Hazardous Situations</td>
    <td rowspan=4>
    <ul>
        <li>[Agile Portfolio Management](https://about.gitlab.com/product/agile-portfolio-management/)</li>
        <li>[CI/CD](https://about.gitlab.com/product/continuous-integration/)</li>
        <li>[Kanban Boards](https://about.gitlab.com/product/kanban-boards/)</li>
        <li>[Project Management](https://about.gitlab.com/product/project-management/)</li>
        <li>[Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/project/merge_requests/sast.html)</li>
        <li>[Wiki](https://docs.gitlab.com/ee/user/project/wiki/)</li>
        <li>[Future: Requirements Management](https://gitlab.com/groups/gitlab-org/-/epics/670)</li>
    </ul>
    </td>
  </tr>
  <tr>
    <td>Risk Control Measures</td>
  </tr>
  <tr>
    <td>Verification of Risk Control Measures</td>
  </tr>
  <tr>
    <td>Risk Management of Software Changes</td>
  </tr>
  <tr>
    <td colspan=2>Software Configuration Management Process</td>
  </tr>
  <tr>
    <td>Configuration Identification</td>
    <td rowspan=3><ul>
        <li>[CI/CD](https://about.gitlab.com/product/continuous-integration/)</li>
        <li>[Feature Flags](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html)</li>
        <li>[Release Orchestration](https://about.gitlab.com/direction/release/release_orchestration/)</li>
      </ul>
  </td>
  </tr>
  <tr>
    <td>Change Control</td>
  </tr>
  <tr>
    <td>Configuration Status Accounting</td>
  </tr>
  <tr>
    <td colspan=2><center>**Software Problem Resolution Process**</center></td>
  </tr>
  <tr>
    <td>Prepare Problem Reports</td>
    <td rowspan=8><ul>
        <li>[Agile Portfolio Management](https://about.gitlab.com/product/agile-portfolio-management/)</li>
        <li>[CI/CD](https://about.gitlab.com/product/continuous-integration/)</li>
        <li>[Kanban Boards](https://about.gitlab.com/product/kanban-boards/)</li>
        <li>[Project Management](https://about.gitlab.com/product/project-management/)</li>
        <li>[Wiki](https://docs.gitlab.com/ee/user/project/wiki/)</li>
        <li>[Future: Requirements Management](https://gitlab.com/groups/gitlab-org/-/epics/670)</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>Investigate the Problem</td>
  </tr>
  <tr>
    <td>Advise Relevant Parties</td>
  </tr>
  <tr>
    <td>Use Change Control Process</td>
  </tr>
  <tr>
    <td>Maintain Records</td>
  </tr>
  <tr>
    <td>Analysis Problems for Trends</td>
  </tr>
  <tr>
    <td>Verify Software Problem Resolution</td>
  </tr>
  <tr>
    <td>Test Documentation Contents</td>
  </tr>
</table>
