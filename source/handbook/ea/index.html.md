---
layout: markdown_page
title: "Executive Assistants"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Intro

This page details processes and general guidelines specific to the Executive Assistants at GitLab. The page is intended to be helpful, feel free to deviate from it and update this page if you think it makes sense.
If there are things that might seem pretentious or overbearing please raise them so we can remove or adapt them. 

## Executive Assistant Team 

* Cheri Holmes (Manager) supporting Sid Sijbrandij (Co-Founder & CEO), Paul Machle (CFO) 
* Stefanie Haynes (Sr. EA) supporting Todd Barr (CMO), PeopleOps and Future CPO, Brandon Jung (VP Alliances), Eric Johnson (VP Engineering) and Mark Pundsack (VP Product)
* Vanessa Wheeler (EA) supporting Michael McBride (CRO)

## Meeting request requirements

If you want to schedule a meeting, email the EA the following:
* Must have/optional attendees (keep in mind that the more must haves, the harder to schedule/more time needed)
* Meeting type: internal prep, client facing/customer, prospective customer, etc
* Urgency: in the next two days, in the next week or two, etc
* Duration: 25 mins, 50 mins, etc.
* Provide context: include topic, agenda to be covered. Share google doc if available to be included in invite
* [Meeting Template](https://docs.google.com/document/d/1qj4MRlIXXGs4Jni0ITYp1uaHDQr53IvOqmpN-47RD-k/edit#heading=h.954v91mukl7r) should be used for all meetings with members of our E-Group and provided at the time of the meeting request. Select file > make a copy to use this template.

In case you have a **same day** request, you can send the same information in the "ea-team" Slack channel. If not, you don't have to ping us on Slack if you already sent the email.


### Formats for invites
* MTG for meetings in person, either in the boardroom or another location
* INTERVIEW for interviews (make sure to loop in our PR partner)
* PHONECALL for phone calls
* VIDEOCALL for video conference calls using Zoom
  * Example: “VIDEOCALL Kirsten Abma for Executive Assistant” or "VIDEOCALL Kirsten Abma (GitLab) & Sid Sijbrandij (GitLab)""
* When using the [Zoom plugin for Google Calendar](/handbook/communication/#video-calls) you can easily get the info you need in the invite.
* Please add the subject of the call in the description, for internal and external calls.
* All external meetings RSVP should be confirmed with the guests a day before to make sure people are attending or need to reschedule
* When meetings are being rescheduled please put RESCHEDULING at the beginning of the appointment
* When video calls or meetings are being recorded add RECORDING to the invite so people in the boardroom can see it and keep the background noise minimal
* When a meeting is being live-streamed to YouTube add LIVESTREAM in subject of invite
* Addresses in calendar invites should only have an address and nothing else
* **Everyone external** receives a link to the [GitLab About page](/about). Only for final interviews people receive the form to fill out.
* **All** holds on Sid's calendar need a reason so he can judge when the hold might no longer apply.

Make sure to include the following in the description:

### Meetings in the boardroom

<br>
Please read our Company page as preparation for this meeting: /company/<br>
Executive cell: <br>
Guest cell:<br>
<br>
Copy-paste instructions to GitLab boardroom from the Google Doc the EAs have saved
<br>


## General scheduling guidelines

* [everytimezone.com](http://www.everytimezone.com) can help determine the best time to schedule
* You can add other [calendars](calendar.google.com) on the left using "Find a Time", to see when GitLab team members are free to schedule a meeting with. Please be cognizant of people time zones.
* Use for example a tool like [Skyscanner](www.skyscanner.com) to find different flight options with most airlines when needing to book travel
* Schedule calls in European timezones in the morning (am) Pacific (Daylight) Time and US time zones in the afternoon (pm) Pacific (Daylight) Time
* Holds on the schedule should be removed at least 15 minutes before the preceeding meeting starts.
* Monthly video calls are 25 minutes while quarterly calls/dinners are scheduled for 90 minutes plus any necessary travel time.
* If the call is with any Google company, use Hangouts instead of Zoom.
* Meetings in the boardroom with another guest joining via videocall. The EA will schedule an additional ten minutes before the appointment to test the system.
* For meetings or lunch/dinner appointments, always make sure to add the address in the invite of the location where it’s scheduled.
* Make sure to plan travel time (in a separate calendar item, just for the exec) before and after the meeting in case another meeting or call should follow.
* Sales meetings are important. If the CEO can help the process, feel free to include him in the meeting by working with the EA on this. Please include the sales rep and solutions architect in this meeting. The person requesting the meeting should provide a meeting brief document to the EA.

## E Group In-person Meetings and Board Meetings

There should be one invite for all attendees that includes the following:

* Exact meeting time blocked (ie: Start at 9am PST, End at 5pm PST)
* Zoom Link for remote participants
* Agenda (the agenda should also include the zoom link at the top)
* Notes doc shared via calendar invite and sharing set to "can edit" for those attending the BoD meeting
* Optional lunch invite sent in advance to BoD members and E-Group

Make sure to:

* Test Zoom Room setup a day before
* Arrange drinks and snacks at the Board Room. Keep an eye on the coffee pods.
* Ensure remote participants feel invited and welcomed
* Track attendance in the Notes doc of the BoD meeting

## Email Management

* Labels: /archive, /read-respond, /personal or /urgent-important
* Prepare draft responses
* Proactively schedule meetings requested via e-mail
* Standard reply for recruiters:
“We do not accept solicitations by recruiters, recruiting agencies, headhunters, and outsourcing organizations. Please find all info [on our jobs page](/jobs/#no-recruiters)

## Physical Mail

* Check all incoming (physical) mail at our Bush street address and sort the urgent and important letters.
* Inform AP if invoices came in.
* Inform other people/departments if mail is addressed for them and include a scan of the document.

## Travel

EAs research the best option for a flight and propose this before booking.
Make sure to add a calendar item for 2 hours before take off for check in and add a separate one for travel time before that in the exec's calendar.
If a flight was changed or not taken due to delays or other circumstances, make sure to check with the airline for the current flight status.

## Expensify

* When you’re logged in, you can find wingman account access for other team members in the top right corner menu.
* Check their email (if you have access), using the search bar in the top, to find any receipts for the postings in the current expense report.
* And/or write down what receipts are missing and email to request them if needed.

## OKRs 
* EA to the CEO to assist in maintaining and scheduling meetings revolving around the OKR schedule referenced here: https://about.gitlab.com/company/okrs/#updating 

## Scheduling preferences for Sid Sijbrandij, CEO

* Don't schedule over the Weekly E-group call unless approved by Sid
* When our Sr. Dir. of Legal requests a meeting it will always override other meetings that are scheduled if urgent
* Mark the events listed in [Google Calendar section](/handbook/communication/#google-calendar) of the handbook as 'Private'
* The [agenda](https://docs.google.com/document/d/187Q355Q4IvrJ-uayVamoQmh0aXZ6eixAOE90jZspAY4/edit?ts=574610db&pli=1) of items to be handled by Sid's EA
* Monthly video calls are 25 minutes while quarterly calls/dinners are scheduled for 90 minutes plus any necessary travel time.
* After each meeting with a potential investor, make sure to update the sheet with the information on these meetings (to be found in the agenda doc for Sid's EA)
* Follow up on introductions from certain recipients (board, investors) immediately without asking for approval.
* If Sid is meeting with a potential hire, make sure to create a profile in our ATS before the meeting for Sid to take notes during the call.
* If Sid has a **ride or walks** to an appointment, make sure to **add 5 minutes extra** to find the address and sign in at reception.
* If Sid is **driving himself**, make sure to **add 15 minutes extra** for random occurences such as traffic, stopping for gas or parking.
* If Sid is **driving himself** to a meeting, he likes to plan phone calls to catch up with the team. Check with him who he'd like to talk with during his commute and schedule accordingly.
* Due to a busy schedule Sid has a preference of meeting setup: First try for a video call or  a meeting in the GitLab boardroom. If the other party presses to meet at their location, confirm if that is OK before accepting.

### Travel preferences
Current preferences for flights are:
* Aisle seat
* Check a bag for all trips longer than one night
* Frequent Flyer details of all (previously flown) airlines are in EA vault of 1Password as well as important passport/visa info

### Pick your brain meetings

If people want advice on open source, remote work, or other things related to GitLab we'll consider that. If Sid approves of the request we suggest the following since we want to make sure the content is radiated as wide as possible.:

1. We send an email: "Thanks for being interested in GitLab. If we schedule a meeting it will follow the format on /handbook/ceo/#pick-your-brain-meetings Are you able to submit a draft post with us within 48 hours of interview?"
1. If we receive a positive answer we schedule a 50 minute Youtube Livestream. For an example of an interview see [this one about stress in remote work](https://www.youtube.com/watch?v=23XIx6n9SsQ).
1. Within 48 hours you share a draft post with us in a Google Doc with suggestion or edit rights for anyone that knows the url.
1. You can redact anything you don't want to publish.
1. Our executive assistant will work with you to publish the post if we think it is interesting enough for our audience. The EA-team will follow up to make sure the draft post is submitted and coordinate with Marketing to cross-post on our blog.
1. A great examples of this in action are the first few times we did this [/2016/07/14/building-an-open-source-company-interview-with-gitlabs-ceo/](https://about.gitlab.com/2016/07/14/building-an-open-source-company-interview-with-gitlabs-ceo/) and [https://news.ycombinator.com/item?id=12615723](https://news.ycombinator.com/item?id=12615723). Both got to nr. 1 on [Hacker News](https://news.ycombinator.com/).


Reply to emails: Thanks for wanting to chat. I propose we meet in the format proposed on /handbook/ceo/#pick-your-brain-meetings so that other people benefit from our conversation too. If you're up for that please work with Cheri (cc:) to schedule.

### Scheduling Pick Your Brain meetings

* Once pick your brain meetings are confirmed, schedule a Zoom Webinar and send out a calendar invite to all guests and make sure to add a separate calendar invite for Sid with preparation time.
* To schedule a Zoom webinar: https://support.zoom.us/hc/en-us/articles/115000350446-Streaming-a-Webinar-on-YouTube-Live


## Scheduling for Michael McBride, CRO

* Prefers “appropriate length” conversations, so short meetings are okay on the calendar.  If a topic only needs 10 mins, book 10 mins instead of 30, etc.
* Include Meeting Agendas in invites / make sure the team knows to include this with requests for time.
* Flexible with late evening calls for Asia or Australia - check with him first.
* Add pre-emptive blocks in calendar that can be used for meetings or calls.
* Add an hour a day for work time - title “workflow”. This is time for email follow-up and general work time. If the time has to move to accommodate another meeting ensure another time is found and that it can still happen.

## Scheduling for Eric Johnson, VPE

* 1:1 Meeting title format: “Person:Eric 1:1”
* Tick the box “Attendees can modify”
* Please create an additional reminder (besides the default 10min pop-up) that is 1 day. Email (reminds me to go into the doc and populate notes)
* Use Personal Zoom for meetings with the exception of back to back meetings. Please ensure there is another Zoom link.
Note: Eric will attach private 1:1 docs to the meeting series once they are created.
* Eric will block off personal appointments and family related blocks.
* Add holds when scheduling meetings and interviews so he knows it's being worked on.
* If meetings need to happen before 8am PT or after 6pm PT check with him directly.
* Eric likes updates and/or questions via Slack.
