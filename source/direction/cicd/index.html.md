---
layout: markdown_page
title: "Product Vision - CI/CD"
---

- TOC
{:toc}

### CI/CD at GitLab

The CI/CD sub-department focuses on code [build/verification](/direction/verify), [packaging](/direction/package), 
and [delivery](/direction/release). You can see how these relate to each other in the following infographic:

![Pipeline Infographic](https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_11_9.png "Pipeline Infographic")

### North Stars
GitLab follows the following North Stars within CI/CD:

1. **Progressive Delivery** - We believe [progressive delivery](https://redmonk.com/jgovernor/2018/08/06/towards-progressive-delivery/) is the future
of software delivery, and will continue to ensure capabilities like Feature Flags and Releases as a first-class entity continue to be
added to the product, enabling continuously delivery in a safe and controlled way.
1. **Security & Compliance** - In business critical applications, there cannot be a choice between speed of iteration and security. 
GitLab will continue to add compliance and security capabilities to your pipeline, built-in to the fundamental capabilities of the
pipeline rather than something added on and reactive, after the fact.
1. **Speed & Automation** - A slow CI/CD pipeline can handicap an organization's time-to-value. Businesses are increasingly under
pressure to ship new software faster. GitLab focuses on enabling a blazing fast CI/CD pipeline and providing you ready to use pipelines
out of the box, ensure you can spend more time focused on shipping code and less time managing your pipelines.

## Grand Unified Theory of CI/CD

Our vision for CI/CD remains aligned to the one set forth in Jez Humble's classic [Continuous Delivery](https://continuousdelivery.com/) book.
The ideas of reducing risk, getting features and fixes out quickly, and reducing costs/creating happier teams by removing the barriers
to getting things done has stood the test of time. This builds on the individual [Verify](/direction/verify), [Package](/direction/package), 
and [Release](/direction/release) stage direction pages.

We think there are a few simple but powerful primitives to add to GitLab CI/CD which will allow us to achieve this vision.
Each of these works independently and solves problems from simple to advanced, but truly become powerful when used in concert.
Because everything in CI/CD begins with the pipeline, that's where we focus our attention.

- **Workspaces**: Persisting artifacts between jobs in a pipeline (and even publishing them for later download) is done using a mix of caching
  and artifact functionality in GitLab. Neither is really ideal for the purpose - cache is not guaranteed to be available, and
  artifacts are more heavyweight than they need to be for this usage. Instead, we are going to implement workspaces as a first-class
  concept in GitLab which will support parallelization across jobs and make it easier to have your pipelines run just like they
  would if you had run it locally.

- **Directed Acyclic Graph (DAG)**: There's a simple graph model in GitLab today, but this works by assuming everything in a single stage
  runs in parallel, and every job in the following stage must wait for all the jobs in the previous stage to complete. This is
  functional, but not sufficient for more complex workflows and ones where every minute counts when waiting. By converting
  pipelines into DAGs, we unlock as-fast-as-possible execution as well as multiple success paths within a single pipeline (for
  monorepos, for example.)

- **Pipeline Entity Model**: Pipelines today can internally track a global success/failure status and generate artifacts,
  but have no way to really reflect on those and provide more advanced behaviors based on the internal state of the pipeline. By
  adding typed artifacts (a container image, a .json file, a gemfile) and internal state (the yaml used to run the job, status) as
  something available for introspection at runtime or to be used as a dependency in the DAG, we open the door to incredibly advanced
  cross or per-project pipeline behaviors based on generation of actual dependencies. As an additional benefit, this collection of
  entities within the pipeline itself becomes the bill of materials, collecting auditing and compliance evidence for the release as it runs.

- **Namespaces for Includes**: Finally, we have includes and extends capabilities within the configuration YAML today, but these
  work by importing the included/extended content into a global YAML, creating the risk of namespace collisions or otherwise
  unintended impacts. By adding an optional way to namespace includes, it allows you to build includes in a more rational way, much like
  you would create functions in code using locally scoped variables. This also makes includes more portable, making them easier
  to share and redistribute. We also plan to enable auto-inclusion of configuration YAML from subfolders, making this even simpler
  for monorepo and other scenarios.

In addition to the obvious direct benefits, these reinforce each other. By improving includes we make the DAG syntactically and 
cognitively easier to use. A pipeline entity model allows the DAG to self-reflect and define its own behavior based on the progress
it makes. By simplifying workspaces and making them parallelizable we make them available to an efficient DAG that is running as
much in parallel as possible.

If you're interested in learning more, check out this deep dive video where we discuss in detail how these ideas related to each other.

<figure class="video_container">
<iframe src="https://youtube.com/embed/FURkvXiiJek" frameborder="0" allowfullscreen="true" width="320" height="180"> </iframe>
</figure>
