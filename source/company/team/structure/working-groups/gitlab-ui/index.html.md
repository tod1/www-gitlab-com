---
layout: markdown_page
title: "gitlab-ui (CSS and Reusable Components)"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property     | Value |
|--------------|-------|
| Date Created | March 26, 2019 |
| Date Ended   | TBD |
| Slack        | [#wg_gitlab-ui](https://gitlab.slack.com/archives/CH9QG9TAQ/p1553587707000300) (only accessible from within the company) |
| Google Doc   | [gitlab-ui Working Group Agenda](https://docs.google.com/document/d/1CBg2XXH6l8h5sTKXSwQXEUD46HzEJVU8nsqYwZbW6O8/edit) (only accessible from within the company) |
| Epic         | [gitlab-ui Codebase](https://gitlab.com/groups/gitlab-org/-/epics/950) |

## Business Goal

Drive forward the [gitlab-ui](https://gitlab.com/gitlab-org/gitlab-ui) roadmap of our CSS Cleanup/Restructuring and the implementation of Reusable Components based on our design system. gitlab-ui will from now on have our base CSS implementation and all reusable components in one package to group all efforts of generic styling in one UI project. This will improve engineering productivity and consistency throughout the product. The main target of the working group is not to implement everything themselves but rather keep the effort moving across the whole frontend engineering group in a constant pace and pick up key tasks.

## Exit Criteria

All to-do list items in the main [Epic](https://gitlab.com/groups/gitlab-org/-/epics/950)

Especially the following items:

*  CSS Cleanup as described in the main Epic under *2. Spring Cleaning* for all CSS files
*  CSS Restructuring with splitting generic CSS in gitlab-ui and page specific in CE/EE
*  Implementation of our currently planned components ([bootstrap Components](https://gitlab.com/groups/gitlab-org/-/epics/574) and [Shared components](https://gitlab.com/groups/gitlab-org/-/epics/672))
*  All currently planned components match style and usage guidelines based on the design system
*  Ensure there is clear documentation written for when and how new components should be introduced into the design system and the product, including responsible counterparts

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Facilitator           | Tim Zallmann          | Director of Engineering, Dev   |
| Frontend Lead         | Filipa Lacerda        | Senior Frontend Engineer       |
| Member                | Phil Hughes           | Senior Frontend Engineer       |
| Member                | Sarah Groff Hennigh-Palermo | Senior Frontend Engineer |
| Member                | Denys Mishunov        | Senior Frontend Engineer       |
| Member                | Simon Knox            | Frontend Engineer              |
| UX Lead               | Taurie Davis          | Staff UX Designer              |
| Member                | Rayana Verissimo      | Senior UX Designer             |
| Executive Stakeholder | Christopher Lefelhocz | Senior Director of Development |
