---
layout: markdown_page
title: "FY20-Q1 OKRs"
---

Note: This is the first quarter where we [shift our fiscal year](https://about.gitlab.com/handbook/communication/#writing-style-guidelines). FY20-Q1 will run from February 1, 2019 to April 30, 2019.

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV. More leads. Successful customers, Scalable marketing

1. Alliances: Measure the value partners can bring to the business.  3 references for each major public/private cloud, establish customer ability to purchase through marketplaces, joint marketing with key partners - 200+ leads per event
1. CRO: Generate IACV from sales to higher tiers.  Target 250 new opportunities created from core accounts, Implement sales motion to show all starter renewal accounts the benefit and value of premium and ultimate, Identify two new already deployed large enterprise core customers in each SAL territory
1. CRO: Faster delivery of customer value.  Deliver plan to have Sales quote implementation services and success plan on all new account quotes, Launch standard implementation model in large segment, Measure time to completed implementation for all new enterprise accounts.
1. CMO: Recharge Inbound Marketing Engine. Top 10 Gitlab.com Pages optimized for inbound, Inbound trial and contact-us strategy documented and baselined, Pages optimized for top 20 non-branded SEO target terms, Inbound strategy for Dev, Sec & Ops personas launched
    1. Content Marketing: Broaden audience reach by increasing new user sessions on the blog by 15%. Create next iteration editorial mission statement and content framework. Publishing plan mapped to key topics, persona, and campaigns. Publish 5 interactive or deep-dive assets.
    1. Content Marketing: Increase social referral traffic 20%. Execute 2 social campaigns. Next iteration social strategy and framework. Create internal social advocacy program.
    1. Content Marketing: Build content paths to support lead flow. Create content strategies for 3 personas/buying groups. Execute plan to increase newsletter subscribers 50%. Contribute 10% of overall click-thrus to form fills.
    1. Digital Marketing programs: Improve Top 10 Gitlab.com Pages and increase inbound CTR and conversion rate by 10% QoQ.Optimize and increase visibility for 20 non-branded terms, Optimize competition pages for better visibility and to assist competitive take-out with 10% increase in CTR from Organic Google.
    1. Digital Marketing programs: 15% QoQ increase in inbound website leads through gated content and paid digital and increase conversion rate with improved nurture. Implement 3 baseline nurture tracks to educate and generate leads within the database, Implement an always-on digital paid program (to include re-targeting, etc.).
    1. Product Marketing: Document buyer’s journey (buyer’s experience, tribe needs). Documented handbook pages of messaging around the 3 core pillars of Dev, Sec & Ops.
    1. Product Marketing: Execute 1 competitive takeout campaign. Create campaign materials (web pages, demos, whitepapers, blogs), perform global sales enablement sessions.
    1. Product Marketing: Identify, evaluate and implement competitor analysis and comparisons strategy. Deliver first 5 comparisons using new strategy.
    1. Product Marketing: Improve case study publish rate.  Publish case studies at cadence of at least 2 per month, with at least 3 reference customers using all stages concurrently.
    1. Field Marketing: Create the ABM Framework and tooling. Execute ABM in 5 accounts per region, Identify messaging per account, Execute 1 account-targeted online campaign.
    1. Community Relations: Increase the number of contributions (not contributors) from the wider community (including Core Team) across [selected GitLab projects](https://gitlab.com/Bitergia/c/gitlab/sources/blob/master/projects.json#L27). For the 11.7 release, the target is 180; for the 11.8 release the target is 200; for the 11.9 release the target is 220.
    1. Community Relations: Accelerate open source project adoption of GitLab.com. Hire an Open Source Program Manager, gain commitment of 1 big open source project (1000+ contributors) to migrate to GitLab.
    1. Community Relations: Jumpstart meetups. Schedule 10 meetup events, at least one in every region (EMEA, APAC, NA).
    1. Community Relations: Increase responsiveness and coverage. Decrease median first reply time to 7 hours while adding two new response channels (GitLab forum, Stack Overflow).
1. CMO: Achieve marketing process and visibility excellence. 100% visibility into any and all leads at any time, Full-funnel marketing dashboard published in Looker, Unified marketing calendar published.
    1. Digital Marketing programs: Build out a framework and launch for an integrated campaign structure (ICS), execute one campaign, run one competitive take-out sub-campaign and adapt ICS to ABM use cases.
    1. Sales Development: BDR role clarity, defined development path. Clarified Lead Sources, add at least 2 Additional Lead Channels , inbound “call sales” phone line, Drift optimization - implement Drift Account Based Marketing.
    1. Sales Development: Reduce xDR Ramp Time to 3 months. Hire an Onboarding and Enablement Manager (Done), “Expert” Certifications around Personas, Product Tiers, better knowledge management solution: battlecards by persona, vertical.
    1. Field Marketing: Create Field Marketing Dashboard in SFDC. Identify & create the elements for dashboard, Creation of data driven decision framework for field, Improved visibility of field activity.
    1. Field Marketing: Create SLAs around Field Marketing & Marketing Ops. Creation of SLAs for Field Marketing to Ops (ops to field already exists), Create manual dashboard to track SLAs and improve performance, Improve overall speed of contact to MQL cycle time (need baseline).
    1. Community Relations: Increase responsiveness and coverage. Decrease median first reply time to 7 hours while adding two new response channels (GitLab forum, Stack Overflow).
1. CMO: Achieve $9.4m in Net New IACV Pipeline created by beginning of Q2 FY19 (as measured in [this report](https://gitlab.my.salesforce.com/00O61000004hfKw)).  720 SAOs delivered to sales by SDRs/BDRs (Revised [definition of SAO](https://about.gitlab.com/handbook/business-ops/#criteria-for-sales-accepted-opportunity-sao) as of Jan 2019), 26,000 MQLs created, 11% MQL to SAO conversion rate.
    1. Sales Development: Increase Accepted Opportunity per SDR to 8/month. Rise in reps achieving quota per month. 16 Jan, 18 Feb, 20 Mar, 22 Apr; > 1 significant (3 min+) conversation per rep per day; complete an Outreach.io audit and content improvement.

### CEO: Popular next generation product. Grow secure and defend. Grown use of stages (SMAU). iPhone app on iPad.

1. Product: Increase product breadth. 25% (6 of 22) "new in 2019" categories at `minimal` maturity, CI/CD for iOS and Android development on GitLab.com, develop an iOS app on GitLab.com web IDE on iPad.
1. Product: Increase product depth for existing categories. Consumption pricing for Linux, Windows, macOS for GitLab.com and self-managed, 50% (11 of 22) "new in 2018" categories at `viable` [maturity](/handbook/product/categories/maturity/).
1. Product: Grow use of GitLab for all stages of the DevOps lifecycle. Increase Stage Monthly Active Users ([SMAU](/handbook/product/growth/#smau)) 10% m/m for each stage, 3 reference customers using all stages concurrently. => [SMAU Dashboard](https://gitlab.looker.com/dashboards/69)
1. Tech Writing: In 100/100 sampled Support/forum interactions in FY20-Q1, customer-facing product info is [incorporated in docs](https://gitlab.com/gitlab-com/Product/issues/51), reinforcing [Docs-First Methodology](https://about.gitlab.com/handbook/documentation/#docs-first-methodology)
1. Alliances: evaluate if key partner services (AKS,EKS,GKE, VKE, etc) can grow use of stages.  Conversion of SCM customers to CI/CD through joint GTM.
1. CRO:  Increase CI usage.  Perform CI workshop/whiteboard at 2 of the top 10 customers that are only using SCM today in each region/segment by end of Q1.
1. VPE: [Make GitLab.com profitable. Recognize TBD% cost reduction on a per user basis.](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3905)
1. VPE: [Increase productivity. Increase MRs per engineer 20% for all teams.](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3906)
    1. Development: Increase velocity. Increase MRs per engineer 20% for all teams.
    1. Development: Develop memory program to reduce memory footprint based on customer feedback [Memory OKR](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3914).  
        1. Fellow: [Make GitLab performant with Gitaly atop NFS](https://gitlab.com/groups/gitlab-org/-/epics/824)
        1. Dev: GA of GraphQL to increase throughput for BE and FE. Resolve general topics like performance, abuse prevention and enjoyable development experience. Endpoints for Create and Plan relevant models. Implement new frontend features in Create and Plan only with GraphQL.
            1. Plan BE: Complete steps for [general availability of GraphQL](https://gitlab.com/groups/gitlab-org/-/epics/711).
        1. Dev: Increase velocity for frontend implementations. Increase throughput by 20%. Have at least 50 gitlab-ui components at the end of Q1. Improve tooling in GitLab itself for JS engineering (webpack map, visual diffing, etc.) by adding 3 new tools to our workflow.
        1. Dev: Improve [Performance of JS computation that is done on every page](https://gitlab.com/groups/gitlab-org/-/epics/716). Improve Initialisation execution by 20%. Reduce main bundle size by 20%.
            1. Manage BE: Take on more maintainer responsibilities: add a CE/EE backend maintainer
            1. Manage FE: Increase throughput via proper backlog grooming, improved estimation, and proper issue planning. Also utilize all existing gitlab-ui components for Manage code.
            1. Manage FE: Fully integrate Sentry into the FE org workflow by updating the library, optimize and repair (loading order, configuration, sourcemap support), and determine process for fully utilizing it as an org. Also explore using it in dev, canary, and/or small percentage of production via A/B flipper.
            1. Create BE: Improve maintainability and performance of GitLab Shell: port GitLab Shell to Golang
            1. Create BE: Take on more maintainer responsibilities: add a CE/EE backend maintainer, add a database maintainer
            1. Create BE: Prepare Elasticsearch for use on GitLab.com. [Improve admin controls](https://gitlab.com/groups/gitlab-org/-/epics/428), [reduce index size](https://gitlab.com/groups/gitlab-org/-/epics/429)
        1. Sec: Increase Secure throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trendline
             1. [Philippe Lafoucrière](https://about.gitlab.com/company/team/#plafoucriere): Handover to new Secure Engineering Manager.
             1. [Philippe Lafoucrière](https://about.gitlab.com/company/team/#plafoucriere): [CISSP](https://www.isc2.org/Certifications/CISSP) certification.
             1. [Philippe Lafoucrière](https://about.gitlab.com/company/team/#plafoucriere): Benchmark 3 new security tools for integration in the [Security Products](https://about.gitlab.com/handbook/engineering/ops/secure/#security_products).
        1. Ops (Verify, Release & Package): Increase BE team's throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trend-line
            1. [Kamil](https://about.gitlab.com/company/team/#ayufanpl) Train 2 new maintainers of GitLab Rails,
            1. [Kamil](https://about.gitlab.com/company/team/#ayufanpl) Help with building CD features needed to continuously deploy GitLab.com by closing 5 that are either ~"technical debt" or ~"bug"
            1. Verify BE: Increase Verify throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trend-line
            1. Verify FE: Reduce implementation time by building on top of gitlab-ui components; increase throughput by building/improving upon JS and Frontend specific tooling by doing product discovery for 2 new tools.
            1. Release FE: Formulate approach to utilize GraphQL in areas where data is requested across multiple endpoints or custom datasets would increase performance.
            1. Verify FE: Formulate approach to utilize GraphQL in areas where data is requested across multiple endpoints or custom datasets would increase performance.
            1. Release BE: Increase Release throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trend-line
            1. Release FE: Reduce implementation time by building on top of gitlab-ui components; increase throughput by building/improving upon JS and Frontend specific tooling by doing product discovery for 2 new tools.
		1. Ops (Configure & Monitor): Define additional data to complement throughput: Implement a prototype for team pulse and define a path to implement it in GitLab product.
        1. Ops (Configure & Monitor): Increase the groups's throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trend-line
            1. Configure BE: Increase Configure throughput by 20% and drive consistency week to week: Increase number of MRs week to week, increase number of reviewers and maintainers, and drive consistency in the trend-line
            1. Configure FE: Implement ES2015+ bundles for modern browsers & transpiled ES5 for older ones should reduce bundle size and parsing time
            1. Monitor BE: Increase throughput and consistency, Increase throughput by 20% and even out throughput week to week to bring the standard deviation below 2 for the most recent 12 weeks. As of Jan 4 the standard deviation is [2.8](https://www.wolframalpha.com/input/?i=standard+deviation+of+2,7,8,5,9,8,1,5,5,3,1,3)
            1. Monitor FE: Reduce 5% of the css loaded in our main css bundle and implement for 4 additional gitlab-ui components.
        1. Enablement: Communicate and address pain points with issue boards: identify and work with Product to prioritize 5 issues
        1. Enablement: Increase ownership for key GitLab components: add one new maintainer for each of Gitaly, Pages, Workhorse, and the ElasticSearch indexer
            1. Geo BE: support Production team in preparing Geo for DR: [enhance selective sync to work at scale](https://gitlab.com/gitlab-org/gitlab-ee/issues/8798), [support GitLab Pages](https://gitlab.com/groups/gitlab-org/-/epics/589)
    1. Infrastructure: Make all user-visible services ready for mission-critical workloads while seamlessly supporting delivery of new product functionality and integrating infrastructure solutions in GitLab. [Availability] Definition and tracking of GitLab.com metrics for SLAs, [Product] Deliver OKR Infrastructure workflow and tooling using GitLab, [Introspective] Achieve a 10% cost reduction on GitLab.com spending.
        1. Reliability[AS]: Support SRE-at-large MTBF and MTTR objectives. [Availability] Reduce spin-up time for provisioning auto-scaled servers to <3.5min, [Product] Automate error budgets as a GitLab feature, [Introspective] Deliver new testing environment.
        1. Reliability[DS]: Drive all user-visible services' MTBF to Infinity. [Availability] Deliver improvements to GitLab.com reliability on two foundational subsystems (ZFS on storage, Kubernetes + Charts), [Product] Take full operational ownership of CI/CD (take oncall from tomscz), [Introspective] Operationalize Vault and Consul.
        1. Reliability[JF]: Drive all user-visible services' MTTR to less than 60 seconds. [Availability] Implement 4 significant availability features to improve database reliability (autovacuum, masterless backups, pgrepack, replica on ZFS), [Product] Deliver Patroni on GitLab, [Introspective] Deliver full service operational inventory
        1. Delivery: Streamline GitLab deployments on GitLab.com and self-managed releases. [Availability] Create deployment rollback process, [Product] [Merge CE/EE codebases](https://about.gitlab.com/handbook/engineering/infrastructure/design/merge-ce-ee-codebases/), [Introspective] Replace `takeoff` off with a method better suited for CI pipelines.
        1. Andrew Newdigate: Improve observability for all-user visible services to maximize MTBF and minimize MTTR. [Availability] Deliver distributed tracing on GitLab.com, [Product] Deliver Puma on GitLab and experiments on GitLab.com, [Introspective] Deliver overall service-level tracking in preparation for SLA tracking.
    1. Quality: [Build everything in GitLab, move GitLab Insights production ready graphs into GitLab itself](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3908).
    1. Quality: Implement performance monitoring for the development department. [Continuous measurement with performance tests for on-prem instances (10K scaled down reference)](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3909).
    1. Quality: Improve Engineering velocity. [Add 4 Engineering Productivity metrics with historical measures. Monthly throughput, total monthly Merge Requests, Merge Request review time, and time to resolve P1 P2 Bugs](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3991).
    1. Security: Secure the Product. Conduct one Red Team exercise with H1.
        1. AppSec: Security release: complete at least 4 cycles of security release process/checklist X%
        1. AppSec: App sec reviews: conduct at least 4 appsec reviews X%
        1. Compliance: SOC2 Examination Preparation and Gap Assessment: Complete SOC2 Control Statements, supporting documentation, and self-assessment questionnaire, including its distribution X%
        1. Compliance: PCI Compliance: PCI requirements review and self-assessment X%
    1. Security: Secure the company.
        1. Strategic Sec: Evaluate at least 2 enterprise centralized SSO solutions and make selection. X%
        1. SecOps: Evaluate at least 2 security monitoring capabilities and deploy in GitLab.com as part of Zero Trust initiative. X%
        1. SecOps: Evaluate at least 2, and implement vulnerability scanning capability for GitLab.com infrastructure. X%
        1. SecOps: Develop and deliver one monthly New Hire Security Orientation training by EOQ. X%
    1. Support: [Clarify support expectations within each solution](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3966).
    1. Support: [Author architecture document for 10k GitLab install with review and approval from Infrastructure.](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3964).
    1. Support: [Improve docs based on solutions to customer reported issues](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3965).
    1. UX: Convert more prospects into new GitLab users. Decrease sign-up time to 1 minute or less. Document the existing user journey in video, propose a new sign-up flow, test it with users, and ship the first iteration of the new workflow.
    1. UX: Provide a guided onboarding experience that helps new users become productive in GitLab more quickly. 80% of new users should start guided onboarding, and 80% of those who start should finish. Create a user journey of current experiences and pain points, propose and test a guided first-time experience, and ship the MVC solution.
    1. UX: Improve usability and help beautify our UI by addressing 100% of known color contrast issues and updating CSS according to design system specs. For every stage group, identify areas where we rely solely on color to convey information by performing an audit, creating related issues, and identifying solutions.
        1. UX Research: Democratize UX Research and prolong the relevance of every research study (scale) by making insights easily findable by anyone. Create a GitLab repository, evaluate 100% of existing studies to identify granular findings and recommendations, and tag every finding with searchable labels (such as stage group and customer type).
        1. UX Research: Understand how GitLab users currently rate our usability so we can track changes over time. Identify segmenting data (such as customer type and length of use), run a pilot study with X users, analyze data, and re-run a larger study.


### CEO: Great team. Employer brand, Effective hiring, Increase decision making effectiveness.
1. CFO: Improve financial reporting and accounting processes to support growth and increased transparency.
    1. Mgr. of Data Team: 100% of executive dashboards completed with goals and definitions
    1. Mgr. of Data Team: Public release of finance metric(s).
    1. Dir. of Bus Ops: Data Quality Process (DQP) completed for ARR, Net and Gross Retention and Customer counts.
    1. FinOps Lead: Integrated financial and hiring model that covers 100% of expense categories driven by IACV (as first iteration) and marketing funnel (as second iteration).
    1. Sr Dir. of Legal: Detailed SoX compliance plan published internally and reviewed by Board.
    1. Sr Acctg Manager:  Key internal controls documented in handbook.
1. CFO: Create scalable infrastructure for achieving headcount growth
    1. Sr Dir. of Legal: 90% of team members covered by scalable employment solution
    1. Payroll and Payments Lead: Automation of contractor payroll completed.
1. CFO: Improve company wide operational processes
    1. Mgr. of Data Team: All data processes documented with handbook as SSOT
    1. Dir. of Bus Ops: Roadmaps for all staffed business operations functions are shipped and Q2 iteration by EOQ
    1. Controller: Zuora upgrade to orders which will allow for ramped deals, multiple amendments on single quote and MRR by subscription reporting.
1. VPE: [Create a happy and productive team. Unify vacancy descriptions with experience factor content and career matrices for all roles, develop compensation roadmaps for every major role in engineering, hold a roundtable meeting for each role benchmark](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3907)
    1. Development: Deliver the 2019 roadmap. Hire IC's to plan, Hire Directors for secure and enablement
    1. Development: Improve customer focus. Get every engineer in one customer conversation
        1. Fellow: Help solve critical customer issues: Solve 1 P1 issue
        1. Dev
            1. Gitter: Improve security practices: Enable use of dev.gitlab.org for security issues, document security release process for gitter.im in partnership with Security
            1. Manage BE: Increase test coverage for the customers app: increase from 45% to 60%
            1. Manage BE: Proactively reduce future security flaws. Identify five areas where we have systematic/pervasive/repeated security flaws, Finalize plan to tackle three areas, 2 merge requests merged
            1. Create BE: Increase set of people working on Create features: have team deliver 6 [Create Deep Dives](https://gitlab.com/gitlab-org/create-team/issues/1)
            1. Plan BE: Add to engineering blog to make it a more attractive destination for customers and prospective candidates. Four blog posts from the team on recent Plan work (including backstage)
        1. Sec
            1. Secure BE: Improve onboarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.
        1. Ops (Verify, Release & Package): Improve on-boarding for new engineers: Improve team pages, resources, time to first MR merged by new engineers: Goal for first MR to be complete in the first 2 weeks.
            1. Verify BE: Improve on-boarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.
            1. Release: Improve on-boarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.
        1. Ops (Configure & Monitor): Improve on-boarding for new engineers: Improve team pages, resources, time to first MR merged by new engineers: Goal for first MR to be complete in the first 2 weeks.
        1. Ops (Configure & Monitor): Define first iteration for BE role framework (Software Engineer, Senior Software Engineer, Staff Software Engineer)
            1. Configure BE: Improve on-boarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.
            1. Monitor BE: Improve on-boarding for new engineers: Improve team page, resources, time to first MR merged by new engineers.
        1. Enablement: Improve documentation and training for supporting our customers: identify and address 3 major pain points for customer support and/or professional services.
            1. Distribution: GA for GitLab Operator: GitLab Operator enabled by default in the charts
            1. Distribution: Create automated charts installation and upgrade pipelines for supported Kubernetes flavors (e.g. GKE, EKS, etc.)
            1. Distribution: Generate library licenses and collection for the official Helm charts (similar to the omnibus-gitlab functionality)
            1. Gitaly: Make rapid progress on top company priorities: ship GA of object deduplication, ship beta of Gitaly HA
1. CCO: Improve the candidate experience at GitLab for hiring great talent.
     1. Recruiting Director: Recruiting processes and systems to expedite hiring communicated across GitLab
     1. Recruiting Director: Average candidate timeline between application and offer reduced to 30 days.
     1. All Hiring Managers and Interviewers: 80% of candidate feedback submitted within 24 hours of the interview.
1. CCO:  Improve GitLab's training and development Program,
     1. People Ops Director: Hire a L&D Specialist by the end of Q1.
     1. People Ops Director: Increase the tuition/course reimbursement utilization by 10%
     1. People Ops Director: Define metrics for each training that inform us on the value of current and future trainings
     1. L&D Specialist:  80% of attendees rate the training as valuable and/or actionable
     1. L&D Specialist: Defined key business metrics that should be improved by the training.
     1. L&D Specialist: Follow-up survey 30-60 after training to gauge applicability and usefulness.
1. CCO:  Improve GitLab's Employer brand to increase our hiring and sourcing success.
     1. Recruiting Director: Hire an employment branding specialist by the end of Q1.
     1. Recruiting Director: Identify 5-10 locations to focus our branding and sourcing (25% increase in pipeline from these locations).
     1. Recruiting Director: Collaborate with Marketing to ensure that recruiting activities are integrated into at least 95% for conferences by the end of Q1
     1. Employer Branding Specialist: Reply to 90% of Glassdoor reviews
1. Alliances: 3 acquisitions completed with 15 new team members joining GitLab. Establish the foundations of the sourcing process.
