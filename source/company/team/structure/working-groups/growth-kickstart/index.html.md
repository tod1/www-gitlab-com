---
layout: markdown_page
title: "Growth Kickstart Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property     | Value |
|--------------|-------|
| Date Created | March 27, 2019 |
| Date Ended   | TBD |
| Slack        | [#wg_growth-kickstart](https://gitlab.slack.com/messages/CHACKQLQG) (only accessible from within the company) |
| Google Doc   | [Growth Kickstart Working Group Agenda](https://docs.google.com/document/d/1kur419WeY_tIVX4EqtNSiXWOuLR-VYv8-8WsNdyyVoM/edit#) (only accessible from within the company) |

## Business Goal

To kickstart the Growth group (PM, Engineering) until it has a critical mass of people and can function on it's own.

## Exit Criteria

* TBD issues from board hired
* TBD Growth group members hired
* Growth KPIs defined

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Facilitator           | Eric Brinkman         | Dir of Product Management      |
| Engineering Lead      | Bartek Marnane        | Dir of Engineering, Growth     |
| PM Lead               | Jeremy Watson         | Product Manager, Manage        |
| Finance Lead          | Brooks Royer          | Finance Business Partner       |
| Member                | Mark Pundsack         | Head of Product                |
| Executive Stakeholder | Paul Machle           | CFO                            |
