---
layout: markdown_page
title: "Portal Process Improvement Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property     | Value |
|--------------|-------|
| Date Created | March 29, 2019 |
| Date Ended   | TBD |
| Slack        | [#wg_portal_process](https://gitlab.slack.com/messages/CHFB3AU1K)(only accessible from within the company) |
| Google Doc   | Portal Process Improvement Working Group Agenda (only accessible from within the company) |
| Epic         | [tbd] |

## Business Goal

Quickly identify and address actionable, high priority iterations to our portal process, while we resource this area of the business longer term.

## Exit Criteria

TBD

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Facilitator           | Walter Zabaglio       | Director of Business Operations |
| Member                | Luca Williams        | Product Manager, Fulfillment         |
| Member                | Kyla Gradin       | SMB Customer Advocate       |
| Member                | Cristine Marquardt       | Senior Billing Specialist       |
| Member                | Francis  | Director, Sales Operations               |
| Executive Stakeholder | Todd Barr             | CMO |