---
layout: markdown_page
title: "Financial Planning & Analysis"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Financial Planning @ GitLab

At GitLab, we run a rolling 4 quarter forecast process. This means that we are always looking out a minimum of 12 months when projecting revenue and expenses, and we update those forecasts at least once per quarter. Things move quickly and our forecast needs to iterate quickly to keep up with the business. We plan our expenses at a high level (e-group) and we expect this group to make prioritizations and trade-offs while remaining accountable against the budget parameters. By reforecasting quarterly, we can quickly evaluate and incorporate new initiatives into our forecasting model. That being said, we do follow an annual plan to set our goals and measurement for our top-level targets of revenue, profitability and expense management. We follow the cadence below in our planning process:

**Note: During the last quarter of the year, GitLab runs a 5 quarter forecast that aligns with it's annual planning efforts.** 

## FY2020 Financial Planning Goals

| Function  | FY2020 Expense Target  |
|---|---|
| Customer Support  |10% of ARR   |
| Customer Solutions  | 80% of PS Recognized Revenue  |
| Engineering  | 65% of IACV  |
| Sales  | 50% of IACV  |
| Marketing   | 50% of IACV  |
| G&A  | 10% of Total Operating Expense  |
	
****

## <i class="fas fa-users fa-fw icon-color font-awesome" aria-hidden="true"></i> FP&A Team Responsibilities

### Financial Planning & Analysis Lead

###### Responsibilities

- Coordinates Monthly Investor Update
- Coordinates Rolling 4 Quarter Process
- Coordinates Annual Planning Process
- Ensures GitLab's financial model and forecast is accurate
- Partners with Business Operations to ensure Financial Systems & Integration Roadmap
- Develops data processes in partnership with the Data team
- Responsible for financial and mechanical integrity of the operating model and street model

Note: Financial Planning & Analysis Lead performing Finance Business Partner role on interim basis for CEO, CPO, CFO and Head of Business Development.



[Position Description](/job-families/finance/finance-operations-and-planning/){:.btn .btn-purple}

### Finance Business Partner

###### Responsibilities

- Reviews BvsA Analysis
- Negotiates Vendor Contracts
- Develops and maintains compensation modeling
- Coordinates Monthly Metrics Prep
- Develops and maintains cost and efficiency benchmarking
- Develops and maintains Unit economic analysis
- Partners with function leaders to ensure financial and key metric forecast accuracy of supported functions 
- Provides Adhoc Analysis to supported functional leaders

[Position Description - Engineering Specific](/job-families/finance/finance-business-partner-engineering/){:.btn .btn-purple}

[Position Description - Sales Specific](/job-families/finance/finance-business-partner-sales/){:.btn .btn-purple}

### Financial Analyst

###### Responsibilities

- Reviews BvsA Analysis - G&A Function
- Partners with Recruiting on hiring plan
- Develops and maintains contract & renewal projections
- Develops and maintains monthly audit projects
- Develops and maintains planned vs actuals GitLab Team analysis
- Reviews BambooHR to ensure GitLab Team are in the correct functions and departments

[Position Description](/job-families/finance/financial-analyst/){:.btn .btn-purple}

****

## Calendar of Events
Throughtout the year the Executive Assistant team puts together a calendar of events for board members and other events associated with planning. Those events can be found in the [GitLab Board Calendar](https://docs.google.com/spreadsheets/d/1GW59GiT0MLXEgMxy2bN0ZVcbMs_wssOkP2c59f19YTk/edit?usp=sharing) sheet.

### Monthly Update
* Hold Departmental Metrics Meetings to review how well the departments are operating. 
* Send out Investors Update
* Update our Budget vs Actual model with revenue, expense and headcount actuals.
* Perform an actuals vs  budget/forecast variance analysis.
* Distribute monthly results to budget owners.

###### Monthly Important Dates
* **10th of the Month** - Update Metrics Report
* **10th of the Month** - Update Revenue Model
* **10th of the Month** - Send out Investors Update
* **15th of the Month** - Update Financial Models
* **15th of the Month** - Distribute monthly results to budget owners

****

### Quarterly Forecast
* All of the activities in the Monthly Forecast.
* Budget owners update headcount planning templates and non-headcount expenses.
* Revenue model updated and signed off by CMO and CRO.

###### Quarterly Important Dates
* **14th of the Last Month in Fiscal Quarter** - Send out calender invites to review non-headcount & headcount expenses 
* **22th of the Last Month in Fiscal Quarter** - Finalize non-headcount & headcount planning with departments
* **22th of the Month** - Finalize Revenue Model signoff from CMO and CRO

****

### Annual Plan
* All of activities above.
* Revise and update the annual sales compensation plan.
* Set annual quota assignments for revenue producing roles.
* Review product investments vs expected revenue generation.
* Set expected amount for annual compensation increases.
* Set targets for any contributors on a company based performance plan.
* Set company targets for board, investors and creditors.
* Our Annual Plan is viewable internally as a google slide presentation.  Search on "[current year e.g. 2018] Plan" to view.

###### Annual Planning Important Dates
* **14th of September** - Sales Compensation Direction
* **15th of September** - Four quarter rolling forecast kick-off
* **30th of September** - Four quarter rolling forecast completed
* **1st of November** - Preliminary outlook reviewed at  Board of Directors
* **6th of November** - Plan iteration and discussion at egroup offsite in Sonoma
* **30th of November** - Product Roadmap and Investments (board review)
* **6th of December** - Sales Ramp and Compensation (board review)
* **13th of December** - Marketing and Demand Generation (board review)
* **5th of January** - Sales compensation plans distributed to sales team
* **25th of January** - Plan sent to Board for Approval
* **31st of January** - 20xx Plan Approved by Board

### Terminology
* Plan is the term we used for the current plan of record which has been approved by the Board.  Typically this is set at the beginning of the year.
* Forecast is a dynamic assessment based on current expectations of financial performance.
* Target is a goal or objective that may be higher or lower than the Plan or Forecast. Targets are typically used in conjunction with setting OKRs, compensation plans or other performance objectives.

## Monthly Budget Variance Analysis

#### Purpose
Each month after the financials have been published, we review department spend data in detail. The goal of this analysis is to compare department budgets with actual results and examine any material discrepancies between budgeted and actual costs. These costs are reviewed at the department level, allowing us to measure progress in meeting our plan, forecast, and operating model.
#### Process
Following the month-end close, the Accounting Manager distributes department income statements to the related budget owners and the e-group members. Each department is then responsible for comparing these reports, which contain actual costs, to the budget spreadsheet in Google drive. Departments should analyze their data and if necessary, discuss items of interest and take appropriate action. Any questions regarding the cost data should be sent to the Accounting Manager.

#### Timing
1. The Accounting Manager will send the income statements on or before the 15th of each month.
1. Budget owners are responsible for performing their analysis and make any related inquiries within two business days of receiving the reports.

#### Expense Controls and Improving Efficiency
1. The primary mechanism to ensure efficient spend of company assets is the approval process prior to authorization. See [Signature Authorization Matrix](/handbook/finance/authorization-matrix/).
1. The secondary mechanism relates to approval of the payment and related voucher package according to the [Signature Authorization Matrix](/handbook/finance/authorization-matrix/)
1. The third mechanism is the budget vs actual review to determine reasons for variances vs Plan. 

## Monthly Investor Update
Each month we send to our investors an update no later than the 10th day following the end of the month. For further reference see our [blog post](https://about.gitlab.com/2018/10/17/how-we-keep-investors-in-the-loop/).
#### Format
1. Thanks
1. Asks
1. Key Metrics
1. Lowlights
1. Highlights
1. Expections (next month)

#### Process
1. When operating metrics have been finalized, the FinOps lead prepares a draft of the investor update in a google document and posts in the #investor-update slack channel.
1. The FinOps lead will `@mention` e-group members with asks for topics related to the investor update agenda. 
1. On the same day the investor update draft is added to the #investor-update slack channel and after review from the e-group members input, the CEO sends the update to the investor mailing list. 
1. Once the investor update is sent to the investor mailing list, the FinOps lead will add the current investor update to the #investor-update slack channel, along with highlight commentary on GitLab's operating metrics. 

## Department Structure

Below is a table showing the structure of GitLab departments as they exist in BambooHR. Please [check out the Team Page](/company/team/org-chart) to see our org chart.

## By Function - Where GitLabbers Report 

| Sales                | Marketing           | Engineering      | Product            | G&A                 |
|:--------------------:|:-------------------:|:----------------:|:------------------:|:-------------------:|
| Business Development | Corporate Marketing | Meltano          | Product Mangement  | Business Operations |
| Channel              | Demand Generation   | Infrastructure   |                    | CEO                 |
| Commerical Sales     | Digitial Marketing  | Development      |                    | Finance             |
| Customer Solutions   | Field Marketing     | Quality          |                    | People Ops          |
| Customer Success     | Marketing Ops       | Security         |                    | Recruiting          |
| Enterprise Sales     | Product Marketing   | UX               |                    |                     |
| Field Operations     | Outreach            | Customer Support |                    |                     |


## Allocation Methodology

GitLab uses an indirect cost allocation. GitLab allocates various cost items throught its departments based on consumption of resources or headcount. Below are the departments or diagrams that illustrate how various cost items are allocated at GitLab.


### By Cost Center
<table>
  <tr>
    <th>Sales</th>
    <th>Cost of Sales</th>
    <th>Marketing</th>
    <th>R&D</th>
    <th>General & Administrative</th>
  </tr>
  <tr>
    <td>Business Development</td>
    <td>Customer Solutions</td>
    <td>Corporate Marketing</td>
    <td>Meltano</td>
    <td>Business Operations</td>
  </tr>
  <tr>
    <td>Channel</td>
    <td>Customer Support</td>
    <td>Demand Generation</td>
    <td>Development</td>
    <td>CEO</td>
  </tr>
  <tr>
    <td>Commerical Sales</td>
    <td></td>
    <td>Field Marketing</td>
    <td>Quality</td>
    <td>Finance</td>
  </tr>
  <tr>
    <td>Customer Success</td>
    <td></td>
    <td>Digital Marketing</td>
    <td>Security</td>
    <td>People Ops</td>
  </tr>
  <tr>
    <td>Enterprise Sales</td>
    <td></td>
    <td>Product Marketing</td>
    <td>UX</td>
    <td></td>
  </tr>
  <tr>
    <td>Field Operations</td>
    <td></td>
    <td>Outreach</td>
    <td>Product Management</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>Marketing Operations</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="2" align="center">Infrastructure</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
      <td colspan="5" align="center">Recruiting</td>
  </tr>
  <tr>
      <td colspan="5" align="center">Business Operations Owned, Companywide Expenses (i.e. gmail, slack, zoom)</td>
  </tr>
</table>

### Creating New Departments
Successfully creating new departments require that various company systems be updated to capture newly defined department structures. Once the need for a new department arises, follow these steps:

1. Create an issue using the *dept_change* template.
2. In the issue, add the appropriate team members from each department included in the checklist.
3. Disclose the nature of the new department. Is the new department simply a name change, or there is a structural modification?   
4. Provide all detail necessary to ensure team members are assigned to the newly created departments. 


